# Spring Tutor Timetable Development
---

This Readme will guide you through the process of setting up an environment for developing the Spring Tutor Timetable system.

## Spring Framework
---
The [Spring][1] Framework is an application framework and inversion of control container for the Java platform. The framework's core features can be used by any Java application, but there are extensions for building web applications on top of the Java EE platform. This project is built on the Spring framework so it may be useful to read through some of the [basic guides][2] to gain an understanding of how Spring works.

## Development
---

### Java Development Kit
1. Download the latest version of the [java development kit][4]

2. Install JDK
	- Windows: Use the appropriate installer
	- Linux: Extract to the appropriate location
	- Both: Ensure system variables point to the new JDK/JRE
    
### Spring Tool Suite
1. Download the [Spring Tool Suite][3] appropriate for your system.

2. Install & run Spring Tool Suite

3. In the toolbar select Help > Install new software

4. Work with "All Available Sites" and search for "Gradle"

5. Install IVY Gradle plugin

6. Restart the tool suite

### Cloning the project
1. Create a new folder in your preferred working directory.

2. Using your preferred git tool, clone the repository https://cnewton@bitbucket.org/cnewton/spring-tutor-timetable.git
	- Within Spring Tool Suite: 
		- Switch to the git perspective, you may need to add it first 
		
		![Failed to Load](readme/addGitPerspective.png "Add Git Perspective")
		
		- Press the clone repo button 
		
		![Failed to Load](readme/cloneGitRepo.png "Clone Repo")
		
		- Select clone URI, enter the URL listed above into the url field, enter your username and password.
		- Press next, select the master branch and any other branches you wish to clone.
		- Press next, select a location on disk.	
		

3. Checkout a local copy of the origin/master branch

### Tomcat
1. Download the [Tomcat7][5] core compressed file appropriate to your system.

2. Extract tomcat to your preferred location.

3. Navigate into the tomcat/conf directory and edit the tomcat-users.xml

4. Add the following between the users tags 
	```
	<role rolename="manager"/>
	<role rolename="manager-gui"/>
	<role rolename="poweruser"/>
	<role rolename="poweruserplus"/>
	<user username="tomcat" password="tomcat" roles="manager-gui,manager-script,admin-gui,admin-script"/>
	```
	
### Project Development
1. If you did not clone the project via Spring Tool Suite
	- Open Spring Tool Suite and navigate to the git perspective as described in "Cloning the project"
	- Use the button to the left of the clone button (Also as described) to add the local repository to view.
	
	
2. Navigate back to the Spring perspective and select File > Import > Projects from Git > Existing local repository

3. Select the project and press next, select "import as general project"

4. After the project becomes available in the package explorer, right click on the project and select Configure > Convert to Gradle project

5. Again right click the project and select Gradle > Manage dependencies. Disregard any errors that occur afterwards.

6. Open the tomcat.xml file located in the project root directory, follow the instructions to create the tomcat-location file.

7. Restart Spring Tool Suite, Gradle should begin downloading the project dependencies.

8. After the dependencies are installed select Window > Show View > Other > Gradle Tasks

9. In the gradle tasks view select the project and once the model is loaded run the "Build" task.

### Testing
1. Test classes in this project should be created in the appropriate test package (see existing tests for example)

2. Each test class will need to extend the SpringTest class found in the test.common package.

3. Each test class will need to be appended to the appropriate test suite (also found in the test.common) package.

4. Separate tests into logical groups: Unit tests, Integration tests, Security tests and possibly *External services tests.* Each of these test types should have a corresponding test suite.

5. Each test suite should have a corresponding gradle task for running them and should also be included in the "JacocoTestReport" task (excluding external services tests).

6. The test database is created in memory and should be defined by a testdb-config file in the test directory.
	- A collection of SQL scripts should be created in the test.sql directory.
	- A RecreateTables script to define each of the tables
	- A script for each domain object needing seed data.
	- Each test should be independent of the contents of the database prior to its execution and should clean up any changes it made to the database during its execution.
	

7. See existing tests for examples. See build.gradle for an example of creating test tasks.

### Deployment
1. Ensure the project.properties file is referencing a valid database.

2. Run the startTomcatServer gradle task

3. Run the deploy gradle task

4. After deployment is finished (This can take up to 5 minutes depending on configuration) view the server at http://localhost:8080


[1]: https://spring.io/ "Spring"
[2]: https://spring.io/guides "Guides"
[3]: https://spring.io/tools/sts/all "STS"
[4]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "JDK"
[5]: http://tomcat.apache.org/download-70.cgi "Tomcat"