package au.edu.curtin.tutortimetable.dao.tutorial;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

@Repository
public class TutorialDAOImpl extends DomainObjectDAOImpl<Tutorial> implements TutorialDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Tutorial> loadTutorialsForTutor(Tutor tutor) {

        Criteria c = getCurrentSession().createCriteria(Tutorial.class)
                .add(Restrictions.eq("tutor", tutor))
                .setFetchMode("tutorialGroup", FetchMode.JOIN)
                .setFetchMode("tutorialGroup.unit", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Tutorial> loadAll() {

        Criteria c = getCurrentSession().createCriteria(Tutorial.class)
                .setFetchMode("tutor", FetchMode.JOIN)
                .setFetchMode("tutorialGroup", FetchMode.JOIN)
                .setFetchMode("tutorialGroup.unit", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }
}
