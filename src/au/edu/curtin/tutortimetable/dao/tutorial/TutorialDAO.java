package au.edu.curtin.tutortimetable.dao.tutorial;

import java.util.List;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

public interface TutorialDAO extends DomainObjectDAO<Tutorial> {

    public List<Tutorial> loadTutorialsForTutor(Tutor tutor);

    public List<Tutorial> loadAll();

}