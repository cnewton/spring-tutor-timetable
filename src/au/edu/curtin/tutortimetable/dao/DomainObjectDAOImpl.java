package au.edu.curtin.tutortimetable.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class is responsible for a majority of the lower level data access operations to be performed. At the time of
 * construction each concrete DAO extending this class will discover it's domain object parametized type. This class
 * type will be used for subsequent data access operations as they are performed via object relational mapping.
 * 
 * @author Cameron Newton
 *
 * @param <T>
 *            - The domain object type
 */
public class DomainObjectDAOImpl<T> implements DomainObjectDAO<T> {

    protected static final Logger logger = LogManager.getLogger(DomainObjectDAOImpl.class);

    @Autowired
    protected SessionFactory sessionFactory;

    protected Class<T> clazz;

    @SuppressWarnings("unchecked")
    public DomainObjectDAOImpl() {

        ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
        clazz = (Class<T>) pt.getActualTypeArguments()[0];
    }

    @Override
    @SuppressWarnings("unchecked")
    public T find(long id) {

        Criteria query = getCurrentSession().createCriteria(clazz);
        query.add(Restrictions.idEq(id));

        return (T) query.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> findAll() {

        Criteria c = getCurrentSession().createCriteria(clazz);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    public void createOrUpdate(T obj) {

        getCurrentSession().saveOrUpdate(obj);
    }

    @Override
    public void delete(long id) {

        getCurrentSession().delete(find(id));
    }

    public void delete(T obj) {

        getCurrentSession().delete(obj);
    }

    @Override
    public long count() {

        Criteria c = getCurrentSession().createCriteria(clazz);
        c.setProjection(Projections.rowCount());
        return (Long) c.uniqueResult();
    }

    @Override
    public boolean idExists(long id) {

        Criteria c = getCurrentSession().createCriteria(clazz);
        c.add(Restrictions.idEq(id));
        c.setProjection(Projections.rowCount());

        return ((Long) c.uniqueResult()).equals(Long.valueOf(1l));
    }

    protected Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }
}