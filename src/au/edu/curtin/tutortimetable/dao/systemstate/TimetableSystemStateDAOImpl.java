package au.edu.curtin.tutortimetable.dao.systemstate;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;

/**
 * This repository acts on a single configuration table. Transactions are only held at this layer to avoid multiple
 * "live" copies of the state being worked on simultaneously.
 * 
 * @author Cameron
 *
 */
@Repository
public class TimetableSystemStateDAOImpl implements TimetableSystemStateDAO {

    protected static final Logger logger = LogManager.getLogger(TimetableSystemStateDAOImpl.class);

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    @Transactional
    public TimetableSystemState get() {

        Criteria query = getCurrentSession().createCriteria(TimetableSystemState.class);

        return (TimetableSystemState) query.uniqueResult();
    }

    @Override
    @Transactional
    public void update(TimetableSystemState state) {

        // Allow only 1 database system state.
        state.setId(1);
        getCurrentSession().saveOrUpdate(state);
    }

    protected Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }

}
