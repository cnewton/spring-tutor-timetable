package au.edu.curtin.tutortimetable.dao.systemstate;

import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;

public interface TimetableSystemStateDAO {

    public TimetableSystemState get();

    public void update(TimetableSystemState state);

}
