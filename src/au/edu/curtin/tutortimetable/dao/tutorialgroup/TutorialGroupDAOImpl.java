package au.edu.curtin.tutortimetable.dao.tutorialgroup;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;

@Repository
public class TutorialGroupDAOImpl extends DomainObjectDAOImpl<TutorialGroup> implements TutorialGroupDAO {

    @Override
    public TutorialGroup findByName(String groupName) {

        Criteria c = getCurrentSession().createCriteria(TutorialGroup.class);
        c.add(Restrictions.eq("groupName", groupName));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (TutorialGroup) c.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TutorialGroup> loadTutorialGroupsForUnit(Unit unit) {

        Criteria c = getCurrentSession().createCriteria(TutorialGroup.class)
                .add(Restrictions.eq("unit", unit))
                .setFetchMode("tutorials", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }
}
