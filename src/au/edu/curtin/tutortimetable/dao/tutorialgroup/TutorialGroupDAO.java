package au.edu.curtin.tutortimetable.dao.tutorialgroup;

import java.util.List;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;

public interface TutorialGroupDAO extends DomainObjectDAO<TutorialGroup> {

    public TutorialGroup findByName(String groupName);

    public List<TutorialGroup> loadTutorialGroupsForUnit(Unit unit);

}
