package au.edu.curtin.tutortimetable.dao.tutorunitpreference;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;

@Repository
public class TutorUnitPreferenceDAOImpl extends DomainObjectDAOImpl<TutorUnitPreference> implements
        TutorUnitPreferenceDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<TutorUnitPreference> loadForUnit(Unit unit) {

        Criteria c = getCurrentSession().createCriteria(TutorUnitPreference.class)
                .add(Restrictions.eq("unit", unit))
                .setFetchMode("tutor", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

}
