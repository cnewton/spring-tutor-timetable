package au.edu.curtin.tutortimetable.dao.tutorunitpreference;

import java.util.List;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;

public interface TutorUnitPreferenceDAO extends DomainObjectDAO<TutorUnitPreference> {

    public List<TutorUnitPreference> loadForUnit(Unit unit);

}
