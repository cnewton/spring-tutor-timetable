package au.edu.curtin.tutortimetable.dao;

import java.util.List;

/**
 * A Generic DAO represents a data access object layer for any standard domain object. A Generic DAO contains methods
 * for finding, adding, updating and deleting domain objects from a database table.
 * 
 * Instances of classes that implement this interface must provide the parameterized domain object type for ORM to work.
 * 
 * @author Cameron Newton
 *
 * @param <T>
 *            - The Domain object type.
 */
public interface DomainObjectDAO<T> {

    /**
     * Attempt to retrieve the object from the database. The object is automatically mapped into its relational class.
     * 
     * @param id
     *            of database row
     * @return database row as object or null if not found.
     */
    public T find(long id);

    /**
     * Retrieves all entries of the implemented datatype from the database and returns them as a list.
     * 
     * @return a list of all database entries.
     */
    public List<T> findAll();

    /**
     * Saves the given object to a new database row, throws an exception if the identifier is not empty
     * 
     * @param obj
     *            to add to database
     */
    public void createOrUpdate(T obj);

    /**
     * Deletes the database row referenced by the given id. Throws exception if the database row does not exist.
     * 
     * @param id
     *            of row to delete
     */
    public void delete(long id);

    /**
     * Deletes the database row related to the given java object. Throws exception if the database row does not exist.
     * 
     * @param obj
     *            matching database row to delete
     */
    public void delete(T obj);

    /**
     * Returns the number of entries of the implemented datatype from the database.
     * 
     * @return count of elements
     */
    public long count();

    /**
     * Returns true if the given id has a corresponding entry in the database table.
     * 
     * @param id
     * @return
     */
    public boolean idExists(long id);

}