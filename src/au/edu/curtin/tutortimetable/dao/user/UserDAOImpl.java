package au.edu.curtin.tutortimetable.dao.user;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.NonUniqueResultException;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

@Repository
public class UserDAOImpl extends DomainObjectDAOImpl<User> implements UserDAO {

    protected static final Logger logger = LogManager.getLogger(UserDAOImpl.class);

    @Override
    public User findByUsername(String username) {

        Criteria c = getCurrentSession().createCriteria(User.class);
        c.add(Restrictions.eq("username", username));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (User) c.uniqueResult();
    }

    @Override
    public Tutor findTutor(long id) {

        Criteria c = getCurrentSession().createCriteria(Tutor.class);
        c.add(Restrictions.eq("id", id));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (Tutor) c.uniqueResult();
    }

    @Override
    public UnitCoordinator findCoordinator(long id) {

        Criteria c = getCurrentSession().createCriteria(UnitCoordinator.class);
        c.add(Restrictions.eq("id", id));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (UnitCoordinator) c.uniqueResult();
    }

    @Override
    public UnitCoordinator findCoordinatorByName(String name) {

        Criteria c = getCurrentSession().createCriteria(UnitCoordinator.class);
        c.add(Restrictions.eq("name", name));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        UnitCoordinator coordinator = null;

        try {
            coordinator = (UnitCoordinator) c.uniqueResult();
        } catch (NonUniqueResultException e) {
            logger.warn("Multiple coordinators with name " + name + " found.");
        }

        return coordinator;
    }

    @Override
    public UnitCoordinator findCoordinatorByStaffId(String staffId) {

        Criteria c = getCurrentSession().createCriteria(UnitCoordinator.class);
        c.add(Restrictions.eq("staffId", staffId));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        UnitCoordinator coordinator = null;

        try {
            coordinator = (UnitCoordinator) c.uniqueResult();
        } catch (NonUniqueResultException e) {
            logger.warn("Multiple coordinators with Staff Id " + staffId + " found.");
        }

        return coordinator;
    }

    @Override
    public Tutor loadTutor(long id) {

        Criteria c = getCurrentSession().createCriteria(Tutor.class)
                .add(Restrictions.eq("id", id))
                .setFetchMode("preferredUnits", FetchMode.JOIN)
                .setFetchMode("assignedTutorials", FetchMode.JOIN)
                .setFetchMode("availabilities", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (Tutor) c.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UnitCoordinator> findAllCoordinators() {

        Criteria c = getCurrentSession().createCriteria(UnitCoordinator.class);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Tutor> findAllTutors() {

        Criteria c = getCurrentSession().createCriteria(Tutor.class, "user");
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        c.add(Restrictions.eq("user.class", Tutor.class));

        return c.list();
    }

}
