package au.edu.curtin.tutortimetable.dao.user;

import java.util.List;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

public interface UserDAO extends DomainObjectDAO<User> {

    public User findByUsername(String username);

    public Tutor findTutor(long id);

    public UnitCoordinator findCoordinatorByName(String name);

    public UnitCoordinator findCoordinatorByStaffId(String staffId);

    public UnitCoordinator findCoordinator(long id);

    public Tutor loadTutor(long id);

    public List<UnitCoordinator> findAllCoordinators();

    public List<Tutor> findAllTutors();

}
