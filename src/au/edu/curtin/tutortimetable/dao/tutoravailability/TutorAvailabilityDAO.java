package au.edu.curtin.tutortimetable.dao.tutoravailability;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;

public interface TutorAvailabilityDAO extends DomainObjectDAO<TutorAvailability> {

}
