package au.edu.curtin.tutortimetable.dao.tutoravailability;

import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;

@Repository
public class TutorAvailabilityDAOImpl extends DomainObjectDAOImpl<TutorAvailability> implements TutorAvailabilityDAO {

}
