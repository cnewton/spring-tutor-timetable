package au.edu.curtin.tutortimetable.dao.unit;

import java.util.List;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

public interface UnitDAO extends DomainObjectDAO<Unit> {

    public List<Unit> findUnitByCode(String code);

    public List<Unit> findAllForCoordinator(UnitCoordinator coordinator);

    public List<Unit> loadAllWithCoordinator();

    public List<Unit> loadAllWithGroups();

}
