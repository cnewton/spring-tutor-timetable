package au.edu.curtin.tutortimetable.dao.unit;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAOImpl;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

@Repository
public class UnitDAOImpl extends DomainObjectDAOImpl<Unit> implements UnitDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Unit> findUnitByCode(String code) {

        Criteria c = getCurrentSession().createCriteria(Unit.class);
        c.add(Restrictions.eq("unitCode", code));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Unit> findAllForCoordinator(UnitCoordinator coordinator) {

        Criteria c = getCurrentSession().createCriteria(Unit.class);
        c.add(Restrictions.eq("unitCoordinator", coordinator));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Unit> loadAllWithCoordinator() {

        Criteria c = getCurrentSession().createCriteria(clazz)
                .setFetchMode("unitCoordinator", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Unit> loadAllWithGroups() {

        Criteria c = getCurrentSession().createCriteria(clazz)
                .setFetchMode("tutorialGroups", FetchMode.JOIN)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c.list();
    }
}
