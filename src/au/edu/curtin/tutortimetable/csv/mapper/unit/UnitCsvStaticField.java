package au.edu.curtin.tutortimetable.csv.mapper.unit;

/**
 * This enum defines the static header names to be searched for in a Unit CSV extract.
 * 
 * @author Cameron Newton
 *
 */
public enum UnitCsvStaticField {
    UNIT_CODE("UNIT_CODE"), UNIT_NAME("UNIT_NAME"), UNIT_COORDINATOR_ID("UNIT_COORDINATOR_ID"), UNIT_COORDINATOR_NAME(
            "UNIT_COORDINATOR_NAME");

    private String csvHeaderName;

    private UnitCsvStaticField(String csvHeaderName) {

        this.csvHeaderName = csvHeaderName;
    }

    public String getCsvHeaderName() {

        return csvHeaderName;
    }

    public static String[] stringValues() {

        String[] values = new String[values().length];
        int ii = 0;
        for (UnitCsvStaticField field : values()) {
            values[ii++] = field.getCsvHeaderName();
        }
        return values;
    }
}