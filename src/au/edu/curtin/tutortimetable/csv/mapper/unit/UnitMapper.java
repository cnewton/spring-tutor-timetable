package au.edu.curtin.tutortimetable.csv.mapper.unit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import au.edu.curtin.tutortimetable.csv.mapper.Mapper;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.UnitMappingException;

public class UnitMapper implements Mapper<Unit> {

    protected static final Logger logger = LogManager.getLogger(UnitMapper.class);

    List<UnitCoordinator> unitCoordinators;

    public UnitMapper(List<UnitCoordinator> coordinators) {

        this.unitCoordinators = coordinators;
    }

    @Override
    public Unit map(Map<String, String> map) throws UnitMappingException {

        if (!isValidMap(map)) {
            throw new UnitMappingException("Supplied map does not contain required fields.");
        }

        Unit unit = new Unit();

        unit.setUnitCode(map.get(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()));
        unit.setUnitName(map.get(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName()));

        String coordinatorId = map.get(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName());
        String coordinatorName = map.get(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName());

        if (coordinatorId != null) {
            unit.setUnitCoordinator(unitCoordinators.stream()
                    .filter(coord -> coordinatorId.equals(coord.getStaffId()))
                    .findAny().orElse(null));
        }

        if (unit.getUnitCoordinator() == null && coordinatorName != null) {

            unit.setUnitCoordinator(unitCoordinators.stream()
                    .filter(coord -> coordinatorName.equals(coord.getName()))
                    .findAny().orElse(null));
        }

        return unit;
    }

    public static Map<String, String> map(Unit obj) {

        Map<String, String> map = new HashMap<>();

        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), obj.getUnitCode());
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), obj.getUnitName());

        if (obj.getUnitCoordinator() != null) {
            map.put(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName(), obj.getUnitCoordinator().getStaffId());
            map.put(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName(), obj.getUnitCoordinator().getName());
        } else {
            map.put(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName(), "");
            map.put(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName(), "");
        }

        return map;
    }

    @Override
    public boolean isValidMap(Map<String, String> map) {

        return UnitMapper.validateHeaders(map.keySet().toArray(new String[0]));
    }

    /**
     * Validates the existence of all of the required UnitCSV fields in the header Array.
     * 
     * @param headers
     * @return false if invalid
     */
    public static boolean validateHeaders(String[] headers) {

        List<String> headerList = Arrays.asList(headers);

        return (headerList.contains(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()) && headerList
                .contains(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName()));
    }

}
