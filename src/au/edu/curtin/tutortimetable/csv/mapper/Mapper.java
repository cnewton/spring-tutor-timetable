package au.edu.curtin.tutortimetable.csv.mapper;

import java.util.Map;

import au.edu.curtin.tutortimetable.exception.ObjectMappingException;

public interface Mapper<T> {

    /**
     * Attempts to constuct an object from the supplied map. Throws a mapping exception if the map is not valid for the
     * object type.
     * 
     * @param Map
     *            to build object from.
     * @return The constructed object.
     * @throws ObjectMappingException
     */
    public T map(Map<String, String> map) throws ObjectMappingException;

    /**
     * This method is to be used to ensure the supplied map is able to be mapped by this mapper implementation.
     * 
     * @param map
     * @return true if able to map. False if missing required field.
     */
    public boolean isValidMap(Map<String, String> map);

}
