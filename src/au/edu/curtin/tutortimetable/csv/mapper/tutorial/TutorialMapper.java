package au.edu.curtin.tutortimetable.csv.mapper.tutorial;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import au.edu.curtin.tutortimetable.csv.mapper.Mapper;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialType;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.exception.ObjectMappingException;
import au.edu.curtin.tutortimetable.exception.TutorialMappingException;

public class TutorialMapper implements Mapper<Tutorial> {

    protected static final Logger logger = LogManager.getLogger(TutorialMapper.class);

    private List<Unit> units;

    public TutorialMapper(List<Unit> units) {

        this.units = units;
    }

    @Override
    public Tutorial map(Map<String, String> map) throws ObjectMappingException {

        if (!isValidMap(map)) {
            throw new TutorialMappingException("Supplied map does not contain required fields.");
        }

        String unitCode = map.get(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName());
        Unit unit = null;
        if (!StringUtils.isBlank(unitCode)) {
            unit = units.stream()
                    .filter(uni -> unitCode.equals(uni.getUnitCode()))
                    .findAny()
                    .orElse(null);
        }
        if (unit == null) {
            throw new TutorialMappingException("Tutorial map is missing a valid unit code, unable to map.");
        }

        String groupName = map.get(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName());
        TutorialGroup tutorialGroup = null;
        if (!StringUtils.isBlank(groupName)) {
            tutorialGroup = unit.getTutorialGroups().stream()
                    .filter(group -> groupName.equals(group.getGroupName()))
                    .findAny()
                    .orElse(null);
        } else {
            throw new TutorialMappingException("Tutorial map is missing a valid group name, unable to map.");
        }
        if (tutorialGroup == null) {

            tutorialGroup = new TutorialGroup();
            tutorialGroup.setTutorials(new HashSet<Tutorial>());
            tutorialGroup.setUnit(unit);
            tutorialGroup.setTutorialType(parseTutorialType(map.get(TutorialCsvStaticField.CLASS_TYPE
                    .getCsvHeaderName())));
            tutorialGroup.setGroupName(groupName);
            unit.getTutorialGroups().add(tutorialGroup);
        }

        Tutorial tutorial = new Tutorial();
        String building = map.get(TutorialCsvStaticField.BUILDING.getCsvHeaderName());
        String room = map.get(TutorialCsvStaticField.ROOM.getCsvHeaderName());

        if (StringUtils.isBlank(building) || StringUtils.isBlank(room)) {
            tutorial.setRoom("Unknown");
        } else {
            tutorial.setRoom(building + "." + room);
        }

        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMM-yy");
            String dateString = map.get(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName());
            // Append leading zero if missing.
            if (dateString.length() == 8) {
                dateString = "0" + dateString;
            }
            LocalDate date = LocalDate.parse(dateString, dtf);
            LocalTime startTime = parseTimeString(map.get(TutorialCsvStaticField.START_TIME.getCsvHeaderName()));
            LocalTime endTime = parseTimeString(map.get(TutorialCsvStaticField.END_TIME.getCsvHeaderName()));

            tutorial.setStartTime(LocalDateTime.of(date, startTime));
            tutorial.setEndTime(LocalDateTime.of(date, endTime));
        } catch (DateTimeException | NullPointerException e) {
            throw new TutorialMappingException("Invalid date-time field.", e);
        }

        tutorial.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial);

        return tutorial;
    }

    public static Map<String, String> map(Tutorial obj) throws TutorialMappingException {

        Map<String, String> map = new HashMap<>();
        if (obj.getTutorialGroup() == null || obj.getTutorialGroup().getUnit() == null) {
            throw new TutorialMappingException("Tutorial missing tutorial group or unit. Unable to map.");
        }
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), obj.getTutorialGroup().getUnit().getUnitCode());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), obj.getTutorialGroup().getGroupName());

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMM-yy");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), obj.getStartTime().format(dtf));
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(),
                parseTimeString(obj.getStartTime().toLocalTime()));
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), parseTimeString(obj.getEndTime().toLocalTime()));

        if (obj.getRoom().split("\\.").length == 2) {
            map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), obj.getRoom().split("\\.")[0]);
            map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), obj.getRoom().split("\\.")[1]);
        } else {
            map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "?");
            map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "?");
        }

        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), obj.getTutorialGroup().getTutorialType()
                .getType());

        if (obj.getTutor() != null) {
            map.put(TutorialCsvStaticField.TUTOR_NAME.getCsvHeaderName(), obj.getTutor().getName());
            map.put(TutorialCsvStaticField.TUTOR_EMAIL.getCsvHeaderName(), obj.getTutor().getEmail());
        }

        return map;
    }

    /**
     * This method should be used to parse time in the format HHMMSS (No separator)
     * 
     * @param time
     */
    private static LocalTime parseTimeString(String time) throws DateTimeException {

        int hr = 0, min = 0, sec = 0;
        if (time.length() == 5) {
            hr = Integer.valueOf(time.substring(0, 1));
            min = Integer.valueOf(time.substring(1, 3));
            sec = Integer.valueOf(time.substring(3, 5));

        } else if (time.length() == 6) {
            hr = Integer.valueOf(time.substring(0, 2));
            min = Integer.valueOf(time.substring(2, 4));
            sec = Integer.valueOf(time.substring(4, 6));
        } else {
            throw new DateTimeException("Time string is of incorrect Length.");
        }
        return LocalTime.of(hr, min, sec);
    }

    /**
     * This method should be used to parse time in the format HHMMSS (No separator)
     * 
     * @param time
     */
    private static String parseTimeString(LocalTime time) throws DateTimeException {

        StringBuilder sb = new StringBuilder(6);

        if (time.getHour() < 10) {
            sb.append(0);
        }
        sb.append(time.getHour());

        if (time.getMinute() < 10) {
            sb.append(0);
        }
        sb.append(time.getMinute());

        if (time.getSecond() < 10) {
            sb.append(0);
        }
        sb.append(time.getSecond());

        return sb.toString();
    }

    /**
     * Attempt to find the closest matching class type for the given string.
     * 
     * @param tutorialType
     */
    private static TutorialType parseTutorialType(String tutorialType) {

        int TYPE_CONFIDENCE_THRESHHOLD = 5;

        TutorialType[] types = TutorialType.values();
        int levDistance[] = new int[types.length];

        int ii = 0, smallestDistance = 10000, smallestIdx = 0;

        for (TutorialType type : types) {
            levDistance[ii] = StringUtils.getLevenshteinDistance(tutorialType, type.getType());
            if (levDistance[ii] < smallestDistance) {
                smallestDistance = levDistance[ii];
                smallestIdx = ii;
            }
            ii++;
        }

        if (levDistance[smallestIdx] > TYPE_CONFIDENCE_THRESHHOLD) {
            logger.warn("Unable to find suitable class type match, fallback on Demonstration");
            return TutorialType.DEMONSTRATION;
        }
        return types[smallestIdx];
    }

    @Override
    public boolean isValidMap(Map<String, String> map) {

        return TutorialMapper.validateHeaders(map.keySet().toArray(new String[0]));
    }

    /**
     * Validates the existence of all of the TutorialCSV fields in the header Array.
     * 
     * @param headers
     * @return false if invalid
     */
    public static boolean validateHeaders(String[] headers) {

        List<String> headerList = Arrays.asList(headers);

        List<String> requiredHeaders = new ArrayList<>();
        requiredHeaders.addAll(Arrays.asList(TutorialCsvStaticField.stringValues()));

        requiredHeaders.remove(TutorialCsvStaticField.TUTOR_NAME.getCsvHeaderName());
        requiredHeaders.remove(TutorialCsvStaticField.TUTOR_EMAIL.getCsvHeaderName());

        for (String required : requiredHeaders) {
            if (!headerList.contains(required)) {
                logger.warn("Header missing for Tutorial Map: " + required);
                return false;
            }
        }

        return true;
    }
}
