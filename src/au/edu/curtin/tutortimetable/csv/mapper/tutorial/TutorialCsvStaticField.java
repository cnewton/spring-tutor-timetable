package au.edu.curtin.tutortimetable.csv.mapper.tutorial;

/**
 * This enum defines the static header names to be searched for in a Tutorial CSV extract.
 * 
 * @author Cameron Newton
 *
 */
public enum TutorialCsvStaticField {
    START_TIME("START_TIME"), END_TIME("END_TIME"), SESSION_DATE("SESSION_DATE"), UNIT_CODE("UNIT_CODE"), BUILDING(
            "BUILDING"), ROOM("ROOM"), CLASS_GROUP(
                    "GROUP_NAME"), CLASS_TYPE("CLASS_TYPE"), TUTOR_NAME("TUTOR_NAME"), TUTOR_EMAIL("TUTOR_EMAIL");

    private String csvHeaderName;

    private TutorialCsvStaticField(String csvHeaderName) {

        this.csvHeaderName = csvHeaderName;
    }

    public String getCsvHeaderName() {

        return csvHeaderName;
    }

    public static String[] stringValues() {

        String[] values = new String[values().length];
        int ii = 0;
        for (TutorialCsvStaticField field : values()) {
            values[ii++] = field.getCsvHeaderName();
        }
        return values;
    }
}