package au.edu.curtin.tutortimetable.csv.parser;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.supercsv.exception.SuperCsvException;
import org.supercsv.io.CsvMapReader;
import org.supercsv.prefs.CsvPreference;

import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;

/**
 * This class provides the implementation of parsing a CSV.
 * 
 * @see parseRow can be called to retrieve the next row map. If the end of the file is reached parseRow will return
 *      null.
 * 
 * @see close should be called when finished with the Csv parser.
 * 
 * @author Cameron Newton
 *
 */
public class CsvImportParser implements Closeable {

    protected static final Logger logger = LogManager.getLogger(CsvImportParser.class);

    private CsvMapReader reader;

    private String[] headers;

    private boolean eof = false;

    /**
     * Construct a CsvParser with a reader (generally an input stream reader) of a CSV file.
     * 
     * @param inputFile
     * @throws InvalidCSVException
     */
    public CsvImportParser(Reader inputFile) throws InvalidCSVException {

        try {
            reader = new CsvMapReader(inputFile, buildReaderPreferences());
            headers = reader.getHeader(true);
        } catch (IOException e) {
            throw new InvalidCSVException("Unable to open CSV File", e);
        }
        for (String header : headers) {
            if (StringUtils.isBlank(header)) {
                close();
                throw new InvalidCSVException("CSV contained blank header.");
            }

            if (!header.matches("[ -~]+")) {
                close();
                throw new InvalidCSVException("CSV contained header with illegal characters");
            }
        }
    }

    /**
     * Parse the next row of the CSV file.
     * 
     * @return a map of header to value
     * @throws CsvParsingException
     */
    public Map<String, String> parseRow() throws CsvParsingException {

        Map<String, String> result = null;

        try {
            result = reader.read(headers);
        }
        // Indicates an error during reading (Kernel layer).
        catch (IOException e) {
            eof = true;
            throw new CsvParsingException("Csv file corrupt", e);
        }
        // Indicates an error parsing the row (Parser Layer)
        catch (SuperCsvException e) {
            throw new CsvParsingException("Row corruption: " + getLastRow(), e);
        }

        // Indicates the end of file.
        if (result == null) {
            eof = true;
        }

        return result;

    }

    /**
     * @return Array of header Strings
     */
    public String[] getHeaders() {

        return headers;
    }

    /**
     * Get the last row (in raw format)
     * 
     * @return raw CSV row
     */
    public String getLastRow() {

        return reader.getUntokenizedRow();
    }

    /**
     * @return true when at the end of file.
     */
    public boolean isEOF() {

        return eof;
    }

    /**
     * Disposes of the underlying reader.
     */
    @Override
    public void close() {

        try {
            reader.close();
        } catch (IOException e) {
            logger.warn("Failed to close CSV Parser");
        }
    }

    /**
     * Add additional preferences here for changes in the automatic reader.
     */
    private CsvPreference buildReaderPreferences() {

        return new CsvPreference.Builder(CsvPreference.STANDARD_PREFERENCE)
                // Additional options appended here.
                .surroundingSpacesNeedQuotes(true).build();
    }
}
