package au.edu.curtin.tutortimetable.csv.parser;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import au.edu.curtin.tutortimetable.exception.InvalidCSVException;

/**
 * This class provides the implementation of writing a CSV.
 * 
 * @see writeRow can be called to write the next map to the CSV
 * 
 * @see close should be called when finished with the Csv parser.
 * 
 * @author Cameron Newton
 *
 */
public class CsvExportParser implements Closeable {

    protected static final Logger logger = LogManager.getLogger(CsvExportParser.class);

    private CsvMapWriter writer;

    private String[] headers;

    /**
     * Construct a CsvParser with a reader (generally an input stream reader) of a CSV file.
     * 
     * @param inputFile
     * @throws InvalidCSVException
     * @throws IOException
     */
    public CsvExportParser(Writer writer, String[] headers) throws InvalidCSVException {

        this.writer = new CsvMapWriter(writer, buildReaderPreferences());

        this.headers = headers;

        try {
            this.writer.writeHeader(headers);
        } catch (IOException e) {
            throw new InvalidCSVException("Unable to write csv out.");
        }

        for (String header : headers) {
            if (StringUtils.isBlank(header)) {
                close();
                throw new InvalidCSVException("CSV contained blank header.");
            }

            if (!header.matches("[ -~]+")) {
                close();
                throw new InvalidCSVException("CSV contained header with illegal characters");
            }
        }
    }

    /**
     * Write the next row of the CSV
     * 
     * @throws InvalidCSVException
     */
    public void writeRow(Map<String, String> row) throws InvalidCSVException {

        try {
            writer.write(row, headers);
        }
        // Indicates an error during writing.
        catch (IOException e) {
            throw new InvalidCSVException("Output writer corrupt", e);
        }

    }

    /**
     * @return Array of header Strings
     */
    public String[] getHeaders() {

        return headers;
    }

    /**
     * Disposes of the underlying writer.
     */
    @Override
    public void close() {

        try {
            writer.close();
        } catch (IOException e) {
            logger.warn("Failed to close CSV Parser");
        }
    }

    /**
     * Add additional preferences here for changes in the automatic reader.
     */
    private CsvPreference buildReaderPreferences() {

        return new CsvPreference.Builder(CsvPreference.STANDARD_PREFERENCE)
                // Additional options appended here.
                .surroundingSpacesNeedQuotes(true).build();
    }
}
