package au.edu.curtin.tutortimetable.exception;

/**
 * An exception that indicates a flaw within a CSV row.
 * 
 * @author Cameron Newton
 *
 */
public class CsvParsingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 6333574663778191081L;

    public CsvParsingException() {

        super();
    }

    public CsvParsingException(String msg) {

        super(msg);
    }

    public CsvParsingException(Throwable cause) {

        super(cause);
    }

    public CsvParsingException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
