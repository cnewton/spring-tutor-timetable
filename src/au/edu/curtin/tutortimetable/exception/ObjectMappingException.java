package au.edu.curtin.tutortimetable.exception;

/**
 * An exception that indicates the inability to map an object.
 * 
 * @author Cameron Newton
 *
 */
public class ObjectMappingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6250903730261251158L;

    public ObjectMappingException() {

        super();
    }

    public ObjectMappingException(String msg) {

        super(msg);
    }

    public ObjectMappingException(Throwable cause) {

        super(cause);
    }

    public ObjectMappingException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
