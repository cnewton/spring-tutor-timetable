package au.edu.curtin.tutortimetable.exception;

/**
 * A runtime exception that indicates an error occured while processing an uploaded file.
 * 
 * @author Cameron Newton
 *
 */
public class FileUploadException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -489251255229486429L;

    public FileUploadException() {

        super();
    }

    public FileUploadException(String msg) {

        super(msg);
    }

    public FileUploadException(Throwable cause) {

        super(cause);
    }

    public FileUploadException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
