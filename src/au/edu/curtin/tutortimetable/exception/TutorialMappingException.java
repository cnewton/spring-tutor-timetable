package au.edu.curtin.tutortimetable.exception;

/**
 * An exception that indicates the inability to map a tutorial object.
 * 
 * @author Cameron Newton
 *
 */
public class TutorialMappingException extends ObjectMappingException {

    /**
     * 
     */
    private static final long serialVersionUID = 295957856592084999L;

    public TutorialMappingException() {

        super();
    }

    public TutorialMappingException(String msg) {

        super(msg);
    }

    public TutorialMappingException(Throwable cause) {

        super(cause);
    }

    public TutorialMappingException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
