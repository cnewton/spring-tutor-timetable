package au.edu.curtin.tutortimetable.exception;

/**
 * An exception that indicates the inability to map a Unit object.
 * 
 * @author Cameron Newton
 *
 */
public class UnitMappingException extends ObjectMappingException {

    /**
     * 
     */
    private static final long serialVersionUID = -5552705178638505993L;

    public UnitMappingException() {

        super();
    }

    public UnitMappingException(String msg) {

        super(msg);
    }

    public UnitMappingException(Throwable cause) {

        super(cause);
    }

    public UnitMappingException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
