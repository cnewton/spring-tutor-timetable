package au.edu.curtin.tutortimetable.exception;

/**
 * An exception that indicates a fatal flaw within a CSV file.
 * 
 * @author Cameron Newton
 *
 */
public class InvalidCSVException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1099190119896174634L;

    public InvalidCSVException() {

        super();
    }

    public InvalidCSVException(String msg) {

        super(msg);
    }

    public InvalidCSVException(Throwable cause) {

        super(cause);
    }

    public InvalidCSVException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
