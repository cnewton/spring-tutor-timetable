package au.edu.curtin.tutortimetable.exception;

/**
 * A runtime exception that indicates a failure to successfully handle a security stack operation.
 * 
 * @author Cameron Newton
 *
 */
public class CustomSecurityException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 2464296100731102573L;

    public CustomSecurityException() {

        super();
    }

    public CustomSecurityException(String msg) {

        super(msg);
    }

    public CustomSecurityException(Throwable cause) {

        super(cause);
    }

    public CustomSecurityException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
