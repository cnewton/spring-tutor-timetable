package au.edu.curtin.tutortimetable.exception;

/**
 * A runtime exception that indicates an attempt was made to access a non-existant record within the database.
 * 
 * @author Cameron Newton
 *
 */
public class ObjectNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -5110768851704882561L;

    public ObjectNotFoundException() {

        super();
    }

    public ObjectNotFoundException(String msg) {

        super(msg);
    }

    public ObjectNotFoundException(Throwable cause) {

        super(cause);
    }

    public ObjectNotFoundException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
