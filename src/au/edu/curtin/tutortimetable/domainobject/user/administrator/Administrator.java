package au.edu.curtin.tutortimetable.domainobject.user.administrator;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.UserRole;

@Entity
@Table(name = "Administrators")
public class Administrator extends User {

    @Override
    public List<UserRole> getUserRoles() {

        ArrayList<UserRole> roles = new ArrayList<UserRole>();
        roles.add(UserRole.ROLE_USER);
        roles.add(UserRole.ROLE_ADMIN);
        return roles;
    }
}
