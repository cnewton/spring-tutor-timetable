package au.edu.curtin.tutortimetable.domainobject.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.user.administrator.Administrator;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "Users")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@class")
@JsonSubTypes({ @JsonSubTypes.Type(value = Tutor.class, name = "Tutor"),
        @JsonSubTypes.Type(value = Administrator.class, name = "Administrator"),
        @JsonSubTypes.Type(value = UnitCoordinator.class, name = "Coordinator") })
public class User implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @Column(name = "staffId", length = 15)
    private String staffId;

    @Column(name = "name", length = 90)
    private String name;

    @Column(name = "email", length = 256)
    private String email;

    @Column(name = "mobile", length = 13)
    private String mobile;

    @Column(name = "enabled")
    private boolean enabled = true;

    @Column(name = "username", nullable = false, length = 35)
    private String username;

    // The default password exists for integration testing purposes.
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "password", nullable = false, length = 60)
    private String password = "DefaultPassword";

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getStaffId() {

        return staffId;
    }

    public void setStaffId(String staffId) {

        this.staffId = staffId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getMobile() {

        return mobile;
    }

    public void setMobile(String mobile) {

        this.mobile = mobile;
    }

    public boolean isEnabled() {

        return enabled;
    }

    public void setEnabled(boolean enabled) {

        this.enabled = enabled;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

    public List<UserRole> getUserRoles() {

        ArrayList<UserRole> roles = new ArrayList<UserRole>();
        roles.add(UserRole.ROLE_USER);
        return roles;
    }
}
