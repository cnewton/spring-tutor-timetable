package au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import au.edu.curtin.tutortimetable.domainobject.user.UserRole;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

@Entity
@Table(name = "UnitCoordinators")
public class UnitCoordinator extends Tutor {

    @Override
    public List<UserRole> getUserRoles() {

        ArrayList<UserRole> roles = new ArrayList<UserRole>();
        roles.add(UserRole.ROLE_USER);
        roles.add(UserRole.ROLE_TUTOR);
        roles.add(UserRole.ROLE_COORDINATOR);
        return roles;
    }

}
