package au.edu.curtin.tutortimetable.domainobject.user;

public enum UserRole {

    ROLE_USER("ROLE_USER"), ROLE_TUTOR("ROLE_TUTOR"), ROLE_COORDINATOR("ROLE_COORDINATOR"), ROLE_ADMIN("ROLE_ADMIN");

    private String role;

    private UserRole(String role) {

        this.role = role;
    }

    public String getRole() {

        return role;
    }
}
