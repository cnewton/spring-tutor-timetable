package au.edu.curtin.tutortimetable.domainobject.user.tutor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import au.edu.curtin.tutortimetable.domainobject.StringArrayConverter;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.UserRole;

@Entity
@Table(name = "Tutors")
public class Tutor extends User {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tutor", orphanRemoval = true)
    @JsonManagedReference(value = "tutor-preferred")
    private Set<TutorUnitPreference> preferredUnits = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tutor", orphanRemoval = false)
    @JsonManagedReference(value = "tutor-tutorial")
    private Set<Tutorial> assignedTutorials = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tutor", orphanRemoval = true)
    @JsonManagedReference(value = "tutor-availability")
    private Set<TutorAvailability> availabilities = new HashSet<>();

    @Column(name = "course", length = 90)
    private String course;

    @Column(name = "year")
    private int year;

    @Column(name = "assignedHours")
    private int assignedHours;

    @Column(name = "studentId", length = 15)
    private String studentId;

    @Convert(converter = StringArrayConverter.class)
    private String[] failedUnits;

    public Set<TutorUnitPreference> getPreferredUnits() {

        return preferredUnits;
    }

    public void setPreferredUnits(Set<TutorUnitPreference> preferredUnits) {

        this.preferredUnits = preferredUnits;
    }

    public Set<Tutorial> getAssignedTutorials() {

        return assignedTutorials;
    }

    public void setAssignedTutorials(Set<Tutorial> assignedUnits) {

        this.assignedTutorials = assignedUnits;
    }

    public Set<TutorAvailability> getAvailabilities() {

        return availabilities;
    }

    public void setAvailabilities(Set<TutorAvailability> availabilities) {

        this.availabilities = availabilities;
    }

    public String getCourse() {

        return course;
    }

    public void setCourse(String course) {

        this.course = course;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public int getAssignedHours() {

        return assignedHours;
    }

    public void setAssignedHours(int assignedHours) {

        this.assignedHours = assignedHours;
    }

    public String getStudentId() {

        return studentId;
    }

    public void setStudentId(String studentId) {

        this.studentId = studentId;
    }

    public String[] getFailedUnits() {

        return failedUnits;
    }

    public void setFailedUnits(String[] failedUnits) {

        this.failedUnits = failedUnits;
    }

    @Override
    public List<UserRole> getUserRoles() {

        ArrayList<UserRole> roles = new ArrayList<UserRole>();
        roles.add(UserRole.ROLE_USER);
        roles.add(UserRole.ROLE_TUTOR);
        return roles;
    }

}
