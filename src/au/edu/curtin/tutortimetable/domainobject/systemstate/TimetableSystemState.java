package au.edu.curtin.tutortimetable.domainobject.systemstate;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TimetableSystemState")
public class TimetableSystemState {

    @Id
    @Column(name = "id")
    private long id = 1;

    private LocalDate semesterStart;

    private LocalDate semesterEnd;

    private LocalDate applicationStart;

    private LocalDate applicationEnd;

    private LocalDate lockout;

    private boolean tutorsLocked;

    private boolean coordinatorsLocked;

    private int tutorMaxHours;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public LocalDate getSemesterStart() {

        return semesterStart;
    }

    public void setSemesterStart(LocalDate semesterStart) {

        this.semesterStart = semesterStart;
    }

    public LocalDate getSemesterEnd() {

        return semesterEnd;
    }

    public void setSemesterEnd(LocalDate semesterEnd) {

        this.semesterEnd = semesterEnd;
    }

    public LocalDate getApplicationStart() {

        return applicationStart;
    }

    public void setApplicationStart(LocalDate applicationStart) {

        this.applicationStart = applicationStart;
    }

    public LocalDate getApplicationEnd() {

        return applicationEnd;
    }

    public void setApplicationEnd(LocalDate applicationEnd) {

        this.applicationEnd = applicationEnd;
    }

    public LocalDate getLockout() {

        return lockout;
    }

    public void setLockout(LocalDate lockout) {

        this.lockout = lockout;
    }

    public boolean isTutorsLocked() {

        return tutorsLocked;
    }

    public void setTutorsLocked(boolean tutorsLocked) {

        this.tutorsLocked = tutorsLocked;
    }

    public boolean isCoordinatorsLocked() {

        return coordinatorsLocked;
    }

    public void setCoordinatorsLocked(boolean coordinatorsLocked) {

        this.coordinatorsLocked = coordinatorsLocked;
    }

    public int getTutorMaxHours() {

        return tutorMaxHours;
    }

    public void setTutorMaxHours(int tutorMaxHours) {

        this.tutorMaxHours = tutorMaxHours;
    }
}
