package au.edu.curtin.tutortimetable.domainobject.tutorialgroup;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TutorialType {

    @JsonProperty("Tutorial") TUTORIAL("Tutorial"), @JsonProperty("Lecture") LECTURE(
            "Lecture"), @JsonProperty("Workshop") WORKSHOP(
                    "Workshop"), @JsonProperty("Computer Laboratory") COMPUTER_LABORATORY(
                            "Computer Laboratory"), @JsonProperty("Seminar") SEMINAR(
                                    "Seminar"), @JsonProperty("Practical") PRACTICAL(
                                            "Practical"), @JsonProperty("Laboratory") LABORATORY(
                                                    "Laboratory"), @JsonProperty("Demonstration") DEMONSTRATION(
                                                            "Demonstration");

    private String type;

    private TutorialType(String type) {

        this.type = type;
    }

    public String getType() {

        return type;
    }
}