package au.edu.curtin.tutortimetable.domainobject.tutorialgroup;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;

@Entity
@Table(name = "TutorialGroups")
public class TutorialGroup implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unitId", nullable = false)
    private Unit unit;

    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tutorialGroup", orphanRemoval = true)
    @JsonIgnore
    private Set<Tutorial> tutorials = new HashSet<>();

    @Column(name = "groupName", length = 35)
    private String groupName;

    @Enumerated(EnumType.STRING)
    @Column(name = "tutorialType")
    private TutorialType tutorialType;

    public long getId() {

        return id;
    }

    public Unit getUnit() {

        return unit;
    }

    public Set<Tutorial> getTutorials() {

        return tutorials;
    }

    public String getGroupName() {

        return groupName;
    }

    public TutorialType getTutorialType() {

        return tutorialType;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setUnit(Unit unit) {

        this.unit = unit;
    }

    public void setTutorials(Set<Tutorial> tutorials) {

        this.tutorials = tutorials;
    }

    public void setGroupName(String groupName) {

        this.groupName = groupName;
    }

    public void setTutorialType(TutorialType tutorialType) {

        this.tutorialType = tutorialType;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
