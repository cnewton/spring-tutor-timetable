package au.edu.curtin.tutortimetable.domainobject.viewmodel;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * A View model for transfering tutor details without the entire Tutor object.
 * 
 * @author Cameron Newton
 *
 */
public class TutorDetailsDTO {

    private String course;

    private int year;

    private String[] failedUnits;

    public String getCourse() {

        return course;
    }

    public void setCourse(String course) {

        this.course = course;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public String[] getFailedUnits() {

        return failedUnits;
    }

    public void setFailedUnits(String[] failedUnits) {

        this.failedUnits = failedUnits;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
