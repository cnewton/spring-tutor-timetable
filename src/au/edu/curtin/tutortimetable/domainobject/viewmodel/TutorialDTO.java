package au.edu.curtin.tutortimetable.domainobject.viewmodel;

import java.time.LocalDateTime;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;

/**
 * A viewmodel for transfering a tutorial's details along with a tutorId and a groupId.
 * 
 * @author Cameron Newton
 *
 */
public class TutorialDTO {

    private long id;

    private long tutorId;

    private long groupId;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private String room;

    public TutorialDTO(Tutorial tutorial) {

        this.id = tutorial.getId();
        this.startTime = tutorial.getStartTime();
        this.endTime = tutorial.getEndTime();
        this.room = tutorial.getRoom();

        if (tutorial.getTutor() != null) {
            this.tutorId = tutorial.getTutor().getId();
        }

        if (tutorial.getTutorialGroup() != null) {
            this.groupId = tutorial.getTutorialGroup().getId();
        }
    }

    public long getId() {

        return id;
    }

    public void setId(long tutorialId) {

        this.id = tutorialId;
    }

    public long getTutorId() {

        return tutorId;
    }

    public void setTutorId(long tutorId) {

        this.tutorId = tutorId;
    }

    public long getGroupId() {

        return groupId;
    }

    public void setGroupId(long groupId) {

        this.groupId = groupId;
    }

    public LocalDateTime getStartTime() {

        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {

        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {

        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {

        this.endTime = endTime;
    }

    public String getRoom() {

        return room;
    }

    public void setRoom(String room) {

        this.room = room;
    }

}
