package au.edu.curtin.tutortimetable.domainobject.viewmodel;

import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

/**
 * A viewmodel for transfering a tutors details along with a unit preference and priority.
 * 
 * @author Cameron Newton
 *
 */
public class TutorUnitPreferenceDTO {

    private long id;

    private String name;

    private String email;

    private String mobile;

    private String staffId;

    private String studentId;

    private String course;

    private int year;

    private int assignedHours;

    private String[] failedUnits;

    private long unitId;

    private long preferenceId;

    private int priority;

    public TutorUnitPreferenceDTO(TutorUnitPreference pref) {

        Tutor tutor = pref.getTutor();
        this.id = tutor.getId();
        this.name = tutor.getName();
        this.email = tutor.getEmail();
        this.mobile = tutor.getMobile();
        this.staffId = tutor.getStaffId();
        this.studentId = tutor.getStudentId();
        this.course = tutor.getCourse();
        this.year = tutor.getYear();
        this.setAssignedHours(tutor.getAssignedHours());
        this.failedUnits = tutor.getFailedUnits();
        this.unitId = pref.getUnit().getId();
        this.preferenceId = pref.getId();
        this.priority = pref.getPriority();
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getMobile() {

        return mobile;
    }

    public void setMobile(String mobile) {

        this.mobile = mobile;
    }

    public String getStaffId() {

        return staffId;
    }

    public void setStaffId(String staffId) {

        this.staffId = staffId;
    }

    public String getStudentId() {

        return studentId;
    }

    public void setStudentId(String studentId) {

        this.studentId = studentId;
    }

    public String getCourse() {

        return course;
    }

    public void setCourse(String course) {

        this.course = course;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public int getAssignedHours() {

        return assignedHours;
    }

    public void setAssignedHours(int assignedHours) {

        this.assignedHours = assignedHours;
    }

    public String[] getFailedUnits() {

        return failedUnits;
    }

    public void setFailedUnits(String[] failedUnits) {

        this.failedUnits = failedUnits;
    }

    public long getUnitId() {

        return unitId;
    }

    public void setUnitId(long unitId) {

        this.unitId = unitId;
    }

    public int getPriority() {

        return priority;
    }

    public void setPriority(int priority) {

        this.priority = priority;
    }

    public long getPreferenceId() {

        return preferenceId;
    }

    public void setPreferenceId(long preferenceId) {

        this.preferenceId = preferenceId;
    }

}
