package au.edu.curtin.tutortimetable.domainobject.viewmodel;

import java.util.ArrayList;
import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;

/**
 * A viewmodel for transfering a TutorialGroup's details along with an internal TutorialDTO.
 * 
 * @author Cameron Newton
 *
 */
public class TutorialGroupDTO {

    private long id;

    private String groupName;

    private String tutorialType;

    private List<TutorialDTO> tutorials = new ArrayList<>();

    public TutorialGroupDTO(TutorialGroup group) {

        this.id = group.getId();
        this.groupName = group.getGroupName();
        this.tutorialType = group.getTutorialType().getType();

        for (Tutorial tutorial : group.getTutorials()) {
            tutorials.add(new TutorialDTO(tutorial));
        }
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public String getGroupName() {

        return groupName;
    }

    public void setGroupName(String groupName) {

        this.groupName = groupName;
    }

    public String getTutorialType() {

        return tutorialType;
    }

    public void setTutorialType(String tutorialType) {

        this.tutorialType = tutorialType;
    }

    public List<TutorialDTO> getTutorials() {

        return tutorials;
    }

    public void setTutorials(List<TutorialDTO> tutorials) {

        this.tutorials = tutorials;
    }
}
