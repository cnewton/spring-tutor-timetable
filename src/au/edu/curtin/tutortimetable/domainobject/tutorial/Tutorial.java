package au.edu.curtin.tutortimetable.domainobject.tutorial;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

@Entity
@Table(name = "Tutorials")
public class Tutorial implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "groupId", nullable = false)
    private TutorialGroup tutorialGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tutorId", nullable = true)
    @JsonBackReference(value = "tutor-tutorial")
    private Tutor tutor;

    @Column(name = "startTime")
    private LocalDateTime startTime;

    @Column(name = "endTime")
    private LocalDateTime endTime;

    @Column(name = "room", length = 35)
    private String room;

    public long getId() {

        return id;
    }

    public TutorialGroup getTutorialGroup() {

        return tutorialGroup;
    }

    public LocalDateTime getStartTime() {

        return startTime;
    }

    public LocalDateTime getEndTime() {

        return endTime;
    }

    public String getRoom() {

        return room;
    }

    public Tutor getTutor() {

        return tutor;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setTutorialGroup(TutorialGroup tutorialGroup) {

        this.tutorialGroup = tutorialGroup;
    }

    public void setStartTime(LocalDateTime startTime) {

        this.startTime = startTime;
    }

    public void setEndTime(LocalDateTime endTime) {

        this.endTime = endTime;
    }

    public void setRoom(String room) {

        this.room = room;
    }

    public void setTutor(Tutor tutor) {

        this.tutor = tutor;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
