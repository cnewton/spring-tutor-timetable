package au.edu.curtin.tutortimetable.domainobject.tutorunitpreference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

@Entity
@Table(name = "TutorUnitPreferences")
public class TutorUnitPreference implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "unitId", nullable = false)
    private Unit unit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tutorId", nullable = false)
    @JsonBackReference(value = "tutor-preferred")
    private Tutor tutor;

    @Column(name = "priority")
    @Min(value = -1)
    @Max(value = 20)
    private int priority = 0;

    public long getId() {

        return id;
    }

    public void setId(long id) {

        this.id = id;
    }

    public Unit getUnit() {

        return unit;
    }

    public void setUnit(Unit unit) {

        this.unit = unit;
    }

    public Tutor getTutor() {

        return tutor;
    }

    public void setTutor(Tutor tutor) {

        this.tutor = tutor;
    }

    public int getPriority() {

        return priority;
    }

    public void setPriority(int priority) {

        this.priority = priority;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
