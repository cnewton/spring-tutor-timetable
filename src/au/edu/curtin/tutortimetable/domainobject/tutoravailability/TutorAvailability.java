package au.edu.curtin.tutortimetable.domainobject.tutoravailability;

import java.time.DayOfWeek;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;

@Entity
@Table(name = "TutorAvailabilities")
public class TutorAvailability implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tutorId", nullable = false)
    @JsonBackReference(value = "tutor-availability")
    private Tutor tutor;

    @Enumerated(EnumType.STRING)
    private DayOfWeek day;

    private LocalTime startTime;

    private LocalTime endTime;

    public long getId() {

        return id;
    }

    public Tutor getTutor() {

        return tutor;
    }

    public DayOfWeek getDay() {

        return day;
    }

    public LocalTime getStartTime() {

        return startTime;
    }

    public LocalTime getEndTime() {

        return endTime;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setTutor(Tutor tutor) {

        this.tutor = tutor;
    }

    public void setDay(DayOfWeek day) {

        this.day = day;
    }

    public void setStartTime(LocalTime startTime) {

        this.startTime = startTime;
    }

    public void setEndTime(LocalTime endTime) {

        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }
}
