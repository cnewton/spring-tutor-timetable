package au.edu.curtin.tutortimetable.domainobject.unit;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;

@Entity
@Table(name = "Units")
public class Unit implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unitCoordinatorId", nullable = true)
    private UnitCoordinator unitCoordinator;

    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "unit", orphanRemoval = true)
    @JsonIgnore
    private Set<TutorialGroup> tutorialGroups = new HashSet<>();

    @Column(name = "unitName", length = 90)
    private String unitName;

    @Column(name = "unitCode", length = 15)
    private String unitCode;

    public long getId() {

        return id;
    }

    public UnitCoordinator getUnitCoordinator() {

        return unitCoordinator;
    }

    public Set<TutorialGroup> getTutorialGroups() {

        return tutorialGroups;
    }

    public String getUnitName() {

        return unitName;
    }

    public String getUnitCode() {

        return unitCode;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setUnitCoordinator(UnitCoordinator unitCoordinator) {

        this.unitCoordinator = unitCoordinator;
    }

    public void setTutorialGroups(Set<TutorialGroup> tutorialGroups) {

        this.tutorialGroups = tutorialGroups;
    }

    public void setUnitName(String unitName) {

        this.unitName = unitName;
    }

    public void setUnitCode(String unitCode) {

        this.unitCode = unitCode;
    }

    @Override
    public boolean equals(Object obj) {

        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
