package au.edu.curtin.tutortimetable.domainobject;

public interface IdBasedDomainObject {

    public long getId();

    public void setId(long id);

}
