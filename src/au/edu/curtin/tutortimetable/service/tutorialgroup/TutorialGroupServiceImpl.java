package au.edu.curtin.tutortimetable.service.tutorialgroup;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorialgroup.TutorialGroupDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorialGroupDTO;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class TutorialGroupServiceImpl extends DomainObjectServiceImpl<TutorialGroup> implements TutorialGroupService {

    @Autowired
    private TutorialGroupDAO tutorialGroupDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Override
    @Transactional
    public void assignTutorialGroupToTutor(long groupId, long tutorId) {

        TutorialGroup tutorialGroup = find(groupId);
        if (tutorialGroup.getTutorials().size() > 0) {

            Tutor tutor = userService.findTutor(tutorId);

            Tutorial t1 = tutorialGroup.getTutorials().iterator().next();

            tutor.setAssignedHours(
                    tutor.getAssignedHours() + (t1.getEndTime().getHour() - t1.getStartTime().getHour()));

            for (Tutorial tutorial : tutorialGroup.getTutorials()) {
                tutorial.setTutor(tutor);
                tutor.getAssignedTutorials().add(tutorial);
            }
        }
    }

    @Override
    @Transactional
    public TutorialGroup findByName(String groupName) {

        TutorialGroup group = tutorialGroupDAO.findByName(groupName);
        return group;
    }

    @Override
    @Transactional
    public List<TutorialGroupDTO> loadTutorialGroupsForUnit(long unitId) {

        Unit unit = unitService.find(unitId);

        List<TutorialGroupDTO> groups = new ArrayList<>();

        for (TutorialGroup grp : tutorialGroupDAO.loadTutorialGroupsForUnit(unit)) {
            groups.add(new TutorialGroupDTO(grp));
        }

        return groups;
    }

    @Override
    @Transactional
    public void clearAssignmentForTutorialGroup(long groupId) {

        TutorialGroup tutorialGroup = find(groupId);

        if (tutorialGroup.getTutorials().size() > 0) {

            Tutorial t1 = tutorialGroup.getTutorials().iterator().next();
            Tutor tutor = t1.getTutor();

            if (tutor != null) {
                tutor.setAssignedHours(
                        Math.max(0,
                                tutor.getAssignedHours() - (t1.getEndTime().getHour() - t1.getStartTime().getHour())));
            }

            for (Tutorial tutorial : tutorialGroup.getTutorials()) {
                tutorial.setTutor(null);
            }
        }
    }
}
