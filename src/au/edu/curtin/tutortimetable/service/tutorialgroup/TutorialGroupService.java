package au.edu.curtin.tutortimetable.service.tutorialgroup;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorialGroupDTO;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface TutorialGroupService extends DomainObjectService<TutorialGroup> {

    public void assignTutorialGroupToTutor(long groupId, long tutorId);

    public TutorialGroup findByName(String groupName);

    public List<TutorialGroupDTO> loadTutorialGroupsForUnit(long unitId);

    public void clearAssignmentForTutorialGroup(long groupId);

}
