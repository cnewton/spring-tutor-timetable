package au.edu.curtin.tutortimetable.service.unit;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface UnitService extends DomainObjectService<Unit> {

    public List<Unit> findUnitByCode(String string);

    public List<Unit> getUnitsForCoordinator(long userId);

    public List<Unit> loadAllWithCoordinator();

    public void assignCoordinator(long unitId, long coordinatorId);

    public List<Unit> loadAllWithGroups();

}
