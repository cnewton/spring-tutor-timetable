package au.edu.curtin.tutortimetable.service.unit;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.unit.UnitDAO;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class UnitServiceImpl extends DomainObjectServiceImpl<Unit> implements UnitService {

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private UserService userService;

    /**
     * For this specific instance the method returns null when the object is not found.
     */
    @Override
    @Transactional
    public List<Unit> findUnitByCode(String code) {

        return unitDAO.findUnitByCode(code);
    }

    @Override
    @Transactional
    public List<Unit> getUnitsForCoordinator(long userId) {

        UnitCoordinator coordinator = userService.findCoordinator(userId);

        return unitDAO.findAllForCoordinator(coordinator);
    }

    @Override
    @Transactional
    public List<Unit> loadAllWithCoordinator() {

        return unitDAO.loadAllWithCoordinator();
    }

    @Override
    @Transactional
    public void assignCoordinator(long unitId, long coordinatorId) {

        Unit unit = find(unitId);

        UnitCoordinator coordinator = userService.findCoordinator(coordinatorId);

        unit.setUnitCoordinator(coordinator);
        update(unit);
    }

    @Override
    public List<Unit> loadAllWithGroups() {

        return unitDAO.loadAllWithGroups();
    }

}
