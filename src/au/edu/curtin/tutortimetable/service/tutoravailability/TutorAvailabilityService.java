package au.edu.curtin.tutortimetable.service.tutoravailability;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface TutorAvailabilityService extends DomainObjectService<TutorAvailability> {

    public void listAddAvailabilities(long tutorId, List<TutorAvailability> availabilities);

    public void listSubtractAvailabilities(long tutorId, long[] availabilityIds);

}
