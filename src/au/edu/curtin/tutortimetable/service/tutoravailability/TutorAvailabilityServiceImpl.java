package au.edu.curtin.tutortimetable.service.tutoravailability;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class TutorAvailabilityServiceImpl extends DomainObjectServiceImpl<TutorAvailability> implements
        TutorAvailabilityService {

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public void listAddAvailabilities(long tutorId, List<TutorAvailability> availabilities) {

        Tutor tutor = userService.findTutor(tutorId);

        for (TutorAvailability avail : availabilities) {
            avail.setTutor(tutor);
            create(avail);
        }
    }

    @Override
    @Transactional
    public void listSubtractAvailabilities(long tutorId, long[] availabilityIds) {

        Tutor tutor = userService.findTutor(tutorId);

        for (long availId : availabilityIds) {
            for (TutorAvailability avail : tutor.getAvailabilities()) {
                if (availId == avail.getId()) {
                    delete(avail);
                    break;
                }
            }
        }
    }

}
