package au.edu.curtin.tutortimetable.service.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.administration.AdministrationService;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    protected static final Logger logger = LogManager.getLogger(AssignmentServiceImpl.class);

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private TutorUnitPreferenceService preferenceService;

    @Autowired
    private AdministrationService administrationService;

    @Override
    @Transactional
    public void assignTutorialsGreedily() {

        List<TutorUnitPreference> preferences = preferenceService.findAll();

        // Filter out preferences with no valid priority.
        Stream<TutorUnitPreference> filteredPreferences = preferences.stream().filter(p -> p.getPriority() > 0);
        // Sort preferences by priority.
        filteredPreferences = filteredPreferences.sorted((p1, p2) -> p1.getPriority() - p2.getPriority());

        // Map preferences into Unit -> tutorList
        Map<Unit, List<Tutor>> unitSortMap = new HashMap<>();
        filteredPreferences.forEach(p -> {
            List<Tutor> tutors = unitSortMap.get(p.getUnit());
            if (tutors == null) {
                tutors = new ArrayList<Tutor>();
                unitSortMap.put(p.getUnit(), tutors);
            }
            tutors.add(p.getTutor());
        });

        // Sorts the list of units in Ascending number of applicants order.
        List<Entry<Unit, List<Tutor>>> entryList = new ArrayList<>(unitSortMap.entrySet());
        entryList.sort((a, b) -> a.getValue().size() - b.getValue().size());

        // For each tutorial group attempt to assign a tutor.
        for (Entry<Unit, List<Tutor>> entry : entryList) {
            for (TutorialGroup group : entry.getKey().getTutorialGroups()) {
                assignTutorialGroupToTutors(group, entry.getValue());
            }
        }
    }

    /*
     * Find and assign a compatible tutor to each of the tutorials within the given group.
     */
    private void assignTutorialGroupToTutors(TutorialGroup group, List<Tutor> tutors) {

        List<List<Tutorial>> tutorialSets = new ArrayList<>();
        // Splits the tutorials of this group into lists of common times.
        for (Tutorial tutorial : group.getTutorials()) {
            // Ignore tutorials that already have a tutor.
            if (tutorial.getTutor() == null) {
                boolean matched = false;
                // Find the appropriate list (timeslot group) to add a tutorial to.
                for (List<Tutorial> list : tutorialSets) {
                    if (list.get(0).getStartTime().toLocalTime().equals(tutorial.getStartTime().toLocalTime())
                            && list.get(0).getStartTime().getDayOfWeek()
                                    .equals(tutorial.getStartTime().getDayOfWeek())) {
                        matched = true;
                        list.add(tutorial);
                    }
                }
                // Create a new list (timeslot group) for this tutorial.
                if (!matched) {
                    List<Tutorial> subList = new ArrayList<>();
                    subList.add(tutorial);
                    tutorialSets.add(subList);
                }
            }
        }
        
        tutorialSets = splitDuplicatedTutorials(tutorialSets);

        for (List<Tutorial> list : tutorialSets) {
            assignTutorialListToTutors(list, tutors);
        }
    }

    /*
     * Separate duplicated tutorials (Handling tutorials needing 2 tutors)
     */
    private List<List<Tutorial>> splitDuplicatedTutorials(List<List<Tutorial>> tutorialSets) {

        List<List<Tutorial>> splitSets = new ArrayList<>();

        for (List<Tutorial> list : tutorialSets) {
            splitSets.add(list);

            if (list.size() > 1) {
                list.sort((t1, t2) -> t1.getStartTime().compareTo(t2.getStartTime()));
                // If the first 2 elements are duplicated. (Indicates duplicated tutorial)
                if (list.get(0).getStartTime().equals(list.get(1).getStartTime())) {
                    
                    // Split elements into 2 sublists.
                    List<Tutorial> subList1 = new ArrayList<>();
                    List<Tutorial> subList2 = new ArrayList<>();
                    boolean even = true;
                    
                    for (Tutorial tute : list) {
                        if (even) {
                            subList1.add(tute);
                        } else {
                            subList2.add(tute);
                        }
                        even = !even;
                    }
                    // Update aggregate list.
                    splitSets.remove(list);
                    splitSets.add(subList1);
                    splitSets.add(subList2);
                }
            }
        }
        return splitSets;
    }

    /*
     * Find an available tutor and assign them to each tutorial in the list, update tutor availabilities. Note:
     * tutorials given to this method should be homogenous in weekly times.f
     */
    private boolean assignTutorialListToTutors(List<Tutorial> tutorials, List<Tutor> tutors) {

        if (tutorials.size() > 0) {
            Tutorial tutorial = tutorials.get(0);
            TutorAvailability avail = findAvailableTutor(tutorial, tutors);
            if (avail != null) {

                Tutor tutor = avail.getTutor();

                int hoursToAllocate = (tutorial.getEndTime().getHour() - tutorial.getStartTime().getHour());

                tutor.setAssignedHours(tutor.getAssignedHours() + hoursToAllocate);

                for (Tutorial tute : tutorials) {
                    tute.setTutor(tutor);
                    tutor.getAssignedTutorials().add(tute);
                }

                if (avail.getStartTime().equals(tutorial.getStartTime().toLocalTime())) {

                    if (avail.getEndTime().equals(tutorial.getEndTime().toLocalTime())) {
                        avail.getTutor().getAvailabilities().remove(avail);
                    } else {
                        avail.setStartTime(tutorial.getEndTime().toLocalTime());
                    }

                } else {

                    if (avail.getEndTime().equals(tutorial.getEndTime().toLocalTime())) {
                        avail.setEndTime(tutorial.getStartTime().toLocalTime());
                    } else {
                        TutorAvailability newAvail = new TutorAvailability();
                        newAvail.setDay(avail.getDay());
                        newAvail.setStartTime(tutorial.getEndTime().toLocalTime());
                        newAvail.setEndTime(avail.getEndTime());
                        newAvail.setTutor(avail.getTutor());
                        tutorAvailabilityService.create(newAvail);
                        avail.getTutor().getAvailabilities().add(newAvail);
                        avail.setEndTime(tutorial.getStartTime().toLocalTime());
                    }
                }
                return true;
            }
        }
        return false;
    }

    /*
     * Find an appropriate tutor for the given tutorial
     */
    private TutorAvailability findAvailableTutor(Tutorial tutorial, List<Tutor> tutors) {

        TimetableSystemState state = administrationService.getSystemState();

        for (Tutor tutor : tutors) {

            // Only assign to tutors with less than max hours.
            if (tutor.getAssignedHours() < state.getTutorMaxHours()) {

                // Find a fitting availability.
                for (TutorAvailability avail : tutor.getAvailabilities()) {

                    // Match Days
                    if (avail.getDay().equals(tutorial.getStartTime().getDayOfWeek())) {

                        // Match start times.
                        if (avail.getStartTime().compareTo(tutorial.getStartTime().toLocalTime()) <= 0) {

                            // Match end times.
                            if (avail.getEndTime().compareTo(tutorial.getEndTime().toLocalTime()) >= 0) {

                                return avail;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
