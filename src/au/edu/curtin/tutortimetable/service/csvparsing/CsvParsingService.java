package au.edu.curtin.tutortimetable.service.csvparsing;

import java.io.InputStream;

import au.edu.curtin.tutortimetable.exception.InvalidCSVException;

public interface CsvParsingService {

    public void parseUnitCsv(InputStream fileStream) throws InvalidCSVException;

    public void parseTutorialCsv(InputStream fileStream) throws InvalidCSVException;

    public byte[] exportUnitCsv() throws InvalidCSVException;

    public byte[] exportTutorialCsv() throws InvalidCSVException;

}
