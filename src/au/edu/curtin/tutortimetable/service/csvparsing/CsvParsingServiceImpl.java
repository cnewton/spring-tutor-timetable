package au.edu.curtin.tutortimetable.service.csvparsing;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialCsvStaticField;
import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialMapper;
import au.edu.curtin.tutortimetable.csv.mapper.unit.UnitCsvStaticField;
import au.edu.curtin.tutortimetable.csv.mapper.unit.UnitMapper;
import au.edu.curtin.tutortimetable.csv.parser.CsvExportParser;
import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.exception.ObjectMappingException;
import au.edu.curtin.tutortimetable.exception.TutorialMappingException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class CsvParsingServiceImpl implements CsvParsingService {

    protected static final Logger logger = LogManager.getLogger(CsvParsingServiceImpl.class);

    @Autowired
    private UnitService unitService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public void parseUnitCsv(InputStream fileStream) throws InvalidCSVException {

        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(fileStream))) {

            if (!UnitMapper.validateHeaders(parser.getHeaders())) {
                throw new InvalidCSVException("The supplied CSV does not contain valid headers for a UnitCSV.");
            }

            UnitMapper mapper = new UnitMapper(userService.findAllCoordinators());

            Map<String, String> unitMap;
            while (!parser.isEOF()) {
                try {
                    unitMap = parser.parseRow();
                    if (unitMap != null) {
                        unitService.create(mapper.map(unitMap));
                    }
                } catch (CsvParsingException | ObjectMappingException e) {
                    logger.warn(e.getMessage());
                }
            }
        }
    }

    @Override
    @Transactional
    public void parseTutorialCsv(InputStream fileStream) throws InvalidCSVException {

        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(fileStream))) {

            if (!TutorialMapper.validateHeaders(parser.getHeaders())) {
                throw new InvalidCSVException("The supplied CSV does not contain valid headers for a UnitCSV.");
            }

            TutorialMapper mapper = new TutorialMapper(unitService.loadAllWithGroups());

            Map<String, String> tutorialMap;
            while (!parser.isEOF()) {
                try {
                    tutorialMap = parser.parseRow();
                    if (tutorialMap != null) {

                        Tutorial tutorial = mapper.map(tutorialMap);

                        // If the tutorial group is new
                        if (tutorial.getTutorialGroup().getId() == 0) {

                            // Create the tutorial group instead of the tutorial (tutorial will be included)
                            tutorialGroupService.create(tutorial.getTutorialGroup());
                        } else {
                            tutorialService.create(tutorial);
                        }
                    }
                } catch (CsvParsingException | ObjectMappingException e) {
                    logger.warn(e.getMessage());
                }
            }
        }
    }

    @Override
    @Transactional
    public byte[] exportUnitCsv() throws InvalidCSVException {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(stream);

        try (CsvExportParser parser = new CsvExportParser(writer, UnitCsvStaticField.stringValues())) {
            List<Unit> units = unitService.findAll();
            for (Unit unit : units) {
                Map<String, String> row = UnitMapper.map(unit);
                parser.writeRow(row);
            }

        }

        return stream.toByteArray();
    }

    @Override
    @Transactional
    public byte[] exportTutorialCsv() throws InvalidCSVException {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(stream);

        try (CsvExportParser parser = new CsvExportParser(writer, TutorialCsvStaticField.stringValues())) {
            List<Tutorial> tutorials = tutorialService.loadAll();
            for (Tutorial tutorial : tutorials) {
                try {
                    Map<String, String> row = TutorialMapper.map(tutorial);
                    parser.writeRow(row);
                } catch (TutorialMappingException e) {
                    logger.warn(e.getMessage());
                }
            }

        }

        return stream.toByteArray();
    }
}
