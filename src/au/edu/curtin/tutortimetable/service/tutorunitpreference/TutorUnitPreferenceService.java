package au.edu.curtin.tutortimetable.service.tutorunitpreference;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorUnitPreferenceDTO;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface TutorUnitPreferenceService extends DomainObjectService<TutorUnitPreference> {

    public TutorUnitPreference createTutorPreference(long tutorId, long unitId);

    public TutorUnitPreference updateTutorPreferencePriority(long preferenceId, int priority);

    public void listAddTutorPreferences(long tutorId, long[] unitIds);

    public void listSubtractTutorPreferences(long tutorId, long[] unitIds);

    public List<TutorUnitPreferenceDTO> loadTutorPreferencesForUnit(long unitId);
}
