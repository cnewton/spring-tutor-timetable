package au.edu.curtin.tutortimetable.service.tutorunitpreference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorunitpreference.TutorUnitPreferenceDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorUnitPreferenceDTO;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class TutorUnitPreferenceServiceImpl extends DomainObjectServiceImpl<TutorUnitPreference> implements
        TutorUnitPreferenceService {

    @Autowired
    private TutorUnitPreferenceDAO preferenceDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Override
    @Transactional
    public TutorUnitPreference find(long id) {

        TutorUnitPreference obj = preferenceDAO.find(id);

        if (obj == null) {
            throw new ObjectNotFoundException("No Object with id: " + id + " exists.");
        }

        // Initialize Lazy Vars.
        if (obj.getTutor() != null) {
            obj.getTutor().getId();
        }
        if (obj.getUnit() != null) {
            obj.getUnit().getId();
        }

        return obj;
    }

    @Override
    @Transactional
    public TutorUnitPreference createTutorPreference(long tutorId, long unitId) {

        Tutor tutor = userService.findTutor(tutorId);
        Unit unit = unitService.find(unitId);

        TutorUnitPreference preference = new TutorUnitPreference();
        preference.setTutor(tutor);
        preference.setUnit(unit);

        return create(preference);
    }

    @Override
    @Transactional
    public TutorUnitPreference updateTutorPreferencePriority(long preferenceId, int priority) {

        TutorUnitPreference preference = find(preferenceId);
        preference.setPriority(priority);

        return update(preference);
    }

    /**
     * Create preferences for the given tutor for each given unit (by id)
     * 
     * @param tutorId
     * @param unitIds
     */
    @Override
    @Transactional
    public void listAddTutorPreferences(long tutorId, long[] unitIds) {

        Tutor tutor = userService.findTutor(tutorId);

        for (long unitId : unitIds) {
            // Only add preference if it does not yet exist.
            if (findPreferenceForUnitId(unitId, tutor.getPreferredUnits()) == null) {
                Unit unit = unitService.find(unitId);
                TutorUnitPreference preference = new TutorUnitPreference();
                preference.setTutor(tutor);
                preference.setUnit(unit);
                create(preference);
            }
        }
    }

    /**
     * Delete preferences for the given tutor for each given unit (by id)
     * 
     * @param tutorId
     * @param unitIds
     */
    @Override
    @Transactional
    public void listSubtractTutorPreferences(long tutorId, long[] unitIds) {

        Tutor tutor = userService.findTutor(tutorId);

        for (long unitId : unitIds) {
            TutorUnitPreference pref = findPreferenceForUnitId(unitId, tutor.getPreferredUnits());
            if (pref != null) {
                delete(pref);
            }
        }
    }

    @Override
    @Transactional
    public List<TutorUnitPreferenceDTO> loadTutorPreferencesForUnit(long unitId) {

        Unit unit = unitService.find(unitId);

        List<TutorUnitPreference> preferences = preferenceDAO.loadForUnit(unit);
        List<TutorUnitPreferenceDTO> dtos = new ArrayList<>();

        for (TutorUnitPreference pref : preferences) {
            dtos.add(new TutorUnitPreferenceDTO(pref));
        }

        return dtos;
    }

    private TutorUnitPreference findPreferenceForUnitId(long unitId, Collection<TutorUnitPreference> preferences) {

        for (TutorUnitPreference pref : preferences) {
            if (pref.getUnit().getId() == unitId) {
                return pref;
            }
        }
        return null;
    }

}
