package au.edu.curtin.tutortimetable.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;

public class DomainObjectServiceImpl<T extends IdBasedDomainObject> implements DomainObjectService<T> {

    @Autowired
    private DomainObjectDAO<T> dao;

    @Override
    @Transactional
    public T find(long id) {

        T obj = dao.find(id);

        if (obj == null) {
            throw new ObjectNotFoundException("No Object with id: " + id + " exists.");
        }
        return obj;
    }

    @Override
    @Transactional
    public List<T> findAll() {

        return dao.findAll();
    }

    @Override
    @Transactional
    public T create(T obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        dao.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public T update(T obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!dao.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No Object with id: " + obj.getId() + " exists.");
        }

        dao.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(T obj) {

        dao.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        dao.delete(find(id));
    }

    @Override
    @Transactional
    public long count() {

        return dao.count();
    }

}
