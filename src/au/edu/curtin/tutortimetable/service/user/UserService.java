package au.edu.curtin.tutortimetable.service.user;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface UserService extends DomainObjectService<User> {

    public User findByUsername(String institutionId);

    public User updateUserDetails(User user);

    public void updateUserPassword(long id, String oldPassword, String newPassword);

    public void forceUpdateUserPassword(long id, String newPassword);

    public Tutor findTutor(long tutorId);

    public UnitCoordinator findCoordinatorByName(String name);

    public UnitCoordinator findCoordinatorByStaffId(String staffId);

    public Tutor loadTutor(long id);

    public void updateTutorDetails(long tutorId, TutorDetailsDTO tutorDTO);

    public UnitCoordinator findCoordinator(long id);

    public List<UnitCoordinator> findAllCoordinators();

    public List<Tutor> findAllTutors();

    public void setTutorAccountsEnabled(boolean lock);

    public void setCoordinatorAccountsEnabled(boolean lock);

}
