package au.edu.curtin.tutortimetable.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.user.UserDAO;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.exception.CustomSecurityException;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;

@Service
public class UserServiceImpl extends DomainObjectServiceImpl<User> implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    @Transactional
    // Must be overridden to hash password.
    public User create(User obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        if (obj.getUsername() == null || obj.getUsername().equals("")) {
            throw new NotCreateOperationException("The username was empty.");
        }
        if (userDAO.findByUsername(obj.getUsername()) != null) {
            throw new NotCreateOperationException("The username is already taken.");
        }
        obj.setPassword(passwordEncoder.encode(obj.getPassword()));
        userDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    // Used to update basic user details.
    public User updateUserDetails(User user) {

        if (user.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        User dbUser = find(user.getId());

        dbUser.setName(user.getName());
        dbUser.setEmail(user.getEmail());
        dbUser.setMobile(user.getMobile());
        dbUser.setStaffId(user.getStaffId());

        // Less than ideal inclusion of a tutor only field.
        if (user instanceof Tutor && dbUser instanceof Tutor) {
            ((Tutor) dbUser).setStudentId(((Tutor) user).getStudentId());
        }

        update(dbUser);

        return dbUser;
    }

    @Override
    @Transactional
    // Used to update tutor application details
    public void updateTutorDetails(long tutorId, TutorDetailsDTO tutorDTO) {

        if (tutorId == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        Tutor dbUser = findTutor(tutorId);
        dbUser.setCourse(tutorDTO.getCourse());
        dbUser.setFailedUnits(tutorDTO.getFailedUnits());
        dbUser.setYear(tutorDTO.getYear());
        update(dbUser);
    }

    @Override
    @Transactional
    public void updateUserPassword(long id, String oldPassword, String newPassword) {

        User user = find(id);

        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new CustomSecurityException("Incorrect old password.");
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        update(user);

    }

    @Override
    @Transactional
    // Intended for admin use only.
    public void forceUpdateUserPassword(long id, String newPassword) {

        User user = find(id);
        user.setPassword(passwordEncoder.encode(newPassword));
        update(user);
    }

    @Override
    @Transactional
    public User findByUsername(String username) {

        User user = userDAO.findByUsername(username);

        if (user == null) {
            throw new ObjectNotFoundException("No User with username: " + username + " exists.");
        }
        return user;
    }

    @Override
    @Transactional
    public Tutor findTutor(long id) {

        Tutor tutor = userDAO.findTutor(id);

        if (tutor == null) {
            throw new ObjectNotFoundException("No Tutor with id " + id + " exists");
        }

        return tutor;
    }

    @Override
    @Transactional
    public UnitCoordinator findCoordinator(long id) {

        UnitCoordinator coordinator = userDAO.findCoordinator(id);

        if (coordinator == null) {
            throw new ObjectNotFoundException("No Tutor with id " + id + " exists");
        }

        return coordinator;
    }

    /**
     * For this specific instance the method returns null when the object is not found or if multiple objects with the
     * given name are found.
     */
    @Override
    @Transactional
    public UnitCoordinator findCoordinatorByName(String name) {

        return userDAO.findCoordinatorByName(name);
    }

    /**
     * For this specific instance the method returns null when the object is not found or if multiple objects with the
     * given staff Id are found.
     */
    @Override
    @Transactional
    public UnitCoordinator findCoordinatorByStaffId(String staffId) {

        return userDAO.findCoordinatorByStaffId(staffId);
    }

    @Override
    @Transactional
    public Tutor loadTutor(long id) {

        Tutor tutor = userDAO.loadTutor(id);

        return tutor;
    }

    @Override
    @Transactional
    public List<UnitCoordinator> findAllCoordinators() {

        return userDAO.findAllCoordinators();
    }

    @Override
    public List<Tutor> findAllTutors() {

        return userDAO.findAllTutors();
    }

    @Override
    @Transactional
    public void setTutorAccountsEnabled(boolean enabled) {

        List<Tutor> tutors = findAllTutors();

        for (Tutor tutor : tutors) {
            tutor.setEnabled(enabled);
        }
    }

    @Override
    @Transactional
    public void setCoordinatorAccountsEnabled(boolean enabled) {

        List<UnitCoordinator> coordinators = findAllCoordinators();

        for (UnitCoordinator coordinator : coordinators) {
            coordinator.setEnabled(enabled);
        }
    }

}
