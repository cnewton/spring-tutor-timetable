package au.edu.curtin.tutortimetable.service.tutorial;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorial.TutorialDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.DomainObjectServiceImpl;
import au.edu.curtin.tutortimetable.service.user.UserService;

@Service
public class TutorialServiceImpl extends DomainObjectServiceImpl<Tutorial> implements TutorialService {

    @Autowired
    private TutorialDAO tutorialDAO;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public List<Tutorial> loadAll() {

        return tutorialDAO.loadAll();
    }

    @Override
    @Transactional
    public List<Tutorial> loadTutorialsForTutor(long tutorId) {

        Tutor tutor = userService.findTutor(tutorId);

        return tutorialDAO.loadTutorialsForTutor(tutor);
    }

    @Override
    @Transactional
    public void assignTutorialToTutor(long tutorialId, long tutorId) {

        Tutor tutor = userService.findTutor(tutorId);

        Tutorial tutorial = find(tutorialId);

        tutorial.setTutor(tutor);
        tutor.getAssignedTutorials().add(tutorial);

        update(tutorial);
    }

    @Override
    @Transactional
    public void clearAssignmentForTutorial(long tutorialId) {

        Tutorial tutorial = find(tutorialId);
        tutorial.setTutor(null);
        update(tutorial);
    }
}
