package au.edu.curtin.tutortimetable.service.tutorial;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

public interface TutorialService extends DomainObjectService<Tutorial> {

    public List<Tutorial> loadTutorialsForTutor(long tutorId);

    public void assignTutorialToTutor(long tutorialId, long tutorId);

    public void clearAssignmentForTutorial(long tutorialId);

    public List<Tutorial> loadAll();

}
