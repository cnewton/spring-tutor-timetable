package au.edu.curtin.tutortimetable.service.fileupload;

import org.springframework.web.multipart.MultipartFile;

import au.edu.curtin.tutortimetable.exception.InvalidCSVException;

public interface FileUploadService {

    public void parseUnitCsv(MultipartFile file);

    public void parseTutorialCsv(MultipartFile file);

    public byte[] downloadUnitCsv() throws InvalidCSVException;

    public byte[] downloadTutorialCsv() throws InvalidCSVException;

}
