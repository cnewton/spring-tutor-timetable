package au.edu.curtin.tutortimetable.service.fileupload;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import au.edu.curtin.tutortimetable.exception.FileUploadException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.service.csvparsing.CsvParsingService;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    protected static final Logger logger = LogManager.getLogger(FileUploadServiceImpl.class);

    @Autowired
    private CsvParsingService csvParsingService;

    @Override
    public void parseUnitCsv(MultipartFile file) {

        if (file == null || file.isEmpty()) {
            throw new FileUploadException("Uploaded file is empty.");
        }

        try {
            csvParsingService.parseUnitCsv(file.getInputStream());
        } catch (IOException | InvalidCSVException e) {
            throw new FileUploadException("Uploaded file is corrupt", e);
        }
    }

    @Override
    public void parseTutorialCsv(MultipartFile file) {

        if (file == null || file.isEmpty()) {
            throw new FileUploadException("Uploaded file is empty.");
        }

        try {
            csvParsingService.parseTutorialCsv(file.getInputStream());
        } catch (IOException | InvalidCSVException e) {
            throw new FileUploadException("Uploaded file is corrupt", e);
        }

    }

    @Override
    public byte[] downloadUnitCsv() throws InvalidCSVException {

        return csvParsingService.exportUnitCsv();
    }

    @Override
    public byte[] downloadTutorialCsv() throws InvalidCSVException {

        return csvParsingService.exportTutorialCsv();
    }

}
