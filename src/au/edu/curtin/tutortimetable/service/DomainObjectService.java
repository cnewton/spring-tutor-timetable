package au.edu.curtin.tutortimetable.service;

import java.util.List;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;

/**
 * A Domain object service represents a generic service that acts on a standard domain object to perform simple data
 * access operations.
 * 
 * @author Cameron Newton
 *
 * @param <T>
 *            The Domain object type
 */
public interface DomainObjectService<T extends IdBasedDomainObject> {

    /**
     * Attempt to retrieve a domain object given its id. The object is mapped into the appropriate class and returned.
     * 
     * @param id
     *            of the domain object
     * @throws ObjectNotFoundException
     *             if no domain object exists with that id.
     * @return Domain object
     */
    public T find(long id);

    /**
     * Retrieves all entries of the implemented datatype from the database and returns them as a list.
     * 
     * @return a list of all database entries.
     */
    public List<T> findAll();

    /**
     * Saves the given object to a new database row, throws an exception if the identifier is not empty (0)
     * 
     * @throws NotCreateOperationException
     *             - Identifier was not 0
     * 
     * @param obj
     *            to add to database
     */
    public T create(T obj);

    /**
     * Updates the database row corresponding to the given object's identifier. Throws an exception if the referenced
     * database object is part of the session and the java object ID is not matched to that object. Also throws an
     * exception if the identifier did not exist in the database.
     * 
     * @throws ObjectNotFoundException
     *             - Identifier did not exist in the database
     * @param obj
     *            to update in database
     */
    public T update(T obj);

    /**
     * Deletes the database row related to the given java object. Throws exception if the database row does not exist.
     * 
     * @param obj
     *            matching database row to delete
     */
    public void delete(T obj);

    /**
     * Deletes the database row referenced by the given id. Throws exception if the database row does not exist.
     * 
     * @param id
     *            of row to delete
     */
    public void delete(long id);

    /**
     * Returns the number of entries of the implemented datatype from the database.
     * 
     * @return count of elements
     */
    public long count();

}
