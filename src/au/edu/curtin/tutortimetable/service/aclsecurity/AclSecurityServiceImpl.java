package au.edu.curtin.tutortimetable.service.aclsecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.UserRole;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.CustomSecurityException;
import au.edu.curtin.tutortimetable.security.UserAuthentication;
import au.edu.curtin.tutortimetable.security.token.UserToken;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;

/**
 * A note on method implementations: All filtering methods in this class should check for a null Authentication first
 * and if the current Authentication is null should PASS (allow). An authentication will only be null if the endpoint is
 * unsecured or if the context is without security (Integration tests).
 * 
 * @author Cameron Newton
 *
 */
@Service
public class AclSecurityServiceImpl implements AclSecurityService {

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private UnitService unitService;

    /**
     * Ensure that the calling user is also the owner of the id being acted on.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Matching Id's
     */
    @Override
    @Transactional
    public void filterOwnerId(long userId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {
                if (auth.getUserId() != userId) {
                    throw new CustomSecurityException("Unable to update another users account!");
                }
            }
        }
    }

    /**
     * Ensure that the calling user owns the preference being deleted.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Tutor for the given preference is the calling user.
     */
    @Override
    @Transactional
    public void filterDeleteTutorPreference(long prefId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                TutorUnitPreference pref = tutorUnitPreferenceService.find(prefId);

                if (pref.getTutor().getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to delete another users preferences.");
                }
            }
        }
    }

    /**
     * Ensure that the calling user owns the availability being deleted.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Tutor for the given availability is the calling user.
     */
    @Override
    @Transactional
    public void filterDeleteTutorAvailability(long availabilityId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                TutorAvailability pref = tutorAvailabilityService.find(availabilityId);

                if (pref.getTutor().getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to delete another users availabilities.");
                }
            }
        }
    }

    /**
     * Ensure that the calling user owns the unit the priority is being set for.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Unit Coordinator for the given preference's unit is the calling user.
     */
    @Override
    @Transactional
    public void filterTutorPreferenceUnitCoordinator(long preferenceId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                TutorUnitPreference pref = tutorUnitPreferenceService.find(preferenceId);
                Unit unit = pref.getUnit();
                UnitCoordinator coordinator = unit.getUnitCoordinator();

                if (coordinator == null || coordinator.getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to set priorities for another coordinators units.");
                }
            }
        }

    }

    /**
     * Ensure that the calling user owns the unit the tutorial belongs to.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Unit Coordinator for the given tutorial's unit is the calling user.
     */
    @Override
    @Transactional
    public void filterTutorialUnitCoordinator(long tutorialId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                Tutorial tutorial = tutorialService.find(tutorialId);
                Unit unit = tutorial.getTutorialGroup().getUnit();
                UnitCoordinator coordinator = unit.getUnitCoordinator();

                if (coordinator == null || coordinator.getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to assign tutors for another coordinators units.");
                }
            }
        }

    }

    /**
     * Ensure that the calling user owns the unit the tutorial group belongs to.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Unit Coordinator for the given tutorial group's unit is the calling user.
     */
    @Override
    @Transactional
    public void filterTutorialGroupUnitCoordinator(long groupId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                TutorialGroup tutorialGroup = tutorialGroupService.find(groupId);
                Unit unit = tutorialGroup.getUnit();
                UnitCoordinator coordinator = unit.getUnitCoordinator();

                if (coordinator == null || coordinator.getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to assign tutors for another coordinators units.");
                }
            }
        }

    }

    /**
     * Ensure that the calling user owns the unit.<br>
     * PASS: Null<br>
     * PASS: Admin<br>
     * PASS: Unit Coordinator for the given unit is the calling user.
     */
    @Override
    @Transactional
    public void filterUnitCoordinator(long unitId) {

        UserToken auth = getCurrentAuth();

        if (auth != null) {
            if (!auth.getRoles().contains(UserRole.ROLE_ADMIN)) {

                Unit unit = unitService.find(unitId);
                UnitCoordinator coordinator = unit.getUnitCoordinator();

                if (coordinator == null || coordinator.getId() != auth.getUserId()) {
                    throw new CustomSecurityException("Unable to access units owned by another coordinator.");
                }
            }
        }
    }

    /**
     * To be used only in a final context, do not modify fields of the returned AuthToken.
     */
    @Override
    public UserToken getCurrentAuthUser() {

        return getCurrentAuth();
    }

    private final UserToken getCurrentAuth() {

        UserAuthentication userAuth = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        if (userAuth == null) {
            return null;
        }
        return userAuth.getUserToken();
    }
}
