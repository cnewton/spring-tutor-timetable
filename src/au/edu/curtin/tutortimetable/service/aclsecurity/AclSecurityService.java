package au.edu.curtin.tutortimetable.service.aclsecurity;

import au.edu.curtin.tutortimetable.security.token.UserToken;

public interface AclSecurityService {

    public UserToken getCurrentAuthUser();

    public void filterOwnerId(long userId);

    public void filterDeleteTutorPreference(long prefId);

    public void filterDeleteTutorAvailability(long availabilityId);

    public void filterTutorPreferenceUnitCoordinator(long preferenceId);

    public void filterTutorialUnitCoordinator(long tutorialId);

    public void filterTutorialGroupUnitCoordinator(long groupId);

    public void filterUnitCoordinator(long unitId);
}
