package au.edu.curtin.tutortimetable.service.administration;

import java.time.LocalDate;

import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;

public interface AdministrationService {

    public TimetableSystemState getSystemState();

    public void setSemesterStartDate(LocalDate date);

    public void setSemesterEndDate(LocalDate date);

    public void setApplicationStartDate(LocalDate date);

    public void setApplicationEndDate(LocalDate date);

    public void setLockoutDate(LocalDate date);

    public void deleteAllUnits();

    public void deleteAllTutorials();

    public void deleteAllTutorPreferences();

    public void deleteAllTutorAvailabilities();

    public void clearAllTutorAssignments();

    public void clearAllApplications();

    public void toggleTutorAccounts();

    public void toggleCoordinatorAccounts();

    public void setTutorMaxHours(int hours);

    public void assignAllTutorials();

}
