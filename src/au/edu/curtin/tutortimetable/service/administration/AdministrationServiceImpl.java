package au.edu.curtin.tutortimetable.service.administration;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.systemstate.TimetableSystemStateDAO;
import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.assignment.AssignmentService;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;

/**
 * This service contains the methods necessary to manage the system state. Methods performing sweeping database actions
 * should be contained within this service. Methods working directly with the state object should *NOT* be
 * transactional. Transactions for this object are maintained within the DAO itself.
 * 
 * @author Cameron
 *
 */
@Service
public class AdministrationServiceImpl implements AdministrationService {

    @Autowired
    private TimetableSystemStateDAO systemStateDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private TutorUnitPreferenceService preferenceService;

    @Autowired
    private TutorAvailabilityService availabilityService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private AssignmentService assignmentService;

    @Override
    public TimetableSystemState getSystemState() {

        return systemStateDAO.get();
    }

    @Override
    public void setSemesterStartDate(LocalDate date) {

        TimetableSystemState state = getSystemState();
        state.setSemesterStart(date);
        systemStateDAO.update(state);
    }

    @Override
    public void setSemesterEndDate(LocalDate date) {

        TimetableSystemState state = getSystemState();
        state.setSemesterEnd(date);
        systemStateDAO.update(state);
    }

    @Override
    public void setApplicationStartDate(LocalDate date) {

        TimetableSystemState state = getSystemState();
        state.setApplicationStart(date);
        systemStateDAO.update(state);
    }

    @Override
    public void setApplicationEndDate(LocalDate date) {

        TimetableSystemState state = getSystemState();
        state.setApplicationEnd(date);
        systemStateDAO.update(state);
    }

    @Override
    public void setLockoutDate(LocalDate date) {

        TimetableSystemState state = getSystemState();
        state.setLockout(date);
        systemStateDAO.update(state);
    }

    @Override
    @Transactional
    public void deleteAllUnits() {

        deleteAllTutorials();

        deleteAllTutorPreferences();

        for (TutorialGroup group : tutorialGroupService.findAll()) {
            tutorialGroupService.delete(group);
        }

        for (Unit unit : unitService.findAll()) {
            unitService.delete(unit);
        }
    }

    @Override
    @Transactional
    public void deleteAllTutorials() {

        for (Tutorial tutorial : tutorialService.findAll()) {
            tutorialService.delete(tutorial);
        }

        for (Tutor tutor : userService.findAllTutors()) {
            tutor.setAssignedHours(0);
        }
    }

    @Override
    @Transactional
    public void deleteAllTutorPreferences() {

        for (TutorUnitPreference pref : preferenceService.findAll()) {
            preferenceService.delete(pref);
        }
    }

    @Override
    @Transactional
    public void deleteAllTutorAvailabilities() {

        for (TutorAvailability avail : availabilityService.findAll()) {
            availabilityService.delete(avail);
        }
    }

    @Override
    @Transactional
    public void clearAllTutorAssignments() {

        for (Tutorial tutorial : tutorialService.findAll()) {
            tutorial.setTutor(null);
        }

        for (Tutor tutor : userService.findAllTutors()) {
            tutor.setAssignedHours(0);
        }
    }

    @Override
    @Transactional
    public void clearAllApplications() {

        deleteAllTutorPreferences();
        deleteAllTutorAvailabilities();

        for (Tutor tutor : userService.findAllTutors()) {
            tutor.setCourse(null);
            tutor.setFailedUnits(null);
            tutor.setYear(0);
        }
    }

    @Override
    public void toggleTutorAccounts() {

        TimetableSystemState state = getSystemState();

        state.setTutorsLocked(!state.isTutorsLocked());

        userService.setTutorAccountsEnabled(!state.isTutorsLocked());

        systemStateDAO.update(state);
    }

    @Override
    public void toggleCoordinatorAccounts() {

        TimetableSystemState state = getSystemState();

        state.setCoordinatorsLocked(!state.isCoordinatorsLocked());

        userService.setCoordinatorAccountsEnabled(!state.isCoordinatorsLocked());

        systemStateDAO.update(state);
    }

    @Override
    public void setTutorMaxHours(int hours) {

        TimetableSystemState state = getSystemState();
        state.setTutorMaxHours(hours);
        systemStateDAO.update(state);
    }

    @Override
    public void assignAllTutorials() {

        assignmentService.assignTutorialsGreedily();
    }
}
