package au.edu.curtin.tutortimetable.common;

import javax.validation.ConstraintViolationException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import au.edu.curtin.tutortimetable.exception.CustomSecurityException;
import au.edu.curtin.tutortimetable.exception.FileUploadException;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;

/**
 * The RestErrorHandler is responsible for catching exceptions thrown out of top level rest controllers. This class will
 * generally translate the exceptions into HTTP response codes to return to the calling client.
 * 
 * @author Cameron Newton
 *
 */
@ControllerAdvice
public class RestErrorHandler {

    protected static final Logger logger = LogManager.getLogger(RestErrorHandler.class);

    @Value("${invalidDataHeader}")
    private String INVALID_HEADER_NAME;

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleObjectNotFoundException(ObjectNotFoundException e) {

        logger.warn("Unknown Target Object: " + e.getMessage(), e);
    }

    @ExceptionHandler(NotCreateOperationException.class)
    public ResponseEntity<String> handleNotCreateException(NotCreateOperationException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        return new ResponseEntity<String>("Bad post request " + e.getMessage(), responseHeaders,
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotUpdateOperationException.class)
    public ResponseEntity<String> handleNotUpdateException(NotUpdateOperationException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        return new ResponseEntity<String>("Bad put request " + e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CustomSecurityException.class)
    public ResponseEntity<String> handleCustomSecurityException(CustomSecurityException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        logger.warn("Custom Security Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        logger.warn("Constraint Violation Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        logger.warn("Illegal Argument Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileUploadException.class)
    public ResponseEntity<String> handleFileUploadException(FileUploadException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(INVALID_HEADER_NAME, e.getMessage());

        logger.warn("File Upload Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }
}
