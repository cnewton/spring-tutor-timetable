package au.edu.curtin.tutortimetable.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.security.token.UserToken;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.user.UserService;

/**
 * The UserController is responsible for exposing endpoints that enable the manipulation of the User domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
public class UserController implements DomainObjectController<User> {

    @Autowired
    private UserService userService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<User> getAll() {

        return userService.findAll();
    }

    @Override
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public User get(@PathVariable("id") long id) {

        return userService.find(id);
    }

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    @Secured("ROLE_USER")
    public User put(@RequestBody User user) {

        aclService.filterOwnerId(user.getId());
        return userService.updateUserDetails(user);
    }

    @RequestMapping(value = "/tutor/{id}/details", method = RequestMethod.PUT)
    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    public void updateTutorDetails(@PathVariable("id") long tutorId, @RequestBody TutorDetailsDTO details) {

        aclService.filterOwnerId(tutorId);
        userService.updateTutorDetails(tutorId, details);
    }

    @RequestMapping(value = "/user/{id}/password", method = RequestMethod.PUT)
    @Secured("ROLE_USER")
    public void changePassword(@RequestBody String[] passwords, @PathVariable("id") long id) {

        userService.updateUserPassword(id, passwords[0], passwords[1]);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user/{id}/password/force", method = RequestMethod.PUT)
    public void changePassword(@RequestBody String newPassword, @PathVariable("id") long id) {

        userService.forceUpdateUserPassword(id, newPassword);
    }

    @Override
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        userService.delete(id);
    }

    @Override
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User post(@RequestBody User user) {

        return userService.create(user);
    }

    @Override
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/user/count", method = RequestMethod.GET)
    public long count() {

        return userService.count();
    }

    // Unsecured
    @RequestMapping(value = "/user/register/tutor", method = RequestMethod.POST)
    public User registerTutor(@RequestBody Tutor tutor) {

        return userService.create(tutor);
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public User getAccount() {

        UserToken token = aclService.getCurrentAuthUser();

        if (token == null) {
            return null;
        }

        return userService.find(token.getUserId());
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/{id}", method = RequestMethod.GET)
    public User loadTutor(@PathVariable("id") long id) {

        aclService.filterOwnerId(id);
        return userService.loadTutor(id);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/unitcoordinator", method = RequestMethod.GET)
    public List<UnitCoordinator> getAllCoordinators() {

        return userService.findAllCoordinators();
    }

    /**
     * Allows for remaking of an administrator password after a database wipe.
     * 
     * @RequestMapping(value = "/user/make", method = RequestMethod.GET) public Administrator make() {
     * 
     *                       Administrator admin = new Administrator();
     *                       admin.setEmail("16148116@student.curtin.edu.au"); admin.setEnabled(true); admin.setId(0);
     *                       admin.setInstitutionId("16148116"); admin.setName("Cameron Newton");
     *                       admin.setPassword("test"); userService.create(admin); return admin; }
     */
}
