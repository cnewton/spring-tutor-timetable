package au.edu.curtin.tutortimetable.controller.fileupload;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.service.fileupload.FileUploadService;

@RestController
@Secured({ "ROLE_ADMIN" })
public class FileUploadController {

    protected static final Logger logger = LogManager.getLogger(FileUploadController.class);

    @Autowired
    private FileUploadService fileUploadService;

    @RequestMapping(value = "/upload/unitcsv", method = RequestMethod.POST)
    public void uploadUnitCsv(@RequestBody MultipartFile file) {

        fileUploadService.parseUnitCsv(file);
    }

    @RequestMapping(value = "/upload/tutorialcsv", method = RequestMethod.POST)
    public void uploadTutorialCsv(@RequestBody MultipartFile file) {

        fileUploadService.parseTutorialCsv(file);
    }

    @RequestMapping(value = "/download/unitcsv", method = RequestMethod.GET)
    public void downloadUnitCsv(HttpServletResponse response) throws InvalidCSVException, IOException {

        byte[] csvData = fileUploadService.downloadUnitCsv();
        response.setContentType("text/plain");
        response.setHeader("Content-Disposition", "attachment; filename=\"unit_export_csv.csv\"");
        response.setContentLength(csvData.length);
        InputStream in = new ByteArrayInputStream(csvData);

        StreamUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value = "/download/tutorialcsv", method = RequestMethod.GET)
    public void downloadTutorialCsv(HttpServletResponse response) throws InvalidCSVException, IOException {

        byte[] csvData = fileUploadService.downloadTutorialCsv();
        response.setContentType("text/plain");
        response.setHeader("Content-Disposition", "attachment; filename=\"tutorial_export_csv.csv\"");
        response.setContentLength(csvData.length);
        InputStream in = new ByteArrayInputStream(csvData);

        StreamUtils.copy(in, response.getOutputStream());
    }
}
