package au.edu.curtin.tutortimetable.controller.tutoravailability;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;

/**
 * The TutorAvailabilityController is responsible for exposing endpoints that enable the manipulation of the
 * TutorAvailability domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_ADMIN" })
public class TutorAvailabilityController implements DomainObjectController<TutorAvailability> {

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @RequestMapping(value = "/tutor/availability", method = RequestMethod.GET)
    public List<TutorAvailability> getAll() {

        return tutorAvailabilityService.findAll();
    }

    @Override
    @RequestMapping(value = "/tutor/availability/{id}", method = RequestMethod.GET)
    public TutorAvailability get(@PathVariable("id") long id) {

        return tutorAvailabilityService.find(id);
    }

    @Override
    @RequestMapping(value = "/tutor/availability", method = RequestMethod.PUT)
    public TutorAvailability put(@RequestBody TutorAvailability tutorAvailability) {

        return tutorAvailabilityService.update(tutorAvailability);
    }

    @Override
    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/availability/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        aclService.filterDeleteTutorAvailability(id);
        tutorAvailabilityService.delete(id);
    }

    @Override
    @RequestMapping(value = "/tutor/availability", method = RequestMethod.POST)
    public TutorAvailability post(@RequestBody TutorAvailability tutorAvailability) {

        return tutorAvailabilityService.create(tutorAvailability);
    }

    @Override
    @RequestMapping(value = "/tutor/availability/count", method = RequestMethod.GET)
    public long count() {

        return tutorAvailabilityService.count();
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/{id}/availability/add", method = RequestMethod.PUT)
    public void listAddAvailabilities(@PathVariable("id") long tutorId,
            @RequestBody List<TutorAvailability> availabilities) {

        aclService.filterOwnerId(tutorId);
        tutorAvailabilityService.listAddAvailabilities(tutorId, availabilities);
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/{id}/availability/subtract", method = RequestMethod.PUT)
    public void listSubtractAvailabilities(@PathVariable("id") long tutorId, @RequestBody long[] availabilityIds) {

        aclService.filterOwnerId(tutorId);
        tutorAvailabilityService.listSubtractAvailabilities(tutorId, availabilityIds);
    }

}
