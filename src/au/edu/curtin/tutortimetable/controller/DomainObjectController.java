package au.edu.curtin.tutortimetable.controller;

import java.util.List;

import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;

/**
 * A Domain object controller represents a generic controller that provides REST endpoints for basic manipulation of a
 * domain object. Generally methods within this class send and receive serialized versions of the domain objects.
 * Exceptions thrown from methods of this class are also generally caught by the
 * {@link au.edu.curtin.tutortimetable.common.RestErrorHandler RestErrorHandler} and sent as HTTP codes.
 * 
 * @author Cameron Newton
 *
 * @param <T>
 *            The Domain object type
 */
public interface DomainObjectController<T> {

    /**
     * Attempt to retrieve a domain object given its id. The object is mapped into the appropriate class and finally
     * serialized into JSON representation to be sent to the entity calling the REST endpoint.
     * 
     * @param id
     *            of the domain object
     * @throws ObjectNotFoundException
     *             if no domain object exists with that id.
     * @return Domain object (generally in serialized form)
     */
    public T get(long id);

    /**
     * Retrieves all entries of the implemented datatype from the database and returns them as a serialized list of
     * objects.
     * 
     * @return a list of all database entries.
     */
    public List<T> getAll();

    /**
     * Accepts an object and saves the given object to a new database row, throws an exception if the identifier is not
     * empty (0). Generally used as a REST endpoint with a serialized JSON representation of the domain object, to be
     * automatically deserialized before the method is invoked.
     * 
     * @throws NotCreateOperationException
     *             - Identifier was not 0
     * 
     * @param obj
     *            in serialized form to add to database
     * 
     * @return the persistent representation of the object generally in serialized form.
     */
    public T post(T obj);

    /**
     * Updates the database row corresponding to the given object's identifier. Throws an exception if the referenced
     * database object is part of the session and the java object ID is not matched to that object. Also throws an
     * exception if the identifier did not exist in the database. Generally used as a REST endpoint with a serialized
     * JSON representation of the domain object, to be automatically deserialized before the method is invoked.
     * 
     * @throws ObjectNotFoundException
     *             - Identifier did not exist in the database
     * @param obj
     *            in serialized form to update in database
     * 
     * @return the persistent representation of the object generally in serialized form.
     */
    public T put(T obj);

    /**
     * Deletes the database row referenced by the given id. Throws exception if the database row does not exist.
     * 
     * @param id
     *            of row to delete
     */
    public void delete(long id);

    /**
     * Returns the number of entries of the implemented datatype from the database.
     * 
     * @return count of elements
     */
    public long count();

}