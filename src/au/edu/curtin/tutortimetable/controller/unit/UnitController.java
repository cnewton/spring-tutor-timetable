package au.edu.curtin.tutortimetable.controller.unit;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;

/**
 * The UnitController is responsible for exposing endpoints that enable the manipulation of the Unit domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
public class UnitController implements DomainObjectController<Unit> {

    @Autowired
    private UnitService unitService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/unit", method = RequestMethod.GET)
    public List<Unit> getAll() {

        return unitService.findAll();
    }

    @Override
    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/{id}", method = RequestMethod.GET)
    public Unit get(@PathVariable("id") long id) {

        return unitService.find(id);
    }

    @Override
    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit", method = RequestMethod.PUT)
    public Unit put(@RequestBody Unit unit) {

        return unitService.update(unit);
    }

    @Override
    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        unitService.delete(id);
    }

    @Override
    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit", method = RequestMethod.POST)
    public Unit post(@RequestBody Unit unit) {

        return unitService.create(unit);
    }

    @Override
    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/count", method = RequestMethod.GET)
    public long count() {

        return unitService.count();
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/user/{userId}", method = RequestMethod.GET)
    public List<Unit> getAllForCoordinator(@PathVariable("userId") long userId) {

        aclService.filterOwnerId(userId);
        return unitService.getUnitsForCoordinator(userId);
    }

    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/load", method = RequestMethod.GET)
    public List<Unit> loadAll() {

        return unitService.loadAllWithCoordinator();
    }

    @Secured({ "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/{unitId}/assign/{coordinatorId}", method = RequestMethod.PUT)
    public void assignCoordinator(@PathVariable("unitId") long unitId,
            @PathVariable("coordinatorId") long coordinatorId) {

        unitService.assignCoordinator(unitId, coordinatorId);
    }
}
