package au.edu.curtin.tutortimetable.controller.administration;

import java.time.LocalDate;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;
import au.edu.curtin.tutortimetable.service.administration.AdministrationService;

@RestController
@Secured({ "ROLE_ADMIN" })
public class AdministrationController {

    protected static final Logger logger = LogManager.getLogger(AdministrationController.class);

    @Autowired
    private AdministrationService administrationService;

    @RequestMapping(value = "/admin/systemstate", method = RequestMethod.GET)
    public TimetableSystemState getState() {

        return administrationService.getSystemState();
    }

    @RequestMapping(value = "/admin/systemstate/semester/start", method = RequestMethod.PUT)
    public void setSemesterStartDate(@RequestBody LocalDate date) {

        administrationService.setSemesterStartDate(date);
    }

    @RequestMapping(value = "/admin/systemstate/semester/end", method = RequestMethod.PUT)
    public void setSemesterEndDate(@RequestBody LocalDate date) {

        administrationService.setSemesterEndDate(date);
    }

    @RequestMapping(value = "/admin/systemstate/application/start", method = RequestMethod.PUT)
    public void setApplicationStartDate(@RequestBody LocalDate date) {

        administrationService.setApplicationStartDate(date);
    }

    @RequestMapping(value = "/admin/systemstate/application/end", method = RequestMethod.PUT)
    public void setApplicationEndDate(@RequestBody LocalDate date) {

        administrationService.setApplicationEndDate(date);
    }

    @RequestMapping(value = "/admin/systemstate/lockout", method = RequestMethod.PUT)
    public void setLockoutDate(@RequestBody LocalDate date) {

        administrationService.setLockoutDate(date);
    }

    @RequestMapping(value = "/admin/units/wipe", method = RequestMethod.DELETE)
    public void deleteAllUnits() {

        administrationService.deleteAllUnits();
    }

    @RequestMapping(value = "/admin/tutorials/wipe", method = RequestMethod.DELETE)
    public void deleteAllTutorials() {

        administrationService.deleteAllTutorials();
    }

    @RequestMapping(value = "/admin/assignments/wipe", method = RequestMethod.DELETE)
    public void clearAllTutorAssignments() {

        administrationService.clearAllTutorAssignments();
    }

    @RequestMapping(value = "/admin/applications/wipe", method = RequestMethod.DELETE)
    public void clearAllApplications() {

        administrationService.clearAllApplications();
    }

    @RequestMapping(value = "/admin/tutors/lock/toggle", method = RequestMethod.PUT)
    public void toggleTutorAccounts() {

        administrationService.toggleTutorAccounts();
    }

    @RequestMapping(value = "/admin/coordinators/lock/toggle", method = RequestMethod.PUT)
    public void toggleCoordinatorAccounts() {

        administrationService.toggleCoordinatorAccounts();
    }

    @RequestMapping(value = "/admin/systemstate/tutor/hours", method = RequestMethod.PUT)
    public void setTutorMaxHours(@RequestParam("maxHours") int hours) {

        administrationService.setTutorMaxHours(hours);
    }

    @RequestMapping(value = "/admin/tutorials/assign", method = RequestMethod.PUT)
    public void assignAllTutorials() {

        administrationService.assignAllTutorials();
    }
}