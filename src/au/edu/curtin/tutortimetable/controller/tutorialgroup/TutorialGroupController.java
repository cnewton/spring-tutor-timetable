package au.edu.curtin.tutortimetable.controller.tutorialgroup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorialGroupDTO;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;

/**
 * The TutorialGroupController is responsible for exposing endpoints that enable the manipulation of the TutorialGroup
 * domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_ADMIN" })
public class TutorialGroupController implements DomainObjectController<TutorialGroup> {

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @RequestMapping(value = "/tutorial/group", method = RequestMethod.GET)
    public List<TutorialGroup> getAll() {

        return tutorialGroupService.findAll();
    }

    @Override
    @RequestMapping(value = "/tutorial/group/{id}", method = RequestMethod.GET)
    public TutorialGroup get(@PathVariable("id") long id) {

        return tutorialGroupService.find(id);
    }

    @Override
    @RequestMapping(value = "/tutorial/group", method = RequestMethod.PUT)
    public TutorialGroup put(@RequestBody TutorialGroup tutorial) {

        return tutorialGroupService.update(tutorial);
    }

    @Override
    @RequestMapping(value = "/tutorial/group/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        tutorialGroupService.delete(id);
    }

    @Override
    @RequestMapping(value = "/tutorial/group", method = RequestMethod.POST)
    public TutorialGroup post(@RequestBody TutorialGroup tutorial) {

        return tutorialGroupService.create(tutorial);
    }

    @Override
    @RequestMapping(value = "/tutorial/group/count", method = RequestMethod.GET)
    public long count() {

        return tutorialGroupService.count();
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/group/{groupId}/tutor/{tutorId}/assign", method = RequestMethod.PUT)
    public void assignTutorialGroup(@PathVariable("tutorId") long tutorId, @PathVariable("groupId") long groupId) {

        aclService.filterTutorialGroupUnitCoordinator(groupId);
        tutorialGroupService.assignTutorialGroupToTutor(groupId, tutorId);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/group/unit/{unitId}", method = RequestMethod.GET)
    public List<TutorialGroupDTO> loadTutorialGroupsForUnit(@PathVariable("unitId") long unitId) {

        aclService.filterUnitCoordinator(unitId);
        return tutorialGroupService.loadTutorialGroupsForUnit(unitId);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/group/{groupId}/tutor/unassign", method = RequestMethod.PUT)
    public void clearAssignmentForTutorialGroup(@PathVariable("groupId") long groupId) {

        aclService.filterTutorialGroupUnitCoordinator(groupId);
        tutorialGroupService.clearAssignmentForTutorialGroup(groupId);
    }
}
