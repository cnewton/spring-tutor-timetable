package au.edu.curtin.tutortimetable.controller.tutorunitpreference;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorUnitPreferenceDTO;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;

/**
 * The TutorUnitPreferenceController is responsible for exposing endpoints that enable the manipulation of the
 * TutorUnitPreference domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured("ROLE_ADMIN")
public class TutorUnitPreferenceController implements DomainObjectController<TutorUnitPreference> {

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @RequestMapping(value = "/tutor/preference", method = RequestMethod.GET)
    public List<TutorUnitPreference> getAll() {

        return tutorUnitPreferenceService.findAll();
    }

    @Override
    @RequestMapping(value = "/tutor/preference/{id}", method = RequestMethod.GET)
    public TutorUnitPreference get(@PathVariable("id") long id) {

        return tutorUnitPreferenceService.find(id);
    }

    @Override
    @RequestMapping(value = "/tutor/preference", method = RequestMethod.PUT)
    public TutorUnitPreference put(@RequestBody TutorUnitPreference tutorUnitPreference) {

        return tutorUnitPreferenceService.update(tutorUnitPreference);
    }

    @Override
    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/preference/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long prefId) {

        aclService.filterTutorPreferenceUnitCoordinator(prefId);
        tutorUnitPreferenceService.delete(prefId);
    }

    @Override
    @RequestMapping(value = "/tutor/preference", method = RequestMethod.POST)
    public TutorUnitPreference post(@RequestBody TutorUnitPreference tutorUnitPreference) {

        return tutorUnitPreferenceService.create(tutorUnitPreference);
    }

    @Override
    @RequestMapping(value = "/tutor/preference/count", method = RequestMethod.GET)
    public long count() {

        return tutorUnitPreferenceService.count();
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/preference/create", method = RequestMethod.POST)
    public TutorUnitPreference createPreference(@RequestParam("tutorId") long tutorId,
            @RequestParam("unitId") long unitId) {

        aclService.filterOwnerId(tutorId);
        return tutorUnitPreferenceService.createTutorPreference(tutorId, unitId);
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/{id}/preference/add", method = RequestMethod.PUT)
    public void listAddPreference(@PathVariable("id") long tutorId, @RequestBody long[] unitIds) {

        aclService.filterOwnerId(tutorId);
        tutorUnitPreferenceService.listAddTutorPreferences(tutorId, unitIds);
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/{id}/preference/subtract", method = RequestMethod.PUT)
    public void listSubtractPreference(@PathVariable("id") long tutorId, @RequestBody long[] unitIds) {

        aclService.filterOwnerId(tutorId);
        tutorUnitPreferenceService.listSubtractTutorPreferences(tutorId, unitIds);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutor/preference/{preferenceId}/priority", method = RequestMethod.PUT)
    public TutorUnitPreference updatePreferencePriority(@PathVariable("preferenceId") long preferenceId,
            @RequestParam("priority") int priority) {

        aclService.filterTutorPreferenceUnitCoordinator(preferenceId);
        return tutorUnitPreferenceService.updateTutorPreferencePriority(preferenceId, priority);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/unit/{unitId}/preference", method = RequestMethod.GET)
    public List<TutorUnitPreferenceDTO> loadPreferencesForUnit(@PathVariable("unitId") long unitId) {

        aclService.filterUnitCoordinator(unitId);
        return tutorUnitPreferenceService.loadTutorPreferencesForUnit(unitId);
    }

}
