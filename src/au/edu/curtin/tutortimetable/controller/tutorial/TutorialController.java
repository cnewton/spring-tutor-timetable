package au.edu.curtin.tutortimetable.controller.tutorial;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import au.edu.curtin.tutortimetable.controller.DomainObjectController;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.service.aclsecurity.AclSecurityService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;

/**
 * The TutorialController is responsible for exposing endpoints that enable the manipulation of the Tutorial domain
 * object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_ADMIN" })
public class TutorialController implements DomainObjectController<Tutorial> {

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private AclSecurityService aclService;

    @Override
    @RequestMapping(value = "/tutorial", method = RequestMethod.GET)
    public List<Tutorial> getAll() {

        return tutorialService.findAll();
    }

    @Override
    @RequestMapping(value = "/tutorial/{id}", method = RequestMethod.GET)
    public Tutorial get(@PathVariable("id") long id) {

        return tutorialService.find(id);
    }

    @Override
    @RequestMapping(value = "/tutorial", method = RequestMethod.PUT)
    public Tutorial put(@RequestBody Tutorial tutorial) {

        return tutorialService.update(tutorial);
    }

    @Override
    @RequestMapping(value = "/tutorial/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        tutorialService.delete(id);
    }

    @Override
    @RequestMapping(value = "/tutorial", method = RequestMethod.POST)
    public Tutorial post(@RequestBody Tutorial tutorial) {

        return tutorialService.create(tutorial);
    }

    @Override
    @RequestMapping(value = "/tutorial/count", method = RequestMethod.GET)
    public long count() {

        return tutorialService.count();
    }

    @Secured({ "ROLE_TUTOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/tutor/{tutorId}", method = RequestMethod.GET)
    public List<Tutorial> loadTutorialsForTutor(@PathVariable("tutorId") long tutorId) {

        aclService.filterOwnerId(tutorId);
        return tutorialService.loadTutorialsForTutor(tutorId);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/{tutorialId}/tutor/{tutorId}/assign", method = RequestMethod.PUT)
    public void assignTutorial(@PathVariable("tutorId") long tutorId, @PathVariable("tutorialId") long tutorialId) {

        aclService.filterTutorialUnitCoordinator(tutorialId);
        tutorialService.assignTutorialToTutor(tutorialId, tutorId);
    }

    @Secured({ "ROLE_COORDINATOR", "ROLE_ADMIN" })
    @RequestMapping(value = "/tutorial/{tutorialId}/tutor/unassign", method = RequestMethod.PUT)
    public void clearAssignmentForTutorial(@PathVariable("tutorialId") long tutorialId) {

        aclService.filterTutorialUnitCoordinator(tutorialId);
        tutorialService.clearAssignmentForTutorial(tutorialId);
    }
}
