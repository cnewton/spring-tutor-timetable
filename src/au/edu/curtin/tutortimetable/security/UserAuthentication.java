package au.edu.curtin.tutortimetable.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import au.edu.curtin.tutortimetable.domainobject.user.UserRole;
import au.edu.curtin.tutortimetable.security.token.UserToken;

/**
 * This constructor should only be used by an application security context aware class that is satisfied with producing
 * a trusted authentication.
 */
public class UserAuthentication implements Authentication {

    /**
     * 
     */
    private static final long serialVersionUID = -3229087372822591301L;

    private UserToken token;

    private List<GrantedAuthority> authorities;

    private boolean authenticated = true;

    public UserAuthentication(UserToken authUser) {

        this.token = authUser;

        authorities = new ArrayList<GrantedAuthority>();
        for (UserRole role : token.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRole()));
        }
    }

    @Override
    public String getName() {

        return token.getUsername();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return authorities;
    }

    @Override
    public Object getCredentials() {

        return null;
    }

    @Override
    public Object getDetails() {

        return token;
    }

    @Override
    public Object getPrincipal() {

        return token.getUsername();
    }

    @Override
    public boolean isAuthenticated() {

        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

        this.authenticated = isAuthenticated;
    }

    public UserToken getUserToken() {

        return token;
    }

}
