package au.edu.curtin.tutortimetable.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.UserRole;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.user.UserService;

/**
 * Custom Authentication provider to authenticate REST calls. Will get the user from the database based on username, and
 * compare the hashed password values to determine if authentication should be granted.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

    protected static final Logger logger = LogManager.getLogger(CustomAuthenticationProvider.class);

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenHandler tokenHandler;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
        String username = String.valueOf(auth.getPrincipal().toString());
        String password = String.valueOf(auth.getCredentials());

        if (username.isEmpty() || password.isEmpty()) {
            throw new UsernameNotFoundException("No username or password entered");
        } else {
            logger.info("Authenticating user \"" + username + "\"...");
            return authenticateUser(username, password);
        }
    }

    /**
     * Verify the credentials are valid. This method hashes the supplied password and checks it against the database
     * values that correspond to the given username.
     * 
     * @param suppliedUsername
     * @param suppliedPassword
     * @return Authentication
     */

    private Authentication authenticateUser(String suppliedUsername, String suppliedPassword) {

        User user = null;

        try {
            user = userService.findByUsername(suppliedUsername);
        } catch (ObjectNotFoundException e) {
            logger.info("Incorrect username (user not found)");
            throw new UsernameNotFoundException("Incorrect username");
        }

        if (user.isEnabled() && passwordEncoder.matches(suppliedPassword, user.getPassword())) {
            logger.info("Authenticated user: " + user.getUsername());
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            // Loop through all Roles and create GrantedAuthority's for them
            for (UserRole role : user.getUserRoles()) {
                authorities.add(new SimpleGrantedAuthority(role.getRole()));
            }
            return new UserAuthentication(tokenHandler.createTokenForUser(user));
        } else {
            logger.info("Unsuccessful login attempt. User: " + suppliedUsername);
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {

        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
