package au.edu.curtin.tutortimetable.security.token;

import java.util.Collection;

import au.edu.curtin.tutortimetable.domainobject.user.UserRole;

public class UserToken {

    private long userId;

    private String username;

    private long expiryTime;

    private Collection<UserRole> roles;

    public long getUserId() {

        return userId;
    }

    public long getExpiryTime() {

        return expiryTime;
    }

    public void setUserId(long userId) {

        this.userId = userId;
    }

    public void setExpiryTime(long expiryTime) {

        this.expiryTime = expiryTime;
    }

    public Collection<UserRole> getRoles() {

        return roles;
    }

    public void setRoles(Collection<UserRole> roles) {

        this.roles = roles;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

}
