package au.edu.curtin.tutortimetable.security.token;

import au.edu.curtin.tutortimetable.domainobject.user.User;

public interface TokenHandler {

    public String createTokenStringForUser(User user);

    public UserToken createTokenForUser(User user);

    public UserToken parseTokenString(String token);

    public String createTokenStringForUser(UserToken token);

}
