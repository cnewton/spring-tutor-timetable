package au.edu.curtin.tutortimetable.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

/**
 * Custom Authentication Failure handler. Handles failed REST authentication requests by returning a HTTP 401 and
 * logging the reason.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {

    protected static final Logger logger = LogManager.getLogger(CustomAuthenticationFailureHandler.class);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authenticationException) throws IOException, ServletException {

        logger.info("Loggin attempt failure: " + authenticationException.getMessage());
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }
}
