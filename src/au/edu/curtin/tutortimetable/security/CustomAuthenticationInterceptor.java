package au.edu.curtin.tutortimetable.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.security.token.UserToken;

/**
 * Intercepts any REST controller call, and extracts the authentication header from the HTTP request, authenticates the
 * user against the token handler and allows the request to proceed if the authentication succeeds.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String authToken = request.getHeader(AUTH_HEADER_NAME);

        if (authToken == null) {
            return true;
        }

        UserToken authUser = tokenHandler.parseTokenString(authToken);

        if (authUser == null) {
            return true;
        }

        Authentication auth = new UserAuthentication(authUser);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return true;
    }
}
