package au.edu.curtin.tutortimetable.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.security.token.UserToken;

/**
 * Custom Authentication Success handler. Handles successful REST authentication requests by adding a header into the
 * response that contains a token that can be decoded by the server in future requests to authenticate the user.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    protected static final Logger logger = LogManager.getLogger(CustomAuthenticationSuccessHandler.class);

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        UserToken userToken = (UserToken) authentication.getDetails();

        logger.info("Authentication Success for:" + userToken.getUsername());

        String authToken = tokenHandler.createTokenStringForUser(userToken);

        response.addHeader(AUTH_HEADER_NAME, authToken);
    }
}