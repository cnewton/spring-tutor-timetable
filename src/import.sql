

 
#This File will be run after a new database has been created by hibernate's auto create system.

#This script creates the root admin user as well as the system state table.

#The password for the root user will be 'password16'

#This file is for MySQL databases.

#Note that entries must be all in one line.

INSERT INTO Users (id, name, email, enabled, username, password)VALUES (1, "Root User", "root@email.com", true, "rootuser", "$2a$10$7kYAWhRHc5yRlAH8psQoWOMptkgQp5JkDe.TQXdXwTchi5UXXE5WW");

INSERT INTO Administrators (id)VALUES (1);

INSERT INTO TimetableSystemState (id, semesterStart, semesterEnd,applicationStart, applicationEnd,lockout, tutorsLocked, coordinatorsLocked,tutorMaxHours)VALUES (1, DATE("2016-02-29"), DATE("2016-06-03"),DATE("2016-02-01"), DATE("2016-02-15"),DATE("2016-06-04"), false, false, 10);