
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO TutorAvailabilities (id, tutorId, day, startTime, endTime)
VALUES (1, 2, 'FRIDAY', to_date('10:00:00', 'HH24:MI:SS'), to_date('12:00:00', 'HH24:MI:SS'));

INSERT INTO TutorAvailabilities (id, tutorId, day, startTime, endTime)
VALUES (2, 2, 'FRIDAY', to_date('10:00:00', 'HH24:MI:SS'), to_date('12:00:00', 'HH24:MI:SS'));

INSERT INTO TutorAvailabilities (id, tutorId, day, startTime, endTime)
VALUES (3, 2, 'FRIDAY', to_date('10:00:00', 'HH24:MI:SS'), to_date('12:00:00', 'HH24:MI:SS'));