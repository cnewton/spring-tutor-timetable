
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Tutorials (id, tutorId, groupId, room, startTime, endTime)
VALUES (1, 2, 1, '314.218', to_date('2016-03-22 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), to_date('2016-03-22 12:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO Tutorials (id, tutorId, groupId, room, startTime, endTime)
VALUES (2, 2, 1, '314.220', to_date('2016-03-22 12:00:00', 'YYYY-MM-DD HH24:MI:SS'), to_date('2016-03-22 14:00:00', 'YYYY-MM-DD HH24:MI:SS'));

INSERT INTO Tutorials (id, tutorId, groupId, room, startTime, endTime)
VALUES (3, 2, 1, '314.219', to_date('2016-03-22 14:00:00', 'YYYY-MM-DD HH24:MI:SS'), to_date('2016-03-22 16:00:00', 'YYYY-MM-DD HH24:MI:SS'));