
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO TimetableSystemState (id, semesterStart, semesterEnd,
								applicationStart, applicationEnd,
								lockout, tutorsLocked, coordinatorsLocked,
								tutorMaxHours)
								
VALUES (1, to_date('2016-02-29', 'YYYY-MM-DD'), to_date('2016-06-03', 'YYYY-MM-DD'),
		to_date('2016-02-01', 'YYYY-MM-DD'), to_date('2016-02-15', 'YYYY-MM-DD'),
		to_date('2016-06-04', 'YYYY-MM-DD'), false, false, 10);