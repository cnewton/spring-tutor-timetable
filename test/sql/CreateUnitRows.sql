
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Units (id, unitCode, unitName, unitCoordinatorId)
VALUES (1, 'COMP300', 'Unix and C Programming', 2);

INSERT INTO Units (id, unitCode, unitName, unitCoordinatorId)
VALUES (2, 'COMP200', 'Object Oriented Programming', 2);

INSERT INTO Units (id, unitCode, unitName, unitCoordinatorId)
VALUES (3, 'COMP100', 'Intro to Software Engineering', 2);

