
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Users (id, name, email, staffId, mobile, enabled, username, password)
VALUES (1, 'John Tester', 'test1@Test.com', '151511515', '0404040404', true, 'testuser1', 'testpassword');

INSERT INTO Administrators (id)
VALUES (1);

INSERT INTO Users (id, name, email, staffId, mobile, enabled, username, password)
VALUES (2, 'Sally Tester', 'test2@Test.com', '161511515', '0404040404', true, 'testuser2', 'testpassword');

INSERT INTO Tutors (id, studentId, course, year, assignedHours, failedUnits)
VALUES (2, 1234567890, 'Bachelor Of Computer Science', 3, 0, 'OOPD %% DSA');

INSERT INTO UnitCoordinators (id)
VALUES (2);

INSERT INTO Users (id, name, email, staffId, mobile, enabled, username, password)
VALUES (3, 'Mark Tester', 'test3@Test.com', '171511515', '0404040404', true, 'testuser3', 'testpassword');