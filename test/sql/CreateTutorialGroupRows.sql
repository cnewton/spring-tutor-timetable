
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO TutorialGroups (id, unitId, groupName, tutorialType)
VALUES (1, 1, 'Lab01', 'DEMONSTRATION');

INSERT INTO TutorialGroups (id, unitId, groupName, tutorialType)
VALUES (2, 1, 'Lab02', 'LABORATORY');

INSERT INTO TutorialGroups (id, unitId, groupName, tutorialType)
VALUES (3, 2, 'Lab01', 'DEMONSTRATION');