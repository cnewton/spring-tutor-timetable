package au.edu.curtin.tutortimetable.test.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        // Test classes here
})
public class ExternalServicesTestSuite {
    // Leave Empty.
}
