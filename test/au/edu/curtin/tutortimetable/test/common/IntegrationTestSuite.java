package au.edu.curtin.tutortimetable.test.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import au.edu.curtin.tutortimetable.test.integration.controller.administration.AdministrationControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.fileupload.FileUploadControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.tutoravailability.TutorAvailabilityControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.tutorial.TutorialControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.tutorialgroup.TutorialGroupControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.tutorunitpreference.TutorUnitPreferenceControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.unit.UnitControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.user.UserControllerTest;

@RunWith(Suite.class)
@SuiteClasses({
        AdministrationControllerTest.class,
        UnitControllerTest.class,
        UserControllerTest.class,
        FileUploadControllerTest.class,
        TutorialControllerTest.class,
        TutorialGroupControllerTest.class,
        TutorUnitPreferenceControllerTest.class,
        TutorAvailabilityControllerTest.class
        // Classes go here
})
public class IntegrationTestSuite {
    // Remains Empty.
}
