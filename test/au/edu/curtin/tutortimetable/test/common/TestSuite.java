package au.edu.curtin.tutortimetable.test.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import au.edu.curtin.tutortimetable.test.csv.mapper.TutorialMapperTest;
import au.edu.curtin.tutortimetable.test.csv.mapper.UnitMapperTest;
import au.edu.curtin.tutortimetable.test.csv.parser.CsvExportParserTest;
import au.edu.curtin.tutortimetable.test.csv.parser.CsvImportParserTest;
import au.edu.curtin.tutortimetable.test.dao.systemstate.TimetableSystemStateDAOTest;
import au.edu.curtin.tutortimetable.test.dao.tutoravailability.TutorAvailabilityDAOTest;
import au.edu.curtin.tutortimetable.test.dao.tutorial.TutorialDAOTest;
import au.edu.curtin.tutortimetable.test.dao.tutorialgroup.TutorialGroupDAOTest;
import au.edu.curtin.tutortimetable.test.dao.tutorunitpreference.TutorUnitPreferenceDAOTest;
import au.edu.curtin.tutortimetable.test.dao.unit.UnitDAOTest;
import au.edu.curtin.tutortimetable.test.dao.user.UserDAOTest;
import au.edu.curtin.tutortimetable.test.service.administration.AdministrationServiceTest;
import au.edu.curtin.tutortimetable.test.service.assignment.AssignmentServiceTest;
import au.edu.curtin.tutortimetable.test.service.csvparsing.CsvParsingServiceTest;
import au.edu.curtin.tutortimetable.test.service.fileupload.FileUploadServiceTest;
import au.edu.curtin.tutortimetable.test.service.tutoravailability.TutorAvailabilityServiceTest;
import au.edu.curtin.tutortimetable.test.service.tutorial.TutorialServiceTest;
import au.edu.curtin.tutortimetable.test.service.tutorialgroup.TutorialGroupServiceTest;
import au.edu.curtin.tutortimetable.test.service.tutorunitpreference.TutorUnitPreferenceServiceTest;
import au.edu.curtin.tutortimetable.test.service.unit.UnitServiceTest;
import au.edu.curtin.tutortimetable.test.service.user.UserServiceTest;

@RunWith(Suite.class)
@SuiteClasses({
        UnitDAOTest.class,
        UserDAOTest.class,
        TutorialDAOTest.class,
        TutorAvailabilityDAOTest.class,
        TutorUnitPreferenceDAOTest.class,
        TutorialGroupDAOTest.class,
        TimetableSystemStateDAOTest.class,

        CsvExportParserTest.class,
        CsvImportParserTest.class,
        UnitMapperTest.class,
        TutorialMapperTest.class,

        AdministrationServiceTest.class,
        AssignmentServiceTest.class,
        CsvParsingServiceTest.class,
        TutorUnitPreferenceServiceTest.class,
        UnitServiceTest.class,
        TutorialGroupServiceTest.class,
        UserServiceTest.class,
        FileUploadServiceTest.class,
        TutorialServiceTest.class,
        TutorAvailabilityServiceTest.class
        // Test classes here.
})
public class TestSuite {
    // Leave Empty.
}
