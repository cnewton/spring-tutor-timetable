package au.edu.curtin.tutortimetable.test.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import au.edu.curtin.tutortimetable.test.security.controller.fileupload.FileUploadControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.tutoravailability.TutorAvailabilityControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.tutorial.TutorialControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.tutorialgroup.TutorialGroupControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.tutorunitpreference.TutorUnitPreferenceControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.unit.UnitControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.controller.user.UserControllerSecurityTest;
import au.edu.curtin.tutortimetable.test.security.core.CoreSecurityTest;

@RunWith(Suite.class)
@SuiteClasses({ UnitControllerSecurityTest.class,
        FileUploadControllerSecurityTest.class,
        UserControllerSecurityTest.class,
        TutorUnitPreferenceControllerSecurityTest.class,
        TutorialControllerSecurityTest.class,
        TutorialGroupControllerSecurityTest.class,
        TutorAvailabilityControllerSecurityTest.class,
        CoreSecurityTest.class })
public class SecurityTestSuite {
    // Remains Empty
}
