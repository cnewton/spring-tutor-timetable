package au.edu.curtin.tutortimetable.test.common;

import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:testdb-config.xml", "classpath:root-servlet.xml",
        "classpath:test-servlet.xml" })
public class SpringTest implements ApplicationContextAware {

    protected ApplicationContext applicationContext;

    protected MockHttpServletRequest request;

    protected ServletRequestAttributes attributes;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        this.applicationContext = applicationContext;
    }

    protected void scopedBeansConfig() {

        ConfigurableBeanFactory configurableBeanFactory = (ConfigurableBeanFactory) applicationContext
                .getAutowireCapableBeanFactory();
        configurableBeanFactory.registerScope("session", new SessionScope());
        configurableBeanFactory.registerScope("request", new RequestScope());
        request = new MockHttpServletRequest();
        attributes = new ServletRequestAttributes(request);
        RequestContextHolder.setRequestAttributes(attributes);
    }

    @Rule
    public TestRule watcher = new TestWatcher() {

        private long runTime;

        protected void starting(Description description) {

            runTime = System.currentTimeMillis();
            System.out.println("Starting test: " + description.getMethodName());
        }

        protected void succeeded(Description description) {

            runTime = System.currentTimeMillis() - runTime;
            System.out
                    .println(
                            "Finished test: " + description.getMethodName() + "\n\t" + "Time taken: " + runTime + "ms");
        }

        protected void failed(Throwable e, Description description) {

            runTime = System.currentTimeMillis() - runTime;
            System.err
                    .println("Failed test: " + description.getMethodName() + "\n" + "Time taken: " + runTime + "ms");
            System.err.println(e.getMessage() + "\n" + e);
        }
    };
}
