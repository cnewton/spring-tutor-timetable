package au.edu.curtin.tutortimetable.test.common;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialType;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.administrator.Administrator;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;

/**
 * This class provides predefined values for each of the Domain Objects.
 * 
 * @author Cameron Newton
 *
 */
public class DomainObjectFactory {

    public static Unit createUnit() {

        Unit unit = new Unit();
        unit.setId(1);
        unit.setUnitCode("COMP300");
        unit.setUnitName("Unix and C Programming");
        unit.setUnitCoordinator(createUnitCoordinator());
        unit.setTutorialGroups(new HashSet<TutorialGroup>());

        return unit;
    }

    public static User createUser() {

        User user = new User();
        user.setId(1);
        user.setName("John Tester");
        user.setEmail("Test@Test.com");
        user.setStaffId("15151511");
        user.setEnabled(true);
        user.setPassword("testtest");
        user.setMobile("0404040404");
        user.setUsername("123456784");

        return user;
    }

    public static Administrator createAdministrator() {

        Administrator user = new Administrator();
        user.setId(1);
        user.setName("John Tester");
        user.setEmail("Test@Test.com");
        user.setUsername("123456783");

        return user;
    }

    public static Tutor createTutor() {

        Tutor user = new Tutor();
        user.setId(2);
        user.setName("John Tester");
        user.setEmail("Test@Test.com");
        user.setUsername("123456781");
        user.setCourse("Bachelor Of Engineering");
        user.setYear(3);
        user.setAssignedHours(0);
        user.setFailedUnits(new String[] { "OOPD", "DSA" });
        user.setAssignedTutorials(new HashSet<Tutorial>());
        user.setPreferredUnits(new HashSet<TutorUnitPreference>());
        user.setAvailabilities(new HashSet<TutorAvailability>());

        return user;
    }

    public static UnitCoordinator createUnitCoordinator() {

        UnitCoordinator user = new UnitCoordinator();
        user.setId(2);
        user.setName("John Tester");
        user.setEmail("Test@Test.com");
        user.setUsername("123456782");

        return user;
    }

    public static Tutorial createTutorial() {

        Tutorial tutorial = new Tutorial();
        tutorial.setId(1);
        tutorial.setRoom("314.218");
        tutorial.setTutorialGroup(createTutorialGroup());
        tutorial.setTutor(createTutor());
        tutorial.setStartTime(LocalDateTime.of(2016, 3, 22, 14, 0, 0, 0));
        tutorial.setEndTime(tutorial.getStartTime().plusHours(2L));

        return tutorial;
    }

    public static TutorAvailability createTutorAvailability() {

        TutorAvailability tutorAvailability = new TutorAvailability();
        tutorAvailability.setDay(DayOfWeek.FRIDAY);
        tutorAvailability.setId(1);
        tutorAvailability.setTutor(createTutor());
        tutorAvailability.setStartTime(LocalTime.of(14, 0));
        tutorAvailability.setEndTime(LocalTime.of(16, 0));

        return tutorAvailability;
    }

    public static TutorialGroup createTutorialGroup() {

        TutorialGroup tutorialGroup = new TutorialGroup();
        tutorialGroup.setId(1);
        tutorialGroup.setGroupName("Lab1");
        tutorialGroup.setUnit(createUnit());
        tutorialGroup.setTutorials(new HashSet<Tutorial>());
        tutorialGroup.setTutorialType(TutorialType.DEMONSTRATION);

        return tutorialGroup;
    }

    public static TutorUnitPreference createTutorUnitPreference() {

        TutorUnitPreference tutorUnitPreference = new TutorUnitPreference();
        tutorUnitPreference.setId(1);
        tutorUnitPreference.setPriority(1);
        tutorUnitPreference.setTutor(createTutor());
        tutorUnitPreference.setUnit(createUnit());

        return tutorUnitPreference;
    }

    public static TutorDetailsDTO createTutorDetailsDTO() {

        TutorDetailsDTO dto = new TutorDetailsDTO();
        dto.setCourse("Bachelors");
        dto.setYear(3);
        dto.setFailedUnits(new String[] { "Fail1", "Fail2" });

        return dto;
    }
}
