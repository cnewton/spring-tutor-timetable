package au.edu.curtin.tutortimetable.test.security.controller.tutorial;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;
import au.edu.curtin.tutortimetable.test.security.DomainObjectControllerSecurityTest;

public class TutorialControllerSecurityTest extends SecurityAwareSpringTest implements
        DomainObjectControllerSecurityTest {

    private MockMvc mockMvc;

    private Tutorial obj;

    private String url = "/tutorial";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        obj = DomainObjectFactory.createTutorial();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testGETAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAllAsUnauthorized() throws Exception {

        mockMvc.perform(get(url)).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPOSTAsUser() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsTutor() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsCoordinator() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsAdmin() throws Exception {

        long countBefore = tutorialService.count();
        obj.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Tutorial returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = tutorialService.count();

        assertEquals("The count did not increase with create.", countBefore + 1, countAfter);

        // Cleanup
        tutorialService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTAsUnauthorized() throws Exception {

        obj.setId(0);

        mockMvc.perform(
                post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTAsUser() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorialService.delete(obj.getId());
    }

    @Test
    public void testPUTAsTutor() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorialService.delete(obj.getId());

    }

    @Test
    public void testPUTAsCoordinator() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorialService.delete(obj.getId());

    }

    @Test
    public void testPUTAsAdmin() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);
        long countBefore = tutorialService.count();

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Tutorial returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = tutorialService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        tutorialService.delete(returnedObject.getId());

    }

    @Test
    public void testPUTAsUnauthorized() throws Exception {

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEAsUser() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorialService.delete(obj);
    }

    @Test
    public void testDELETEAsTutor() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorialService.delete(obj);
    }

    @Test
    public void testDELETEAsCoordinator() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorialService.delete(obj);
    }

    @Test
    public void testDELETEAsAdmin() throws Exception {

        obj.setId(0);
        obj = tutorialService.create(obj);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testDELETEAsUnauthorized() throws Exception {

        mockMvc.perform(delete(url + "/" + obj.getId())).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testCountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testCountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testCountAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testLoadTutorialsByTutorAsUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialsByTutorAsTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialsByTutorAsTutorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().isOk());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialsByTutorAsCoordinator() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialsByTutorAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().isOk());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialsByTutorAsUnauthorized() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        mockMvc.perform(get("/tutorial/tutor/" + tutor.getId())).andExpect(status().is3xxRedirection());

        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().is4xxClientError());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().is4xxClientError());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsCoordinator() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(null);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().is4xxClientError());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsCoordinatorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setId(0);
        unitCoordinator.setUsername("testUserName");
        userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        String token = tokenHandler.createTokenStringForUser(unitCoordinator);

        mockMvc.perform(
                put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().isOk());

        // Cleanup
        tutorialService.delete(tutorial);
        tutorialGroupService.delete(group);
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().isOk());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialAsUnauthorized() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign")).andExpect(
                status().is3xxRedirection());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testClearAssignmentAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutorial/" + 1 + "/tutor/unassign").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testClearAssignmentAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutorial/" + 1 + "/tutor/unassign").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testClearAssignmentAsCoordinator() throws Exception {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coordinator);
        unitService.create(unit);

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        group.getTutorials().add(tutorial);
        tutorialGroupService.update(group);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/unassign").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

        // Cleanup
        tutorialGroupService.delete(group);
        userService.delete(tutor);
        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testClearAssignmentAsCoordinatorSameUser() throws Exception {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coordinator);
        unitService.create(unit);

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        group.getTutorials().add(tutorial);
        tutorialGroupService.update(group);

        String token = tokenHandler.createTokenStringForUser(coordinator);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/unassign").header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        tutorialGroupService.delete(group);
        userService.delete(tutor);
        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testClearAssignmentAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        group.getTutorials().add(tutorial);
        tutorialGroupService.update(group);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/unassign").header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        tutorialGroupService.delete(group);
        userService.delete(tutor);
    }

    @Test
    public void testClearAssignmentAsUnauthorized() throws Exception {

        mockMvc.perform(put("/tutorial/" + 1 + "/tutor/unassign"))
                .andExpect(status().is3xxRedirection());
    }
}
