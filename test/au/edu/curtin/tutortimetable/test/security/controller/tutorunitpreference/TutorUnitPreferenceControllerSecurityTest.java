package au.edu.curtin.tutortimetable.test.security.controller.tutorunitpreference;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;
import au.edu.curtin.tutortimetable.test.security.DomainObjectControllerSecurityTest;

public class TutorUnitPreferenceControllerSecurityTest extends SecurityAwareSpringTest implements
        DomainObjectControllerSecurityTest {

    private MockMvc mockMvc;

    private TutorUnitPreference testTutorUnitPreference;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        testTutorUnitPreference = DomainObjectFactory.createTutorUnitPreference();
    }

    @After
    public void tearDown() {

    }

    @Test
    @Override
    public void testGETAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/1").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());
    }

    @Test
    @Override
    public void testGETAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/1").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

    }

    @Test
    @Override
    public void testGETAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/1").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

    }

    @Test
    @Override
    public void testGETAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    @Override
    public void testGETAsUnauthorized() throws Exception {

        mockMvc.perform(get("/tutor/preference" + "/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    @Override
    public void testGETAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

    }

    @Test
    @Override
    public void testGETAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

    }

    @Test
    @Override
    public void testGETAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

    }

    @Test
    @Override
    public void testGETAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    @Override
    public void testGETAllAsUnauthorized() throws Exception {

        mockMvc.perform(get("/tutor/preference")).andExpect(status().is3xxRedirection());

    }

    @Test
    @Override
    public void testPOSTAsUser() throws Exception {

        testTutorUnitPreference.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());

    }

    @Test
    @Override
    public void testPOSTAsTutor() throws Exception {

        testTutorUnitPreference.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());

    }

    @Test
    @Override
    public void testPOSTAsCoordinator() throws Exception {

        testTutorUnitPreference.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());
    }

    @Test
    @Override
    public void testPOSTAsAdmin() throws Exception {

        long countBefore = tutorUnitPreferenceService.count();
        testTutorUnitPreference.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post("/tutor/preference").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        TutorUnitPreference returnedObject = jsonMapper.readValue(responseBody.getBytes(), TutorUnitPreference.class);

        long countAfter = tutorUnitPreferenceService.count();

        assertEquals("The count did not increase with create.", countBefore + 1, countAfter);

        // Cleanup
        tutorUnitPreferenceService.delete(returnedObject.getId());

    }

    @Test
    @Override
    public void testPOSTAsUnauthorized() throws Exception {

        testTutorUnitPreference.setId(0);

        mockMvc.perform(
                post("/tutor/preference").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    @Override
    public void testPUTAsUser() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());

        tutorUnitPreferenceService.delete(testTutorUnitPreference);
    }

    @Test
    @Override
    public void testPUTAsTutor() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());

        tutorUnitPreferenceService.delete(testTutorUnitPreference);

    }

    @Test
    @Override
    public void testPUTAsCoordinator() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/preference").header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(
                        status().is4xxClientError());

        tutorUnitPreferenceService.delete(testTutorUnitPreference);

    }

    @Test
    @Override
    public void testPUTAsAdmin() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);
        long countBefore = tutorUnitPreferenceService.count();

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put("/tutor/preference").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        TutorUnitPreference returnedObject = jsonMapper.readValue(responseBody.getBytes(), TutorUnitPreference.class);

        long countAfter = tutorUnitPreferenceService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        tutorUnitPreferenceService.delete(returnedObject.getId());

    }

    @Test
    @Override
    public void testPUTAsUnauthorized() throws Exception {

        mockMvc.perform(
                put("/tutor/preference").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(testTutorUnitPreference)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @Override
    public void testDELETEAsUser() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                delete("/tutor/preference" + "/" + testTutorUnitPreference.getId()).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

        tutorUnitPreferenceService.delete(testTutorUnitPreference);
    }

    @Test
    @Override
    public void testDELETEAsTutor() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                delete("/tutor/preference" + "/" + testTutorUnitPreference.getId()).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

        tutorUnitPreferenceService.delete(testTutorUnitPreference);
    }

    @Test
    @Override
    public void testDELETEAsCoordinator() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        UnitCoordinator testCoordinator = DomainObjectFactory.createUnitCoordinator();
        testCoordinator.setId(0);
        testCoordinator.setUsername("testname");
        userService.create(testCoordinator);

        String token = tokenHandler.createTokenStringForUser(testCoordinator);

        mockMvc.perform(
                delete("/tutor/preference" + "/" + testTutorUnitPreference.getId()).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());

        userService.delete(testCoordinator);
        tutorUnitPreferenceService.delete(testTutorUnitPreference);
    }

    @Test
    public void testDELETEAsCoordinatorSameUser() throws Exception {

        UnitCoordinator testCoordinator = DomainObjectFactory.createUnitCoordinator();
        testCoordinator.setId(0);
        testCoordinator.setUsername("testname");
        userService.create(testCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(testCoordinator);
        unitService.create(unit);

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference.setUnit(unit);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        String token = tokenHandler.createTokenStringForUser(testCoordinator);

        mockMvc.perform(
                delete("/tutor/preference" + "/" + testTutorUnitPreference.getId()).header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        unitService.delete(unit);
        userService.delete(testCoordinator);
    }

    @Test
    @Override
    public void testDELETEAsAdmin() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                delete("/tutor/preference" + "/" + testTutorUnitPreference.getId()).header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

    }

    @Test
    @Override
    public void testDELETEAsUnauthorized() throws Exception {

        mockMvc.perform(delete("/tutor/preference" + "/" + testTutorUnitPreference.getId())).andExpect(
                status().is3xxRedirection());

    }

    @Test
    @Override
    public void testCountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/count").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

    }

    @Test
    @Override
    public void testCountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/count").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());
    }

    @Test
    @Override
    public void testCountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/count").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());
    }

    @Test
    @Override
    public void testCountAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/preference" + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    @Override
    public void testCountAsUnauthorized() throws Exception {

        mockMvc.perform(get("/tutor/preference" + "/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testCreateTutorPreferenceAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", "1").param("unitId", "1")
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCreateTutorPreferenceAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", String.valueOf(1)).param("unitId", String.valueOf(1))
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCreateTutorPreferenceAsTutorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);
        String tutorId = String.valueOf(tutor.getId());

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);
        String unitId = String.valueOf(unit.getId());

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", tutorId).param("unitId", unitId)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        tutorUnitPreferenceService.delete(tutorUnitPreferenceService.findAll().get(
                tutorUnitPreferenceService.findAll().size() - 1));
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testCreateTutorPreferenceUnitCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", String.valueOf(1)).param("unitId", String.valueOf(1))
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCreateTutorPreferenceAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);
        String tutorId = String.valueOf(tutor.getId());

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);
        String unitId = String.valueOf(unit.getId());

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", tutorId).param("unitId", unitId)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        tutorUnitPreferenceService.delete(tutorUnitPreferenceService.findAll().get(
                tutorUnitPreferenceService.findAll().size() - 1));
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testCreateTutorPreferenceAsUnauthorized() throws Exception {

        mockMvc.perform(
                post("/tutor/preference/create").param("tutorId", String.valueOf(1)).param("unitId", String.valueOf(1)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutor/preference/1/priority").param("priority", "1").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutor/preference/1/priority").param("priority", "1").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        user.setId(5);
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/tutor/preference/3/priority").param("priority", "1").header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsCoordinatorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);

        TutorUnitPreference pref = tutorUnitPreferenceService.createTutorPreference(tutor.getId(), unit.getId());

        String token = tokenHandler.createTokenStringForUser(unitCoordinator);

        mockMvc.perform(
                put("/tutor/preference/" + pref.getId() + "/priority").param("priority", "5").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().isOk());

        pref = tutorUnitPreferenceService.find(pref.getId());
        assertEquals("The priority was not correctly updated.", 5, pref.getPriority());

        // Cleanup
        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);

        TutorUnitPreference pref = tutorUnitPreferenceService.createTutorPreference(tutor.getId(), unit.getId());

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/preference/" + pref.getId() + "/priority").param("priority", "5").header(AUTH_HEADER_NAME,
                        token))
                .andExpect(status().isOk());

        pref = tutorUnitPreferenceService.find(pref.getId());
        assertEquals("The priority was not correctly updated.", 5, pref.getPriority());

        // Cleanup
        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testUpdateTutorPreferencePriorityAsUnauthorized() throws Exception {

        mockMvc.perform(put("/tutor/preference/1/priority").param("priority", "1")).andExpect(
                status().is3xxRedirection());
    }

    @Test
    public void testAddPreferenceListAsUser() throws Exception {

        long[] unitIds = { 1, 2 };

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testAddPreferenceListAsTutor() throws Exception {

        long[] unitIds = { 1, 2 };

        Tutor tutor = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testAddPreferenceListAsTutorSameUser() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long[] unitIds = { unit.getId(), unit2.getId() };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
        unitService.delete(unit2);
    }

    @Test
    public void testAddPreferenceListAsCoordinator() throws Exception {

        long[] unitIds = { 1, 2 };

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testAddPreferenceListAsAdmin() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long[] unitIds = { unit.getId(), unit2.getId() };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
        unitService.delete(unit2);
    }

    @Test
    public void testAddPreferenceListAsUnauthorized() throws Exception {

        long[] unitIds = { 1, 2 };

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/add").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testSubtractPreferenceListAsUser() throws Exception {

        long[] unitIds = { 1, 2 };

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSubtractPreferenceListAsTutor() throws Exception {

        long[] unitIds = { 1, 2 };

        Tutor tutor = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSubtractPreferenceListAsTutorSameUser() throws Exception {

        long[] unitIds = { 1, 2 };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);

    }

    @Test
    public void testSubtractPreferenceListAsCoordinator() throws Exception {

        long[] unitIds = { 1, 2 };

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSubtractPreferenceListAsAdmin() throws Exception {

        long[] unitIds = { 1, 2 };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractPreferenceListAsUnauthorized() throws Exception {

        long[] unitIds = { 1, 2 };

        mockMvc.perform(
                put("/tutor/" + 1 + "/preference/subtract").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testLoadForUnitAsUser() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
    }

    @Test
    public void testLoadForUnitAsTutor() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
    }

    @Test
    public void testLoadForUnitAsCoordinator() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference").header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void testLoadForUnitAsCoordinatorSameUser() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        String token = tokenHandler.createTokenStringForUser(coord);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference").header(AUTH_HEADER_NAME, token)).andExpect(
                status().isOk());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void testLoadForUnitAsAdmin() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference").header(AUTH_HEADER_NAME, token)).andExpect(
                status().isOk());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
    }

    @Test
    public void testLoadForUnitAsUnauthorized() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference")).andExpect(status().is3xxRedirection());

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
    }

}
