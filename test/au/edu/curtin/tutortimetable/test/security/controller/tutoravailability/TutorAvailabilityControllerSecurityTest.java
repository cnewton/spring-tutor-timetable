package au.edu.curtin.tutortimetable.test.security.controller.tutoravailability;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;
import au.edu.curtin.tutortimetable.test.security.DomainObjectControllerSecurityTest;

public class TutorAvailabilityControllerSecurityTest extends SecurityAwareSpringTest implements
        DomainObjectControllerSecurityTest {

    private MockMvc mockMvc;

    private TutorAvailability obj;

    private String url = "/tutor/availability";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private UserService userService;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        obj = DomainObjectFactory.createTutorAvailability();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testGETAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAllAsUnauthorized() throws Exception {

        mockMvc.perform(get(url)).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPOSTAsUser() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsTutor() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsCoordinator() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsAdmin() throws Exception {

        long countBefore = tutorAvailabilityService.count();
        obj.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        TutorAvailability returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = tutorAvailabilityService.count();

        assertEquals("The count did not increase with create.", countBefore + 1, countAfter);

        // Cleanup
        tutorAvailabilityService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTAsUnauthorized() throws Exception {

        obj.setId(0);

        mockMvc.perform(
                post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTAsUser() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorAvailabilityService.delete(obj.getId());
    }

    @Test
    public void testPUTAsTutor() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorAvailabilityService.delete(obj.getId());

    }

    @Test
    public void testPUTAsCoordinator() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        tutorAvailabilityService.delete(obj.getId());

    }

    @Test
    public void testPUTAsAdmin() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);
        long countBefore = tutorAvailabilityService.count();

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        TutorAvailability returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = tutorAvailabilityService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        tutorAvailabilityService.delete(returnedObject.getId());

    }

    @Test
    public void testPUTAsUnauthorized() throws Exception {

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEAsUser() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorAvailabilityService.delete(obj);
    }

    @Test
    public void testDELETEAsTutor() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createTutor();
        user.setId(1000);
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorAvailabilityService.delete(obj);
    }

    @Test
    public void testDELETEAsTutorSameUser() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testDELETEAsCoordinator() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        user.setId(1000);
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        tutorAvailabilityService.delete(obj);
    }

    @Test
    public void testDELETEAsAdmin() throws Exception {

        obj.setId(0);
        obj = tutorAvailabilityService.create(obj);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testDELETEAsUnauthorized() throws Exception {

        mockMvc.perform(delete(url + "/" + obj.getId())).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testCountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testCountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testCountAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testAddAvailabilityListAsUser() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testAddAvailabilityListAsTutor() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testAddAvailabilityListAsTutorSameUser() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testAddAvailabilityListAsCoordinator() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testAddAvailabilityListAsAdmin() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractAvailabilityListAsUser() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(new long[] { 1, 2 })))
                .andExpect(
                        status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractAvailabilityListAsTutor() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(new long[] { 1, 2 })))
                .andExpect(
                        status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractAvailabilityListAsTutorSameUser() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(new long[] { 1, 2 })))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractAvailabilityListAsCoordinator() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(new long[] { 1, 2 })))
                .andExpect(
                        status().is4xxClientError());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractAvailabilityListAsAdministrator() throws JsonProcessingException, Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("TestUser1");
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(new long[] { 1, 2 })))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(tutor);
    }
}
