package au.edu.curtin.tutortimetable.test.security.controller.unit;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;
import au.edu.curtin.tutortimetable.test.security.DomainObjectControllerSecurityTest;

public class UnitControllerSecurityTest extends SecurityAwareSpringTest implements DomainObjectControllerSecurityTest {

    private MockMvc mockMvc;

    private Unit obj;

    private String url = "/unit";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        obj = DomainObjectFactory.createUnit();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testGETAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testGETAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testGETAllAsUnauthorized() throws Exception {

        mockMvc.perform(get(url)).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPOSTAsUser() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsTutor() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsCoordinator() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAsAdmin() throws Exception {

        long countBefore = unitService.count();
        obj.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Unit returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = unitService.count();

        assertEquals("The count did not increase with create.", countBefore + 1, countAfter);

        // Cleanup
        unitService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTAsUnauthorized() throws Exception {

        obj.setId(0);

        mockMvc.perform(
                post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTAsUser() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        unitService.delete(obj.getId());
    }

    @Test
    public void testPUTAsTutor() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        unitService.delete(obj.getId());

    }

    @Test
    public void testPUTAsCoordinator() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        unitService.delete(obj.getId());

    }

    @Test
    public void testPUTAsAdmin() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);
        long countBefore = unitService.count();

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Unit returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = unitService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        unitService.delete(returnedObject.getId());

    }

    @Test
    public void testPUTAsUnauthorized() throws Exception {

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEAsUser() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(obj);
    }

    @Test
    public void testDELETEAsTutor() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(obj);
    }

    @Test
    public void testDELETEAsCoordinator() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(obj);
    }

    @Test
    public void testDELETEAsAdmin() throws Exception {

        obj.setId(0);
        obj = unitService.create(obj);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testDELETEAsUnauthorized() throws Exception {

        mockMvc.perform(delete(url + "/" + obj.getId())).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testCountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testCountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testCountAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void getUnitsForCoordinatorAsUser() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/user/" + coord.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void getUnitsForCoordinatorAsTutor() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/user/" + coord.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void getUnitsForCoordinatorAsCoordinator() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/user/" + coord.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void getUnitsForCoordinatorAsCoordinatorSameUser() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        String token = tokenHandler.createTokenStringForUser(coord);

        mockMvc.perform(get("/unit/user/" + coord.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void getUnitsForCoordinatorAsAdmin() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/user/" + coord.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void getUnitsForCoordinatorAsUnauthorized() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        mockMvc.perform(get("/unit/user/" + coord.getId())).andExpect(status().is3xxRedirection());

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void testLoadAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/load").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testLoadAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/load").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testLoadAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/load").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Test
    public void testLoadAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/unit/load").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Test
    public void testLoadAllAsUnauthorized() throws Exception {

        mockMvc.perform(get("/unit/load")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testAssignCoordinatorAsUser() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId())
                .header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testAssignCoordinatorAsTutor() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId())
                .header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testAssignCoordinatorAsCoordinator() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId())
                .header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testAssignCoordinatorAsAdmin() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId())
                .header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

        unitService.delete(unit);
        userService.delete(coordinator);
    }

    @Test
    public void testAssignCoordinatorAsUnauthorized() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId()))
                .andExpect(status().is3xxRedirection());

        unitService.delete(unit);
        userService.delete(coordinator);
    }
}
