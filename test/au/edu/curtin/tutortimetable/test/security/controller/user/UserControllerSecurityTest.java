package au.edu.curtin.tutortimetable.test.security.controller.user;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;
import au.edu.curtin.tutortimetable.test.security.DomainObjectControllerSecurityTest;

public class UserControllerSecurityTest extends SecurityAwareSpringTest implements DomainObjectControllerSecurityTest {

    private MockMvc mockMvc;

    private User obj;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private TokenHandler tokenHandler;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    private String url = "/user";

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        obj = DomainObjectFactory.createUser();
    }

    @After
    public void tearDown() {

    }

    @Override
    @Test
    public void testGETAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Override
    @Test
    public void testGETAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testGETAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testGETAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/1").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Override
    @Test
    public void testGETAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/1")).andExpect(status().is3xxRedirection());

    }

    @Override
    @Test
    public void testGETAllAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testGETAllAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testGETAllAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testGETAllAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Override
    @Test
    public void testGETAllAsUnauthorized() throws Exception {

        mockMvc.perform(get(url)).andExpect(status().is3xxRedirection());

    }

    @Override
    @Test
    public void testPOSTAsUser() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testPOSTAsTutor() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testPOSTAsCoordinator() throws Exception {

        obj.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testPOSTAsAdmin() throws Exception {

        long countBefore = userService.count();
        obj.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = userService.count();

        assertEquals("The count did not increase with create.", countBefore + 1, countAfter);

        // Cleanup
        userService.delete(returnedObject.getId());

    }

    @Override
    @Test
    public void testPOSTAsUnauthorized() throws Exception {

        obj.setId(0);

        mockMvc.perform(
                post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());

    }

    @Override
    @Test
    public void testPUTAsUser() throws Exception {

        obj.setId(0);
        obj.setUsername("123456");
        obj = userService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        userService.delete(obj.getId());
    }

    @Test
    public void testPUTAsUserSameUser() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);
        long countBefore = userService.count();

        User user = DomainObjectFactory.createUser();
        user.setId(obj.getId());
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = userService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        userService.delete(returnedObject.getId());

    }

    @Override
    @Test
    public void testPUTAsTutor() throws Exception {

        obj.setId(0);
        obj.setUsername("123456");
        obj = userService.create(obj);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        userService.delete(obj.getId());

    }

    @Test
    public void testPUTAsTutorSameUser() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);
        long countBefore = userService.count();

        User user = DomainObjectFactory.createTutor();
        user.setId(obj.getId());
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = userService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        userService.delete(returnedObject.getId());

    }

    @Override
    @Test
    public void testPUTAsCoordinator() throws Exception {

        obj.setId(0);
        obj.setUsername("123456");
        obj = userService.create(obj);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is4xxClientError());

        // Then cleanup
        userService.delete(obj.getId());

    }

    @Test
    public void testPUTAsCoordinatorSameUser() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);
        long countBefore = userService.count();

        User user = DomainObjectFactory.createUnitCoordinator();
        user.setId(obj.getId());
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = userService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        userService.delete(returnedObject.getId());

    }

    @Override
    @Test
    public void testPUTAsAdmin() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);
        long countBefore = userService.count();

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        put(url).header(AUTH_HEADER_NAME, token).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = userService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Then cleanup
        userService.delete(returnedObject.getId());

    }

    @Override
    @Test
    public void testPUTAsUnauthorized() throws Exception {

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testPUTTutorDetailsAsUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testPUTTutorDetailsAsTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().is4xxClientError());

        userService.delete(tutor);

    }

    @Test
    public void testPUTTutorDetailsAsTutorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().isOk());

        userService.delete(tutor);

    }

    @Test
    public void testPUTTutorDetailsAsCoordinator() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().is4xxClientError());

        userService.delete(tutor);

    }

    @Test
    public void testPUTTutorDetailsAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().isOk());

        userService.delete(tutor);

    }

    @Test
    public void testPUTTutorDetailsAsUnauthorized() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().is3xxRedirection());

        userService.delete(tutor);
    }

    @Override
    @Test
    public void testDELETEAsUser() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        // Cleanup
        userService.delete(obj);
    }

    @Override
    @Test
    public void testDELETEAsTutor() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        // Cleanup
        userService.delete(obj);
    }

    @Override
    @Test
    public void testDELETEAsCoordinator() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        // Cleanup
        userService.delete(obj);
    }

    @Override
    @Test
    public void testDELETEAsAdmin() throws Exception {

        obj.setId(0);
        obj = userService.create(obj);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(delete(url + "/" + obj.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Override
    @Test
    public void testDELETEAsUnauthorized() throws Exception {

        mockMvc.perform(delete(url + "/" + obj.getId())).andExpect(status().is3xxRedirection());

    }

    @Override
    @Test
    public void testCountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());

    }

    @Override
    @Test
    public void testCountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Override
    @Test
    public void testCountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().is4xxClientError());
    }

    @Override
    @Test
    public void testCountAsAdmin() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get(url + "/count").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

    }

    @Override
    @Test
    public void testCountAsUnauthorized() throws Exception {

        mockMvc.perform(get(url + "/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testUpdateUserPasswordAsUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsTutor() throws Exception {

        User testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsCoordinator() throws Exception {

        User testUser = DomainObjectFactory.createUnitCoordinator();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUnitCoordinator();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsNoAuth() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsTutor() throws Exception {

        User testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsCoordinator() throws Exception {

        User testUser = DomainObjectFactory.createUnitCoordinator();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsAdmin() throws Exception {

        User testUser = DomainObjectFactory.createAdministrator();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header(AUTH_HEADER_NAME, token)
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsNoAuth() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testRegisterTutorAsUser() throws Exception {

        Tutor testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        // Then cleanup
        userService.delete(returnedObject);
    }

    @Test
    public void testRegisterTutorAsTutor() throws Exception {

        Tutor testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        // Then cleanup
        userService.delete(returnedObject);
    }

    @Test
    public void testRegisterTutorAsCoordinator() throws Exception {

        Tutor testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        // Then cleanup
        userService.delete(returnedObject);
    }

    @Test
    public void testRegisterTutorAsAdmin() throws Exception {

        Tutor testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").header(AUTH_HEADER_NAME, token)
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        // Then cleanup
        userService.delete(returnedObject);
    }

    @Test
    public void testRegisterTutorAsUnauthorized() throws Exception {

        Tutor testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                                jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        // Then cleanup
        userService.delete(returnedObject);
    }

    @Test
    public void testGetAccountAsUser() throws Exception {

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);
        mockMvc.perform(get("/account").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());
    }

    @Test
    public void testGetAccountAsTutor() throws Exception {

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);
        mockMvc.perform(get("/account").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());
    }

    @Test
    public void testGetAccountAsCoordinator() throws Exception {

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);
        mockMvc.perform(get("/account").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());
    }

    @Test
    public void testGetAccountAsAdministrator() throws Exception {

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);
        mockMvc.perform(get("/account").header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());
    }

    @Test
    public void testGetAccountAsUnauthorized() throws Exception {

        mockMvc.perform(get("/account")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testLoadTutorAsUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorAsTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorAsTutorSameUser() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        String token = tokenHandler.createTokenStringForUser(tutor);

        mockMvc.perform(get("/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorAsCoordinator() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(
                status().is4xxClientError());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorAsAdmin() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(get("/tutor/" + tutor.getId()).header(AUTH_HEADER_NAME, token)).andExpect(status().isOk());

        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorAsUnauthorized() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        mockMvc.perform(get("/tutor/" + tutor.getId())).andExpect(status().is3xxRedirection());

        userService.delete(tutor);
    }
}
