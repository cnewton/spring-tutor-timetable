package au.edu.curtin.tutortimetable.test.security.controller.fileupload;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;

public class FileUploadControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    private MockMultipartFile uploadedFile;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UnitService unitService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private Filter springSecurityFilterChain;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testUploadUnitCsvAsUser() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadUnitCsvAsTutor() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadUnitCsvAsCoordinator() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile).header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadUnitCsvAsAdmin() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile).header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        List<Unit> units = unitService.findAll();
        for (int ii = 0; ii < 4; ii++) {
            Unit lastUnit = units.get(units.size() - 1); // Latest added unit
            units.remove(lastUnit);
            unitService.delete(lastUnit);
        }
    }

    @Test
    public void testUploadTutorialCsvAsUser() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createUser();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadTutorialCsvAsTutor() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createTutor();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadTutorialCsvAsCoordinator() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createUnitCoordinator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testUploadTutorialCsvAsAdmin() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("UnitA");
        unitService.create(unit);

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        User user = DomainObjectFactory.createAdministrator();
        String token = tokenHandler.createTokenStringForUser(user);

        mockMvc.perform(
                MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)
                        .header(AUTH_HEADER_NAME, token))
                .andExpect(status().isOk());

        // Cleanup
        List<Tutorial> tutorials = tutorialService.findAll();
        for (int ii = 0; ii < 4; ii++) {
            Tutorial lastTutorial = tutorials.get(tutorials.size() - 1); // Latest added unit
            tutorials.remove(lastTutorial);
            tutorialService.delete(lastTutorial);
        }
    }

}
