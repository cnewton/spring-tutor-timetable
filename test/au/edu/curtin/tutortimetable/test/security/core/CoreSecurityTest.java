package au.edu.curtin.tutortimetable.test.security.core;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.security.token.TokenHandler;
import au.edu.curtin.tutortimetable.security.token.UserToken;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SecurityAwareSpringTest;

public class CoreSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenHandler tokenHandler;

    private User testUser;

    @Value("${AUTH_HEADER_NAME}")
    private String AUTH_HEADER_NAME;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
        testUser = DomainObjectFactory.createUser();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testValidUserLogin() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        login(mockMvc, testUser.getUsername(), "testpassword").andExpect(status().isOk());
        userService.delete(testUser);
    }

    @Test
    @Transactional
    public void testInValidLoginNoUsername() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        login(mockMvc, null, "testpassword").andExpect(status().is4xxClientError());
        userService.delete(testUser);
    }

    @Test
    @Transactional
    public void testInValidLoginWrongUsername() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        login(mockMvc, "wrongusername", "testpassword").andExpect(status().is4xxClientError());
        userService.delete(testUser);
    }

    @Test
    @Transactional
    public void testInValidLoginNoPassword() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        login(mockMvc, testUser.getUsername(), null).andExpect(status().is4xxClientError());
        userService.delete(testUser);
    }

    @Test
    @Transactional
    public void testInValidLoginWrongPassword() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        login(mockMvc, testUser.getUsername(), "wrongPassword").andExpect(status().is4xxClientError());
        userService.delete(testUser);
    }

    @Test
    @Transactional
    public void testRequestWithTamperedToken() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);
        token = token.substring(5);

        mockMvc.perform(get("/unit/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void testRequestWithMalformedToken() throws Exception {

        String token = "Malformed";

        mockMvc.perform(get("/unit/1").header(AUTH_HEADER_NAME, token)).andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void testRequestWithExpiredToken() throws Exception {

        testUser.setId(0);
        testUser.setPassword("testpassword");
        testUser.setUsername("111111111");
        userService.create(testUser);
        String token = tokenHandler.createTokenStringForUser(testUser);

        UserToken uToken = tokenHandler.parseTokenString(token);

        uToken.setExpiryTime(1);

        String expiredToken = tokenHandler.createTokenStringForUser(uToken);

        mockMvc.perform(get("/unit/1").header(AUTH_HEADER_NAME, expiredToken)).andExpect(status().is3xxRedirection());
    }

    protected ResultActions login(MockMvc mvc, String institutionId, String password) throws Exception {

        RequestBuilder request = formLogin("/j_spring_security_check").user(institutionId).password(password);

        return mvc.perform(request);
    }
}
