package au.edu.curtin.tutortimetable.test.security;

public interface DomainObjectControllerSecurityTest {

    public void testGETAsUser() throws Exception;

    public void testGETAsTutor() throws Exception;

    public void testGETAsCoordinator() throws Exception;

    public void testGETAsAdmin() throws Exception;

    public void testGETAsUnauthorized() throws Exception;

    public void testGETAllAsUser() throws Exception;

    public void testGETAllAsTutor() throws Exception;

    public void testGETAllAsCoordinator() throws Exception;

    public void testGETAllAsAdmin() throws Exception;

    public void testGETAllAsUnauthorized() throws Exception;

    public void testPOSTAsUser() throws Exception;

    public void testPOSTAsTutor() throws Exception;

    public void testPOSTAsCoordinator() throws Exception;

    public void testPOSTAsAdmin() throws Exception;

    public void testPOSTAsUnauthorized() throws Exception;

    public void testPUTAsUser() throws Exception;

    public void testPUTAsTutor() throws Exception;

    public void testPUTAsCoordinator() throws Exception;

    public void testPUTAsAdmin() throws Exception;

    public void testPUTAsUnauthorized() throws Exception;

    public void testDELETEAsUser() throws Exception;

    public void testDELETEAsTutor() throws Exception;

    public void testDELETEAsCoordinator() throws Exception;

    public void testDELETEAsAdmin() throws Exception;

    public void testDELETEAsUnauthorized() throws Exception;

    public void testCountAsUser() throws Exception;

    public void testCountAsTutor() throws Exception;

    public void testCountAsCoordinator() throws Exception;

    public void testCountAsAdmin() throws Exception;

    public void testCountAsUnauthorized() throws Exception;

}
