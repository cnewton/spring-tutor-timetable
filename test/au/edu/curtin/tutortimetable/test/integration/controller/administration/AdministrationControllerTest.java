package au.edu.curtin.tutortimetable.test.integration.controller.administration;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.edu.curtin.tutortimetable.common.CustomObjectMapper;
import au.edu.curtin.tutortimetable.service.administration.AdministrationService;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class AdministrationControllerTest extends SpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CustomObjectMapper jsonMapper;

    @Autowired
    private AdministrationService administrationService;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetState() throws Exception {

        mockMvc.perform(get("/admin/systemstate")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testSetSemesterStartDate() throws Exception {

        LocalDate before = administrationService.getSystemState().getSemesterStart();

        LocalDate date = LocalDate.now();

        mockMvc.perform(put("/admin/systemstate/semester/start").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(date))).andExpect(status().isOk());

        // Ensure it is in database.
        LocalDate after = administrationService.getSystemState().getSemesterStart();

        assertEquals("The new date should be in the database.", date, after);

        // Then cleanup
        administrationService.setSemesterStartDate(before);
    }

    @Test
    public void testSetSemesterEndDate() throws Exception {

        LocalDate before = administrationService.getSystemState().getSemesterEnd();

        LocalDate date = LocalDate.now();

        mockMvc.perform(put("/admin/systemstate/semester/end").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(date))).andExpect(status().isOk());

        // Ensure it is in database.
        LocalDate after = administrationService.getSystemState().getSemesterEnd();

        assertEquals("The new date should be in the database.", date, after);

        // Then cleanup
        administrationService.setSemesterEndDate(before);
    }

    @Test
    public void testSetApplicationStartDate() throws Exception {

        LocalDate before = administrationService.getSystemState().getApplicationStart();

        LocalDate date = LocalDate.now();

        mockMvc.perform(put("/admin/systemstate/application/start").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(date))).andExpect(status().isOk());

        // Ensure it is in database.
        LocalDate after = administrationService.getSystemState().getApplicationStart();

        assertEquals("The new date should be in the database.", date, after);

        // Then cleanup
        administrationService.setApplicationStartDate(before);
    }

    @Test
    public void testSetApplicationEndDate() throws Exception {

        LocalDate before = administrationService.getSystemState().getApplicationEnd();

        LocalDate date = LocalDate.now();

        mockMvc.perform(put("/admin/systemstate/application/end").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(date))).andExpect(status().isOk());

        // Ensure it is in database.
        LocalDate after = administrationService.getSystemState().getApplicationEnd();

        assertEquals("The new date should be in the database.", date, after);

        // Then cleanup
        administrationService.setApplicationEndDate(before);
    }

    @Test
    public void testSetLockoutDate() throws Exception {

        LocalDate before = administrationService.getSystemState().getLockout();

        LocalDate date = LocalDate.now();

        mockMvc.perform(put("/admin/systemstate/lockout").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(date))).andExpect(status().isOk());

        // Ensure it is in database.
        LocalDate after = administrationService.getSystemState().getLockout();

        assertEquals("The new date should be in the database.", date, after);

        // Then cleanup
        administrationService.setLockoutDate(before);
    }
}
