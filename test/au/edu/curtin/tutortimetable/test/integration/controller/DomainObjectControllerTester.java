package au.edu.curtin.tutortimetable.test.integration.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import au.edu.curtin.tutortimetable.common.CustomObjectMapper;
import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

@Component
public class DomainObjectControllerTester<T extends IdBasedDomainObject> {

    private DomainObjectService<T> service;

    private T obj;

    private String url;

    private MockMvc mockMvc;

    @Autowired
    private CustomObjectMapper jsonMapper;

    public void setService(DomainObjectService<T> service) {

        this.service = service;
    }

    public void setTestObject(T obj) {

        this.obj = obj;
    }

    public void setURL(String url) {

        this.url = url;
    }

    public void setMockMvc(MockMvc mvc) {

        this.mockMvc = mvc;
    }

    public void testGET() throws Exception {

        mockMvc.perform(get(url + "/1")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));

    }

    public void testGETNonExistant() throws Exception {

        mockMvc.perform(get(url + "/-1")).andExpect(status().isNotFound());

    }

    public void testGETAll() throws Exception {

        mockMvc.perform(get(url)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(3)));
    }

    public void testPOST() throws Exception {

        obj.setId(0);

        long countBefore = service.count();

        MvcResult result = mockMvc
                .perform(
                        post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(
                                jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        @SuppressWarnings("unchecked")
        T returnedObject = (T) jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = service.count();

        assertEquals("The count did not increase with creation.", countBefore + 1, countAfter);

        // Ensure it was created in database.
        service.find(returnedObject.getId());

        // Then cleanup
        service.delete(returnedObject.getId());
    }

    public void testPOSTNotCreate() throws Exception {

        obj.setId(5);

        mockMvc.perform(
                post(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isBadRequest());

    }

    public void testPUT() throws Exception {

        long countBefore = service.count();

        MvcResult result = mockMvc
                .perform(
                        put(url).contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        @SuppressWarnings("unchecked")
        T returnedObject = (T) jsonMapper.readValue(responseBody.getBytes(), obj.getClass());

        long countAfter = service.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Ensure it is in database.
        service.find(returnedObject.getId());

        // Then cleanup
        service.delete(returnedObject.getId());

    }

    public void testPUTNotUpdate() throws Exception {

        obj.setId(0);

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isBadRequest());
    }

    public void testPUTNonExistant() throws Exception {

        obj.setId(-1);

        mockMvc.perform(
                put(url).contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isNotFound());
    }

    public void testDELETE() throws Exception {

        mockMvc.perform(delete(url + "/" + obj.getId())).andExpect(status().isOk());

        service.find(obj.getId());

    }

    public void testDELETENonExistant() throws Exception {

        mockMvc.perform(
                delete(url + "/-1").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(obj)))
                .andExpect(status().isNotFound());
    }

    public void testCount() throws Exception {

        mockMvc.perform(get(url + "/count")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(content().string("3"));
    }

}
