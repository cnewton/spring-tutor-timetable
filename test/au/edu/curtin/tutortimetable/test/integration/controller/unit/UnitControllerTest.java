package au.edu.curtin.tutortimetable.test.integration.controller.unit;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class UnitControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private Unit testUnit;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectControllerTester<Unit> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(unitService);
        testUnit = DomainObjectFactory.createUnit();
        tester.setTestObject(testUnit);
        tester.setURL("/unit");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testUnit.setId(0);
        testUnit = unitService.create(testUnit);

        tester.testPUT();

    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testUnit.setId(0);
        unitService.create(testUnit);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testFindUnitsByCoordinator() throws Exception {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        mockMvc.perform(get("/unit/user/" + coord.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(1)));

        unitService.delete(unit);
        userService.delete(coord);
    }

    @Test
    public void testLoadAll() throws Exception {

        long countBefore = unitService.count();

        Unit unit1 = DomainObjectFactory.createUnit();
        unit1.setId(0);
        unitService.create(unit1);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        MvcResult result = mockMvc.perform(get("/unit/load")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize((int) countBefore + 2))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Unit[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), Unit[].class);

        assertEquals("The units should include their loaded coordinators.", unit1.getUnitCoordinator().getId(),
                returnedObject[0].getUnitCoordinator().getId());

        unitService.delete(unit1);
        unitService.delete(unit2);
    }

    @Test
    public void testAssignCoordinator() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        mockMvc.perform(put("/unit/" + unit.getId() + "/assign/" + coordinator.getId())).andExpect(status().isOk());

        unitService.delete(unit);
        userService.delete(coordinator);
    }
}
