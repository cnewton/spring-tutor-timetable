package au.edu.curtin.tutortimetable.test.integration.controller.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class UserControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private User testUser;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private DomainObjectControllerTester<User> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(userService);
        testUser = DomainObjectFactory.createUser();
        tester.setTestObject(testUser);
        tester.setURL("/user");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testUser.setId(0);
        testUser = userService.create(testUser);

        tester.testPUT();

    }

    @Test
    public void testPUTTutor() throws JsonProcessingException, Exception {

        // Note the tutor id here should match an existing tutor.
        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);

        userService.create(tutor);

        tutor.setStudentId("123444555");

        long countBefore = userService.count();

        MvcResult result = mockMvc
                .perform(
                        put("/user").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                                jsonMapper.writeValueAsBytes(tutor)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Tutor returnedObject = jsonMapper.readValue(responseBody.getBytes(), Tutor.class);

        assertEquals("The names do not match", "123444555", returnedObject.getStudentId());

        long countAfter = userService.count();

        assertEquals("The count increased with update.", countBefore, countAfter);

        // Ensure it is in database.
        userService.find(returnedObject.getId());

        // Then cleanup
        userService.delete(returnedObject.getId());

    }

    @Test
    public void testPUTTutorDetails() throws JsonProcessingException, Exception {

        // Note the tutor id here should match an existing tutor.
        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);

        userService.create(tutor);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/details").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().isOk());

        tutor = userService.findTutor(tutor.getId());

        assertEquals("The course was not updated.", dto.getCourse(), tutor.getCourse());

        // Then cleanup
        userService.delete(tutor);

    }

    @Test
    public void testPUTNonExistantTutorDetails() throws JsonProcessingException, Exception {

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        mockMvc.perform(
                put("/tutor/-1/details").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(dto)))
                .andExpect(status().isNotFound());
    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testUser.setId(0);
        userService.create(testUser);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testRegisterTutor() throws Exception {

        testUser = DomainObjectFactory.createTutor();
        testUser.setId(0);

        long countBefore = userService.count();

        MvcResult result = mockMvc
                .perform(
                        post("/user/register/tutor").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                                jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), testUser.getClass());

        long countAfter = userService.count();

        assertEquals("The count did not increase with creation.", countBefore + 1, countAfter);

        // Ensure it was created in database.
        userService.find(returnedObject.getId());

        // Then cleanup
        userService.delete(returnedObject.getId());
    }

    @Test
    public void testRegisterTutorNotTutor() throws Exception {

        testUser = DomainObjectFactory.createAdministrator();
        testUser.setId(0);

        mockMvc.perform(
                post("/user/register/tutor").contentType(MediaType.APPLICATION_JSON_UTF8).content(
                        jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testGetAccount() throws Exception {

        mockMvc.perform(get("/account")).andExpect(status().isOk());
    }

    @Test
    public void testLoadTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        avail.setTutor(tutor);
        tutorAvailabilityService.create(avail);

        MvcResult result = mockMvc.perform(get("/tutor/" + tutor.getId())).andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Tutor returnedObject = jsonMapper.readValue(responseBody.getBytes(), Tutor.class);

        assertEquals("The tutor should include its loaded availability.", 1, returnedObject.getAvailabilities().size());

        tutorAvailabilityService.delete(avail);
        userService.delete(tutor);
    }
}
