package au.edu.curtin.tutortimetable.test.integration.controller.tutorialgroup;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class TutorialGroupControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private TutorialGroup testTutorialGroup;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private DomainObjectControllerTester<TutorialGroup> tester;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(tutorialGroupService);
        testTutorialGroup = DomainObjectFactory.createTutorialGroup();
        tester.setTestObject(testTutorialGroup);
        tester.setURL("/tutorial/group");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testTutorialGroup.setId(0);
        testTutorialGroup = tutorialGroupService.create(testTutorialGroup);

        tester.testPUT();

    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testTutorialGroup.setId(0);
        tutorialGroupService.create(testTutorialGroup);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testAssignTutorialGroupGroupToTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup tutorialGroup = DomainObjectFactory.createTutorialGroup();
        tutorialGroup.setId(0);
        tutorialGroupService.create(tutorialGroup);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        tutorialGroupService.update(tutorialGroup);

        mockMvc.perform(put("/tutorial/group/" + tutorialGroup.getId() + "/tutor/" + tutor.getId() + "/assign"))
                .andExpect(status().isOk());

        assertEquals("There should now be 2 assigned tutorials.", 2, tutorialService
                .loadTutorialsForTutor(tutor.getId()).size());

        // Cleanup
        tutorialGroupService.delete(tutorialGroup);
        userService.delete(tutor);
    }

    @Test
    public void testLoadTutorialGroupsForUnit() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        mockMvc.perform(get("/tutorial/group/unit/" + unit.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)));

        // Cleanup
        tutorialGroupService.delete(group);
        unitService.delete(unit);
    }

    @Test
    public void testClearAssignment() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        group.getTutorials().add(tutorial);
        tutorialGroupService.update(group);

        mockMvc.perform(put("/tutorial/group/" + group.getId() + "/tutor/unassign"))
                .andExpect(status().isOk());

        tutorial = tutorialService.find(tutorial.getId());

        assertEquals("The tutorial should not have a tutor", null, tutorial.getTutor());

        tutorialGroupService.delete(group);
        userService.delete(tutor);
    }
}
