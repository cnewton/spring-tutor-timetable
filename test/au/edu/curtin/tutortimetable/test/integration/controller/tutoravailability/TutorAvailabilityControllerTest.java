package au.edu.curtin.tutortimetable.test.integration.controller.tutoravailability;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class TutorAvailabilityControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private TutorAvailability testTutorAvailability;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectControllerTester<TutorAvailability> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(tutorAvailabilityService);
        testTutorAvailability = DomainObjectFactory.createTutorAvailability();
        tester.setTestObject(testTutorAvailability);
        tester.setURL("/tutor/availability");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testTutorAvailability.setId(0);
        testTutorAvailability = tutorAvailabilityService.create(testTutorAvailability);

        tester.testPUT();

    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testTutorAvailability.setId(0);
        tutorAvailabilityService.create(testTutorAvailability);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testAddAvailabilityList() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("testUser");
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/add").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(list)))
                .andExpect(status().isOk());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain two availabilities.", 2, tutor.getAvailabilities().size());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testSubtractPreferenceList() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        tutorAvailabilityService.listAddAvailabilities(tutor.getId(), list);

        tutor = userService.loadTutor(tutor.getId());

        long[] ids = { tutor.getAvailabilities().iterator().next().getId() };

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/availability/subtract").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(ids)))
                .andExpect(status().isOk());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain 0 availabilities.", 0, tutor.getAvailabilities().size());

        // Cleanup
        userService.delete(tutor);
    }
}
