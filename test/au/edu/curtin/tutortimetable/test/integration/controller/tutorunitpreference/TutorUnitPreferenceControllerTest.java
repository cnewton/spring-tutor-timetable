package au.edu.curtin.tutortimetable.test.integration.controller.tutorunitpreference;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class TutorUnitPreferenceControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private TutorUnitPreference testTutorUnitPreference;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private DomainObjectControllerTester<TutorUnitPreference> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(tutorUnitPreferenceService);
        testTutorUnitPreference = DomainObjectFactory.createTutorUnitPreference();
        tester.setTestObject(testTutorUnitPreference);
        tester.setURL("/tutor/preference");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testTutorUnitPreference.setId(0);
        testTutorUnitPreference = tutorUnitPreferenceService.create(testTutorUnitPreference);

        tester.testPUT();

    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testTutorUnitPreference.setId(0);
        tutorUnitPreferenceService.create(testTutorUnitPreference);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testCreateTutorPreference() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);
        String tutorId = String.valueOf(tutor.getId());

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);
        String unitId = String.valueOf(unit.getId());

        mockMvc.perform(post("/tutor/preference/create").param("tutorId", tutorId).param("unitId", unitId)).andExpect(
                status().isOk());

        // Cleanup
        tutorUnitPreferenceService.delete(tutorUnitPreferenceService.findAll().get(
                tutorUnitPreferenceService.findAll().size() - 1));
        unitService.delete(unit);
        userService.delete(unitCoordinator);
        userService.delete(tutor);
    }

    @Test
    public void testCreateTutorPreferenceNonExistantTutor() throws Exception {

        UnitCoordinator unitCoordinator = DomainObjectFactory.createUnitCoordinator();
        unitCoordinator.setUsername("username2");
        unitCoordinator.setId(0);
        unitCoordinator = (UnitCoordinator) userService.create(unitCoordinator);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(unitCoordinator);
        unit = unitService.create(unit);
        String unitId = String.valueOf(unit.getId());

        mockMvc.perform(post("/tutor/preference/create").param("tutorId", String.valueOf(-1)).param("unitId", unitId))
                .andExpect(status().isNotFound());

        // Cleanup
        unitService.delete(unit);
        userService.delete(unitCoordinator);
    }

    @Test
    public void testCreateTutorPreferenceNonExistantUnit() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("username1");
        tutor.setId(0);
        tutor = (Tutor) userService.create(tutor);
        String tutorId = String.valueOf(tutor.getId());

        mockMvc.perform(post("/tutor/preference/create").param("tutorId", tutorId).param("unitId", String.valueOf(-1)))
                .andExpect(status().isNotFound());

        // Cleanup
        userService.delete(tutor);
    }

    @Test
    public void testUpdatePreferencePriority() throws Exception {

        TutorUnitPreference pref = tutorUnitPreferenceService.find(1);

        mockMvc.perform(put("/tutor/preference/" + pref.getId() + "/priority").param("priority", String.valueOf(5)))
                .andExpect(status().isOk());

        TutorUnitPreference pref2 = tutorUnitPreferenceService.find(1);
        assertEquals("The priority was not correctly updated.", 5, pref2.getPriority());
        // Cleanup
        tutorUnitPreferenceService.update(pref);
    }

    @Test
    public void testUpdatePreferencePriorityNonExistantPreference() throws Exception {

        mockMvc.perform(put("/tutor/preference/-1/priority").param("priority", String.valueOf(5))).andExpect(
                status().isNotFound());
    }

    @Test
    public void testUpdatePreferencePriorityInvalidPriority() throws Exception {

        TutorUnitPreference pref = tutorUnitPreferenceService.find(1);

        mockMvc.perform(put("/tutor/preference/" + pref.getId() + "/priority").param("priority", String.valueOf(-5)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testAddPreferenceList() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long[] unitIds = { unit.getId(), unit2.getId() };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/add").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain two products.", 2, tutor.getPreferredUnits().size());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
        unitService.delete(unit2);
    }

    @Test
    public void testAddPreferenceListNonExistantUnit() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        long[] unitIds = { unit.getId(), -1 };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/add").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isNotFound());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain two products.", 0, tutor.getPreferredUnits().size());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
    }

    @Test
    public void testSubtractPreferenceList() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long[] unitIds = { unit.getId(), unit2.getId() };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        tutorUnitPreferenceService.listAddTutorPreferences(tutor.getId(), new long[] { unit.getId(), unit2.getId() });

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/subtract").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain 0 products.", 0, tutor.getPreferredUnits().size());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
        unitService.delete(unit2);
    }

    @Test
    public void testSubtractPreferenceListNonExistantUnit() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long[] unitIds = { unit.getId(), unit2.getId() };

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        tutorUnitPreferenceService.listAddTutorPreferences(tutor.getId(), new long[] { unit.getId(), unit2.getId() });

        unitIds[1] = -1;

        mockMvc.perform(
                put("/tutor/" + tutor.getId() + "/preference/subtract").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(unitIds)))
                .andExpect(status().isOk());

        tutor = userService.loadTutor(tutor.getId());

        assertEquals("The Tutor should contain 1 products.", 1, tutor.getPreferredUnits().size());

        // Cleanup
        userService.delete(tutor);
        unitService.delete(unit);
        unitService.delete(unit2);
    }

    @Test
    public void testLoadForUnit() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        mockMvc.perform(get("/unit/" + unit.getId() + "/preference")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(1)));

        tutorUnitPreferenceService.delete(pref);
        unitService.delete(unit);
    }
}
