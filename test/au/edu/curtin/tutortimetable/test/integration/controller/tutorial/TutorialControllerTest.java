package au.edu.curtin.tutortimetable.test.integration.controller.tutorial;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTest;
import au.edu.curtin.tutortimetable.test.integration.controller.DomainObjectControllerTester;

public class TutorialControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private Tutorial testTutorial;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectControllerTester<Tutorial> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(tutorialService);
        testTutorial = DomainObjectFactory.createTutorial();
        tester.setTestObject(testTutorial);
        tester.setURL("/tutorial");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() throws Exception {

        tester.testGET();

    }

    @Override
    @Test
    public void testGETNonExistant() throws Exception {

        tester.testGETNonExistant();

    }

    @Override
    @Test
    public void testGETAll() throws Exception {

        tester.testGETAll();

    }

    @Override
    @Test
    public void testPOST() throws Exception {

        tester.testPOST();

    }

    @Override
    @Test
    public void testPOSTNotCreate() throws Exception {

        tester.testPOSTNotCreate();

    }

    @Override
    @Test
    public void testPUT() throws Exception {

        testTutorial.setId(0);
        testTutorial = tutorialService.create(testTutorial);

        tester.testPUT();

    }

    @Override
    @Test
    public void testPUTNotUpdate() throws Exception {

        tester.testPUTNotUpdate();

    }

    @Override
    @Test
    public void testPUTNonExistant() throws Exception {

        tester.testPUTNonExistant();

    }

    @Override
    @Test
    public void testDELETE() throws Exception {

        testTutorial.setId(0);
        tutorialService.create(testTutorial);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        }
    }

    @Override
    @Test
    public void testDELETENonExistant() throws Exception {

        tester.testDELETENonExistant();

    }

    @Override
    @Test
    public void testCount() throws Exception {

        tester.testCount();
    }

    @Test
    public void testLoadTutorialsForTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        tutorialService.assignTutorialToTutor(tutorial.getId(), tutor.getId());

        MvcResult result = mockMvc.perform(get("/tutorial/tutor/" + tutor.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(1)))
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Tutorial[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), Tutorial[].class);

        assertEquals("The tutorials should include their loaded groups.", tutorial.getTutorialGroup().getId(),
                returnedObject[0].getTutorialGroup().getId());
        assertEquals("The tutorials should include their loaded units.", tutorial.getTutorialGroup().getUnit().getId(),
                returnedObject[0].getTutorialGroup().getUnit().getId());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testAssignTutorialToTutor() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/" + tutor.getId() + "/assign")).andExpect(
                status().isOk());

        assertEquals("There should now be 1 assigned tutorial.", 1, tutorialService
                .loadTutorialsForTutor(tutor.getId()).size());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }

    @Test
    public void testClearAssignment() throws Exception {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorialService.create(tutorial);

        mockMvc.perform(put("/tutorial/" + tutorial.getId() + "/tutor/unassign"))
                .andExpect(status().isOk());

        tutorial = tutorialService.find(tutorial.getId());

        assertEquals("The tutorial should not have a tutor", null, tutorial.getTutor());

        // Cleanup
        tutorialService.delete(tutorial);
        userService.delete(tutor);
    }
}
