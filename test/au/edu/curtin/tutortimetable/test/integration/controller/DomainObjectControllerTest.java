package au.edu.curtin.tutortimetable.test.integration.controller;

public interface DomainObjectControllerTest {

    public void testGET() throws Exception;

    public void testGETNonExistant() throws Exception;

    public void testGETAll() throws Exception;

    public void testPOST() throws Exception;

    public void testPOSTNotCreate() throws Exception;

    public void testPUT() throws Exception;

    public void testPUTNotUpdate() throws Exception;

    public void testPUTNonExistant() throws Exception;

    public void testDELETE() throws Exception;

    public void testDELETENonExistant() throws Exception;

    public void testCount() throws Exception;

}
