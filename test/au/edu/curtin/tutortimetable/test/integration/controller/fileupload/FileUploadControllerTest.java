package au.edu.curtin.tutortimetable.test.integration.controller.fileupload;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class FileUploadControllerTest extends SpringTest {

    private MockMvc mockMvc;

    private MockMultipartFile uploadedFile;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UnitService unitService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private TutorialService tutorialService;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testUploadValidUnitCsv() throws Exception {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        long countBefore = unitService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile)).andExpect(
                status().isOk());
        long countAfter = unitService.count();

        assertEquals("There should be 4 additional units.", 4, countAfter - countBefore);

        // Cleanup
        List<Unit> units = unitService.findAll();
        for (int ii = 0; ii < 4; ii++) {
            Unit lastUnit = units.get(units.size() - 1); // Latest added unit
            units.remove(lastUnit);
            unitService.delete(lastUnit);
        }
    }

    @Test
    public void testUploadNullUnitCsv() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/unitcsv")).andExpect(status().isBadRequest());
    }

    @Test
    public void testUploadEmptyUnitCsv() throws Exception {

        InputStream stream = new ByteArrayInputStream(new byte[0]);
        uploadedFile = new MockMultipartFile("file", stream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile)).andExpect(
                status().isBadRequest());
    }

    @Test
    public void testUploadCorruptUnitCsv() throws Exception {

        InputStream stream = new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 });
        uploadedFile = new MockMultipartFile("file", stream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/unitcsv").file(uploadedFile)).andExpect(
                status().isBadRequest());
    }

    @Test
    public void testUploadValidTutorialCsv() throws Exception {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("UnitA");
        unitService.create(unit);

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadedFile = new MockMultipartFile("file", inputStream);

        long countBefore = tutorialService.count();
        long countGroupsBefore = tutorialGroupService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)).andExpect(
                status().isOk());
        long countAfter = tutorialService.count();
        long countGroupsAfter = tutorialGroupService.count();

        assertEquals("There should be 4 additional tutorials.", 4, countAfter - countBefore);
        assertEquals("There should be 1 additional tutorial group.", 1, countGroupsAfter - countGroupsBefore);

        // Cleanup
        List<TutorialGroup> tutorialGroups = tutorialGroupService.findAll();
        // Clean up the newly created tutorial group (Which deletes its orphaned tutorials)
        tutorialGroupService.delete(tutorialGroups.get(tutorialGroups.size() - 1));
    }

    @Test
    public void testUploadNullTutorialCsv() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv")).andExpect(status().isBadRequest());
    }

    @Test
    public void testUploadEmptyTutorialCsv() throws Exception {

        InputStream stream = new ByteArrayInputStream(new byte[0]);
        uploadedFile = new MockMultipartFile("file", stream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)).andExpect(
                status().isBadRequest());
    }

    @Test
    public void testUploadCorruptTutorialCsv() throws Exception {

        InputStream stream = new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 });
        uploadedFile = new MockMultipartFile("file", stream);
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/tutorialcsv").file(uploadedFile)).andExpect(
                status().isBadRequest());
    }

    @Test
    public void testDownloadUnitCsv() throws Exception {

        long unitCount = unitService.count();
        long csvCount = 0;

        MvcResult result = mockMvc.perform(get("/download/unitcsv")).andExpect(
                status().isOk()).andReturn();

        byte[] unitCsv = result.getResponse().getContentAsByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(unitCsv);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The unitCount and csvCount should match.", unitCount, csvCount);
    }

    @Test
    public void testDownloadTutorialCsv() throws Exception {

        long tutorialCount = tutorialService.count();
        long csvCount = 0;

        MvcResult result = mockMvc.perform(get("/download/tutorialcsv")).andExpect(
                status().isOk()).andReturn();

        byte[] unitCsv = result.getResponse().getContentAsByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(unitCsv);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The tutorialCount and csvCount should match.", tutorialCount, csvCount);
    }

}
