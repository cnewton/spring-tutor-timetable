package au.edu.curtin.tutortimetable.test.csv.parser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class CsvImportParserTest extends SpringTest {

    private File testFile;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void test4Standard() throws FileNotFoundException, IOException, InvalidCSVException, CsvParsingException {

        testFile = new ClassPathResource("csv/test4Standard.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream));
        int rowCount = 0;

        while (!parser.isEOF()) {
            Map<String, String> row = parser.parseRow();
            if (row != null) {
                rowCount++;
                assertEquals("The map size was not 3.", 3, row.size());
            }
        }

        assertEquals("The row count was not 4.", 4, rowCount);

        parser.close();

    }

    @Test(expected = InvalidCSVException.class)
    public void testEmptyHeader() throws FileNotFoundException, IOException, InvalidCSVException {

        testFile = new ClassPathResource("csv/testEmptyHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream))) {
        }
    }

    @Test(expected = InvalidCSVException.class)
    public void testInvalidHeader() throws FileNotFoundException, IOException, InvalidCSVException {

        testFile = new ClassPathResource("csv/testInvalidHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream))) {
        }
    }

    @Test
    public void testMissingHeader() throws FileNotFoundException, IOException, InvalidCSVException {

        testFile = new ClassPathResource("csv/testMissingHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream));
        int rowCount = 0;
        int failCount = 0;

        while (!parser.isEOF()) {

            Map<String, String> row = null;

            try {
                row = parser.parseRow();
            } catch (CsvParsingException e) {
                failCount++;
                row = null;
            }

            if (row != null) {
                rowCount++;
            }
        }

        assertEquals("The row count was not 0.", 0, rowCount);
        assertEquals("The fail count was not 4.", 4, failCount);

        parser.close();
    }

    @Test
    public void testMissingField() throws FileNotFoundException, IOException, InvalidCSVException {

        testFile = new ClassPathResource("csv/testMissingField.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream));
        int rowCount = 0;
        int failCount = 0;

        while (!parser.isEOF()) {

            Map<String, String> row = null;

            try {
                row = parser.parseRow();
            } catch (CsvParsingException e) {
                failCount++;
                row = null;
            }

            if (row != null) {
                rowCount++;
            }
        }

        assertEquals("The row count was not 3.", 3, rowCount);
        assertEquals("The fail count was not 1.", 1, failCount);

        parser.close();
    }

    @Test
    public void test1StandardOutput() throws FileNotFoundException, IOException, InvalidCSVException,
            CsvParsingException {

        testFile = new ClassPathResource("csv/test1Standard.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        CsvImportParser parser = new CsvImportParser(new InputStreamReader(stream));

        Map<String, String> row = parser.parseRow();

        assertEquals("The map size was not 3.", 3, row.size());
        assertEquals("Field1 value was incorrect.", "A", row.get("FIELD1"));
        assertEquals("Field2 value was incorrect.", "B", row.get("FIELD2"));
        assertEquals("Field3 value was incorrect.", "C", row.get("FIELD3"));

        parser.close();

    }

}
