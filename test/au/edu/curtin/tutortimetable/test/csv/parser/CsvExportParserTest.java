package au.edu.curtin.tutortimetable.test.csv.parser;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialCsvStaticField;
import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialMapper;
import au.edu.curtin.tutortimetable.csv.mapper.unit.UnitCsvStaticField;
import au.edu.curtin.tutortimetable.csv.parser.CsvExportParser;
import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.exception.TutorialMappingException;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class CsvExportParserTest extends SpringTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testExport2Units() throws InvalidCSVException, CsvParsingException {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(stream);

        try (CsvExportParser parser = new CsvExportParser(writer, UnitCsvStaticField.stringValues())) {
            Map<String, String> row1 = new HashMap<>();
            row1.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "UNIT1");
            row1.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "Unit 1");

            Map<String, String> row2 = new HashMap<>();
            row2.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "UNIT2");
            row2.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "Unit 2");

            parser.writeRow(row1);
            parser.writeRow(row2);
        }

        byte[] result = stream.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(result);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            Map<String, String> row1 = parser.parseRow();
            assertEquals("The unit code should match.", "UNIT1",
                    row1.get(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()));

            Map<String, String> row2 = parser.parseRow();
            assertEquals("The unit code should match.", "UNIT2",
                    row2.get(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()));
        }
    }

    @Test
    public void testExport2Tutorials() throws InvalidCSVException, CsvParsingException, TutorialMappingException {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(stream);

        Tutorial tutorial1 = DomainObjectFactory.createTutorial();
        tutorial1.setRoom("123.123");
        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setRoom("321.321");

        try (CsvExportParser parser = new CsvExportParser(writer, TutorialCsvStaticField.stringValues())) {
            Map<String, String> row1 = TutorialMapper.map(tutorial1);
            Map<String, String> row2 = TutorialMapper.map(tutorial2);
            parser.writeRow(row1);
            parser.writeRow(row2);
        }

        byte[] result = stream.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(result);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            Map<String, String> row1 = parser.parseRow();
            assertEquals("The unit code should match.", tutorial1.getTutorialGroup().getUnit().getUnitCode(),
                    row1.get(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName()));

            assertEquals("The tutor name should match.", tutorial1.getTutor().getName(),
                    row1.get(TutorialCsvStaticField.TUTOR_NAME.getCsvHeaderName()));

            Map<String, String> row2 = parser.parseRow();
            assertEquals("The unit code should match.", tutorial2.getTutorialGroup().getUnit().getUnitCode(),
                    row2.get(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName()));

            assertEquals("The tutor name should match.", tutorial2.getTutor().getName(),
                    row2.get(TutorialCsvStaticField.TUTOR_NAME.getCsvHeaderName()));
        }
    }
}
