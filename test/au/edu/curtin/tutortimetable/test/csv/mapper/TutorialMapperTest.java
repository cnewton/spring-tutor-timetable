package au.edu.curtin.tutortimetable.test.csv.mapper;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialCsvStaticField;
import au.edu.curtin.tutortimetable.csv.mapper.tutorial.TutorialMapper;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialType;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.exception.ObjectMappingException;
import au.edu.curtin.tutortimetable.exception.TutorialMappingException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class TutorialMapperTest extends SpringTest {

    private TutorialMapper tutorialMapper;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private UnitService unitService;

    @Before
    public void setUp() throws Exception {

        tutorialMapper = new TutorialMapper(unitService.loadAllWithGroups());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testMapToTutorial() throws ObjectMappingException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("A");
        unitService.create(unit);

        // Include newly created unit
        tutorialMapper = new TutorialMapper(unitService.loadAllWithGroups());

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "8-Mar-16");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "80000");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), "Group");

        Tutorial tutorial = tutorialMapper.map(map);

        assertEquals("Unit was not correctly attached", unit, tutorial.getTutorialGroup().getUnit());
        assertEquals("Room was not correctly formed.", "B.C", tutorial.getRoom());
        assertEquals("Start time was not correctly constructed.", LocalDateTime.of(2016, 03, 8, 8, 0),
                tutorial.getStartTime());
        assertEquals("End time was not correctly constructed.", LocalDateTime.of(2016, 03, 8, 10, 0),
                tutorial.getEndTime());
        assertEquals("The class type was not correctly attached.", TutorialType.COMPUTER_LABORATORY, tutorial
                .getTutorialGroup().getTutorialType());
    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testMapToTutorialMissingGroupName() throws ObjectMappingException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("A");
        unitService.create(unit);

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "16-Mar-16");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "80000");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());

        tutorialMapper.map(map);
    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testMapToTutorialMissingUnit() throws ObjectMappingException {

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "16-Mar-16");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "80000");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), "Group");

        tutorialMapper.map(map);
    }

    @Test
    @Transactional
    public void testMapToTutorialExistingTutorialGroup() throws ObjectMappingException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("A");
        unitService.create(unit);

        Tutorial tute = DomainObjectFactory.createTutorial();
        tute.setId(0);
        tute.getTutorialGroup().setId(0);
        tute.getTutorialGroup().setGroupName("ExistingName");
        tute.getTutorialGroup().setUnit(unit);
        tutorialGroupService.create(tute.getTutorialGroup());
        tutorialService.create(tute);

        unit.getTutorialGroups().add(tute.getTutorialGroup());

        // Include newly created unit and tutorialgroup.
        tutorialMapper = new TutorialMapper(unitService.loadAllWithGroups());

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "16-Mar-16");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "80000");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), "ExistingName");

        Tutorial tutorial = tutorialMapper.map(map);

        assertEquals("Unit was not correctly attached", unit, tutorial.getTutorialGroup().getUnit());
        assertEquals("Room was not correctly formed.", "B.C", tutorial.getRoom());
        assertEquals("Start time was not correctly constructed.", LocalDateTime.of(2016, 03, 16, 8, 0),
                tutorial.getStartTime());
        assertEquals("End time was not correctly constructed.", LocalDateTime.of(2016, 03, 16, 10, 0),
                tutorial.getEndTime());
        assertEquals("The class type was not correctly attached.", tute.getTutorialGroup().getTutorialType(), tutorial
                .getTutorialGroup().getTutorialType());
        assertEquals("Tutorial group was not correctly attached.", tute.getTutorialGroup(),
                tutorial.getTutorialGroup());
    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testMapToTutorialBadDate() throws ObjectMappingException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("A");
        unitService.create(unit);

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "Invalid");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "80000");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), "Group");

        tutorialMapper.map(map);

    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testMapToTutorialBadTime() throws ObjectMappingException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("A");
        unitService.create(unit);

        Map<String, String> map = new HashMap<>();
        map.put(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(TutorialCsvStaticField.BUILDING.getCsvHeaderName(), "B");
        map.put(TutorialCsvStaticField.ROOM.getCsvHeaderName(), "C");
        map.put(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName(), "16-Mar-16");
        map.put(TutorialCsvStaticField.START_TIME.getCsvHeaderName(), "1234");
        map.put(TutorialCsvStaticField.END_TIME.getCsvHeaderName(), "100000");
        map.put(TutorialCsvStaticField.CLASS_TYPE.getCsvHeaderName(), TutorialType.COMPUTER_LABORATORY.getType());
        map.put(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName(), "Group");

        tutorialMapper.map(map);

    }

    @Test
    @Transactional
    public void testTutorialToMap() throws ObjectMappingException {

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setStartTime(LocalDateTime.of(2016, 03, 16, 8, 0));

        Map<String, String> map = TutorialMapper.map(tutorial);

        assertEquals("The map did not contain the correct group name.", tutorial.getTutorialGroup().getGroupName(),
                map.get(TutorialCsvStaticField.CLASS_GROUP.getCsvHeaderName()));
        assertEquals("The map did not contain the correct unit code.", tutorial.getTutorialGroup().getUnit()
                .getUnitCode(), map.get(TutorialCsvStaticField.UNIT_CODE.getCsvHeaderName()));
        assertEquals("The map did not contain the correct session date.", "16-Mar-16",
                map.get(TutorialCsvStaticField.SESSION_DATE.getCsvHeaderName()));
        assertEquals("The map did not contain the correct start time.", "080000",
                map.get(TutorialCsvStaticField.START_TIME.getCsvHeaderName()));
    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testTutorialToMapNoGroup() throws ObjectMappingException {

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setTutorialGroup(null);
        TutorialMapper.map(tutorial);
    }

    @Test(expected = TutorialMappingException.class)
    @Transactional
    public void testTutorialToMapNoUnit() throws ObjectMappingException {

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.getTutorialGroup().setUnit(null);
        TutorialMapper.map(tutorial);
    }

}
