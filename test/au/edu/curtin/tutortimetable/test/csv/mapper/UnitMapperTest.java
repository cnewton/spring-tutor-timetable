package au.edu.curtin.tutortimetable.test.csv.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.csv.mapper.unit.UnitCsvStaticField;
import au.edu.curtin.tutortimetable.csv.mapper.unit.UnitMapper;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.ObjectMappingException;
import au.edu.curtin.tutortimetable.exception.UnitMappingException;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class UnitMapperTest extends SpringTest {

    @Autowired
    private UserService userService;

    private UnitMapper mapper;

    @Before
    public void setUp() throws Exception {

        mapper = new UnitMapper(userService.findAllCoordinators());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testMapToUnitById() throws ObjectMappingException {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setStaffId("12345");
        userService.create(coordinator);

        // Include newly created coordinator
        mapper = new UnitMapper(userService.findAllCoordinators());

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "B");
        map.put(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName(), "12345");

        Unit unit = mapper.map(map);

        assertEquals("The unit code was incorrect.", "A", unit.getUnitCode());
        assertEquals("The unit name was incorrect.", "B", unit.getUnitName());
        assertEquals("The unit coordinator staff id was incorrect.", "12345", unit.getUnitCoordinator().getStaffId());
    }

    @Test
    @Transactional
    public void testMapToUnitByName() throws ObjectMappingException {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setName("Test");
        userService.create(coordinator);

        // Include newly created coordinator
        mapper = new UnitMapper(userService.findAllCoordinators());

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "B");
        map.put(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName(), "Test");

        Unit unit = mapper.map(map);

        assertEquals("The unit code was incorrect.", "A", unit.getUnitCode());
        assertEquals("The unit name was incorrect.", "B", unit.getUnitName());
        assertEquals("The unit coordinator staff id was incorrect.", "Test", unit.getUnitCoordinator().getName());
    }

    @Test
    @Transactional
    public void testMapToUnitNullCoordinator() throws ObjectMappingException {

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "B");

        Unit unit = mapper.map(map);

        assertEquals("The unit code was incorrect.", "A", unit.getUnitCode());
        assertEquals("The unit name was incorrect.", "B", unit.getUnitName());
        assertNull("The unit coordinator was not null.", unit.getUnitCoordinator());
    }

    @Test
    @Transactional
    public void testMapToUnitEmptyCoordinator() throws ObjectMappingException {

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "B");
        map.put(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName(), "");

        Unit unit = mapper.map(map);

        assertEquals("The unit code was incorrect.", "A", unit.getUnitCode());
        assertEquals("The unit name was incorrect.", "B", unit.getUnitName());
        assertNull("The unit coordinator was not null.", unit.getUnitCoordinator());
    }

    @Test(expected = UnitMappingException.class)
    @Transactional
    public void testMapToUnitMissingNameField() throws ObjectMappingException {

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName(), "A");
        mapper.map(map);
    }

    @Test(expected = UnitMappingException.class)
    @Transactional
    public void testMapToUnitMissingCodeField() throws ObjectMappingException {

        Map<String, String> map = new HashMap<>();
        map.put(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName(), "A");
        mapper.map(map);
    }

    @Test
    @Transactional
    public void testUnitToMap() throws ObjectMappingException {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setStaffId("12345");
        coordinator.setName("Test");
        userService.create(coordinator);

        Unit unit = new Unit();
        unit.setUnitCode("A");
        unit.setUnitName("B");
        unit.setUnitCoordinator(coordinator);

        Map<String, String> map = UnitMapper.map(unit);

        assertEquals("The unit code was incorrect.", "A", map.get(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()));
        assertEquals("The unit name was incorrect.", "B", map.get(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName()));
        assertEquals("The unit coordinator staff id was incorrect.", "12345",
                map.get(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName()));
        assertEquals("The unit coordinator name was incorrect.", "Test",
                map.get(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName()));
    }

    @Test
    @Transactional
    public void testUnitToMapNullCoordinator() throws ObjectMappingException {

        Unit unit = new Unit();
        unit.setUnitCode("A");
        unit.setUnitName("B");
        unit.setUnitCoordinator(null);

        Map<String, String> map = UnitMapper.map(unit);

        assertEquals("The unit code was incorrect.", "A", map.get(UnitCsvStaticField.UNIT_CODE.getCsvHeaderName()));
        assertEquals("The unit name was incorrect.", "B", map.get(UnitCsvStaticField.UNIT_NAME.getCsvHeaderName()));
        assertEquals("The unit coordinator id was not empty.", "",
                map.get(UnitCsvStaticField.UNIT_COORDINATOR_ID.getCsvHeaderName()));
        assertEquals("The unit coordinator name was not empty.", "",
                map.get(UnitCsvStaticField.UNIT_COORDINATOR_NAME.getCsvHeaderName()));
    }

}
