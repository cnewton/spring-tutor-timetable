package au.edu.curtin.tutortimetable.test.dao.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.user.UserDAO;
import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class UserDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<User> tester;

    @Autowired
    private UserDAO userDAO;

    private User testUser;

    @Before
    public void setUp() throws Exception {

        testUser = DomainObjectFactory.createUser();
        tester.setDAO(userDAO);
        tester.setTestObject(testUser);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUser.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUser.setName("updated");

        tester.testUpdate();

        assertEquals("Name was not updated correctly", "updated", userDAO.find(testUser.getId()).getName());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUser.setId(0);
        userDAO.createOrUpdate(testUser); // Create the object so that it may
                                          // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userDAO.createOrUpdate(tutor);

        tutor = userDAO.findTutor(tutor.getId());

        assertNotNull("Tutor was not found.", tutor);
    }

    @Test
    @Transactional
    public void testFindNonExistantTutor() {

        Tutor tutor = userDAO.findTutor(10000);

        assertNull(tutor);
    }

    @Test
    @Transactional
    public void testFindCoordinatorByName() {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setName("TestFindName");
        userDAO.createOrUpdate(coordinator);

        coordinator = userDAO.findCoordinatorByName(coordinator.getName());

        assertNotNull("Coordinator was not found.", coordinator);
    }

    @Test
    @Transactional
    public void testFindNonExistantCoordinatorByName() {

        assertNull(userDAO.findCoordinatorByName("Not to be found"));
    }

    @Test
    @Transactional
    public void testFindCoordinatorByNameNotUnique() {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setUsername("NewUser");
        coordinator.setName("TestFindName");
        userDAO.createOrUpdate(coordinator);

        UnitCoordinator coordinator2 = DomainObjectFactory.createUnitCoordinator();
        coordinator2.setId(0);
        coordinator2.setName("TestFindName");
        userDAO.createOrUpdate(coordinator2);

        assertNull(userDAO.findCoordinatorByName(coordinator.getName()));
    }

    @Test
    @Transactional
    public void testFindCoordinatorById() {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setStaffId("12345");
        userDAO.createOrUpdate(coordinator);

        coordinator = userDAO.findCoordinatorByStaffId(coordinator.getStaffId());

        assertNotNull("Coordinator was not found.", coordinator);
    }

    @Test
    @Transactional
    public void testFindNonExistantCoordinatorById() {

        assertNull(userDAO.findCoordinatorByStaffId("Not to be found"));
    }

    @Test
    @Transactional
    public void testFindCoordinatorByIdNotUnique() {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setUsername("NewUser");
        coordinator.setStaffId("1234");
        userDAO.createOrUpdate(coordinator);

        UnitCoordinator coordinator2 = DomainObjectFactory.createUnitCoordinator();
        coordinator2.setId(0);
        coordinator2.setStaffId("1234");
        userDAO.createOrUpdate(coordinator2);

        assertNull(userDAO.findCoordinatorByStaffId(coordinator.getStaffId()));
    }

    @Test
    @Transactional
    public void testFindAllTutors() {

        long countBefore = userDAO.findAllTutors().size();

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("New 1");
        userDAO.createOrUpdate(tutor);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setUsername("New 2");
        userDAO.createOrUpdate(tutor2);

        // Add a unit coordinator to ensure that tutors dont include coordinators (As UC's are an extension of Tutors)
        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setUsername("New 3");
        userDAO.createOrUpdate(coordinator);

        long countAfter = userDAO.findAllTutors().size();

        assertEquals("There should be 2 new tutors.", 2, countAfter - countBefore);
    }
}
