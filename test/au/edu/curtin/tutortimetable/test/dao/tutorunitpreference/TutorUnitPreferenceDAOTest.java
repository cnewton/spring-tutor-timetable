package au.edu.curtin.tutortimetable.test.dao.tutorunitpreference;

import static org.junit.Assert.assertEquals;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorunitpreference.TutorUnitPreferenceDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class TutorUnitPreferenceDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<TutorUnitPreference> tester;

    @Autowired
    private TutorUnitPreferenceDAO tutorUnitPreferenceDAO;

    @Autowired
    private UnitService unitService;

    private TutorUnitPreference testTutorUnitPreference;

    @Before
    public void setUp() throws Exception {

        testTutorUnitPreference = DomainObjectFactory.createTutorUnitPreference();
        tester.setDAO(tutorUnitPreferenceDAO);
        tester.setTestObject(testTutorUnitPreference);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorUnitPreference.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorUnitPreference.setPriority(5);

        tester.testUpdate();

        assertEquals("Day was not updated correctly", 5, tutorUnitPreferenceDAO.find(testTutorUnitPreference.getId())
                .getPriority());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorUnitPreference.setId(0);
        tutorUnitPreferenceDAO.createOrUpdate(testTutorUnitPreference); // Create the object so that it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testLoadForUnit() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceDAO.createOrUpdate(pref);

        assertEquals("There should be one preference for the given unit.", 1, tutorUnitPreferenceDAO.loadForUnit(unit)
                .size());
    }
}
