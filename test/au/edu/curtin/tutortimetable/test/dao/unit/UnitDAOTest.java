package au.edu.curtin.tutortimetable.test.dao.unit;

import static org.junit.Assert.assertEquals;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.unit.UnitDAO;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class UnitDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<Unit> tester;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private UserService userService;

    private Unit testUnit;

    @Before
    public void setUp() throws Exception {

        testUnit = DomainObjectFactory.createUnit();
        tester.setDAO(unitDAO);
        tester.setTestObject(testUnit);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUnit.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUnit.setUnitName("updated");

        tester.testUpdate();

        assertEquals("Name was not updated correctly", "updated", unitDAO.find(testUnit.getId()).getUnitName());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUnit.setId(0);
        unitDAO.createOrUpdate(testUnit); // Create the object so that it may
                                          // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindByUnitCoordinator() {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitDAO.createOrUpdate(unit);

        assertEquals("There should be 1 unit for the given coordinator.", 1, unitDAO.findAllForCoordinator(coord)
                .size());
    }

    @Test
    @Transactional
    public void testLoadAll() {

        long countBefore = unitDAO.count();

        Unit unit1 = DomainObjectFactory.createUnit();
        unit1.setId(0);
        unitDAO.createOrUpdate(unit1);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitDAO.createOrUpdate(unit2);

        assertEquals("There should be 2 more units.", countBefore + 2, unitDAO.loadAllWithCoordinator().size());
    }
}
