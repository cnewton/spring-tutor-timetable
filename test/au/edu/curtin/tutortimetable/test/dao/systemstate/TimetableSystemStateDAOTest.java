package au.edu.curtin.tutortimetable.test.dao.systemstate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.systemstate.TimetableSystemStateDAO;
import au.edu.curtin.tutortimetable.domainobject.systemstate.TimetableSystemState;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class TimetableSystemStateDAOTest extends SpringTest {

    @Autowired
    private TimetableSystemStateDAO systemStateDAO;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testGet() {

        TimetableSystemState obj = systemStateDAO.get();
        assertNotNull("object not found", obj);
        assertEquals("The object id was not correct", 1, obj.getId());
    }

    /**
     * Note this test is NOT Transactional.
     */
    @Test
    public void testUpdate() {

        TimetableSystemState obj = systemStateDAO.get();
        assertTrue(!obj.isTutorsLocked());

        obj.setTutorsLocked(true);
        systemStateDAO.update(obj);

        obj = systemStateDAO.get();
        assertTrue(obj.isTutorsLocked());

        // Cleanup
        obj.setTutorsLocked(false);
        systemStateDAO.update(obj);
    }

}
