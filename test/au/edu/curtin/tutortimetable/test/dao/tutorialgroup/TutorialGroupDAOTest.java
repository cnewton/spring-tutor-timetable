package au.edu.curtin.tutortimetable.test.dao.tutorialgroup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorialgroup.TutorialGroupDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialType;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class TutorialGroupDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<TutorialGroup> tester;

    @Autowired
    private TutorialGroupDAO tutorialGroupDAO;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private UnitService unitService;

    private TutorialGroup testTutorialGroup;

    @Before
    public void setUp() throws Exception {

        testTutorialGroup = DomainObjectFactory.createTutorialGroup();
        tester.setDAO(tutorialGroupDAO);
        tester.setTestObject(testTutorialGroup);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorialGroup.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorialGroup.setTutorialType(TutorialType.COMPUTER_LABORATORY);

        tester.testUpdate();

        assertEquals("Room was not updated correctly", TutorialType.COMPUTER_LABORATORY,
                tutorialGroupDAO.find(testTutorialGroup.getId())
                        .getTutorialType());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorialGroup.setId(0);
        tutorialGroupDAO.createOrUpdate(testTutorialGroup); // Create the object so that it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindTutorialGroupGroupByName() {

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setGroupName("test1Name");
        group.setId(0);

        tutorialGroupService.create(group);

        assertNotNull(tutorialGroupDAO.findByName("test1Name"));
    }

    @Test
    @Transactional
    public void testLoadTutorialGroupsForUnit() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupDAO.createOrUpdate(group);

        assertEquals("There should be 1 group.", 1, tutorialGroupDAO.loadTutorialGroupsForUnit(unit).size());

    }
}
