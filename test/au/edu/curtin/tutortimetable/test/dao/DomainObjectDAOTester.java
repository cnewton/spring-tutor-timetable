package au.edu.curtin.tutortimetable.test.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.DomainObjectDAO;
import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;

@Component
public class DomainObjectDAOTester<T extends IdBasedDomainObject> {

    private DomainObjectDAO<T> dao;

    private T obj;

    public void setDAO(DomainObjectDAO<T> dao) {

        this.dao = dao;
    }

    public void setTestObject(T obj) {

        this.obj = obj;
    }

    @Transactional
    public void testFind() {

        T obj = dao.find(1L);
        assertNotNull("object not found", obj);
        assertEquals("The object id was not correct", 1, obj.getId());
    }

    @Transactional
    public void testFindNonExistant() {

        T obj = dao.find(-1);
        assertNull(obj);
    }

    @Transactional
    public void testFindAll() {

        long count = dao.count();
        List<T> list = dao.findAll();
        assertEquals("Incorrect number of objects found", count, list.size());
    }

    @Transactional
    public void testCreate() {

        obj.setId(0);

        long countBefore = dao.count();
        dao.createOrUpdate(obj);
        long countAfter = dao.count();

        assertNotNull(dao.find(obj.getId()));
        assertEquals("Incorrect count after object was created", countBefore + 1, countAfter);
    }

    @Transactional
    public void testUpdate() {

        long countBefore = dao.count();
        dao.createOrUpdate(obj);
        long countAfter = dao.count();

        assertNotNull(dao.find(obj.getId()));
        assertEquals("Incorrect count after object was updated", countBefore, countAfter);
    }

    @Transactional
    public void testUpdateNonExistant() {

        obj.setId(1000);

        dao.createOrUpdate(obj);

        dao.count(); // Force transaction flush

    }

    @Transactional
    public void testDeleteByObject() {

        long countBefore = dao.count();
        dao.delete(obj);
        long countAfter = dao.count();

        assertEquals("object was not deleted using ID", countBefore - 1, countAfter);
        assertNull(dao.find(obj.getId()));
    }

    @Transactional
    public void testDeleteNonExistantByObject() {

        obj.setId(-1);
        dao.delete(obj);

        dao.count(); // Force transaction flush

    }

    @Transactional
    public void testDeleteById() {

        long countBefore = dao.count();
        dao.delete(3);
        long countAfter = dao.count();

        assertEquals("object was not deleted using ID", countBefore - 1, countAfter);

    }

    @Transactional
    public void testDeleteNonExistantById() {

        dao.delete(-1);

        fail("Test did not throw exception when expected");
    }

    @Transactional
    public void testCount() {

        long countBefore = dao.count();
        dao.delete(3);
        long countAfter = dao.count();

        assertEquals("Incorrect buyer count", countBefore - 1, countAfter);
    }

}
