package au.edu.curtin.tutortimetable.test.dao.tutoravailability;

import static org.junit.Assert.assertEquals;

import java.time.DayOfWeek;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutoravailability.TutorAvailabilityDAO;
import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class TutorAvailabilityDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<TutorAvailability> tester;

    @Autowired
    private TutorAvailabilityDAO tutorAvailabilityDAO;

    private TutorAvailability testTutorAvailability;

    @Before
    public void setUp() throws Exception {

        testTutorAvailability = DomainObjectFactory.createTutorAvailability();
        tester.setDAO(tutorAvailabilityDAO);
        tester.setTestObject(testTutorAvailability);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorAvailability.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorAvailability.setDay(DayOfWeek.MONDAY);

        tester.testUpdate();

        assertEquals("Day was not updated correctly", DayOfWeek.MONDAY,
                tutorAvailabilityDAO.find(testTutorAvailability.getId()).getDay());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorAvailability.setId(0);
        tutorAvailabilityDAO.createOrUpdate(testTutorAvailability); // Create the object so that it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }
}
