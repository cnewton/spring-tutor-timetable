package au.edu.curtin.tutortimetable.test.dao.tutorial;

import static org.junit.Assert.assertEquals;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.dao.tutorial.TutorialDAO;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTest;
import au.edu.curtin.tutortimetable.test.dao.DomainObjectDAOTester;

public class TutorialDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<Tutorial> tester;

    @Autowired
    private TutorialDAO tutorialDAO;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private UserService userService;

    private Tutorial testTutorial;

    @Before
    public void setUp() throws Exception {

        testTutorial = DomainObjectFactory.createTutorial();
        tester.setDAO(tutorialDAO);
        tester.setTestObject(testTutorial);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorial.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorial.setRoom("updated");

        tester.testUpdate();

        assertEquals("Room was not updated correctly", "updated", tutorialDAO.find(testTutorial.getId()).getRoom());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorial.setId(0);
        tutorialDAO.createOrUpdate(testTutorial); // Create the object so that it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testLoadForTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialDAO.createOrUpdate(tutorial);

        tutorialService.assignTutorialToTutor(tutorial.getId(), tutor.getId());

        assertEquals("One tutorial should be found.", 1, tutorialDAO.loadTutorialsForTutor(tutor).size());
    }
}
