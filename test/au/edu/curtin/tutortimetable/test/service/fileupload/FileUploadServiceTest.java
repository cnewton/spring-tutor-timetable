package au.edu.curtin.tutortimetable.test.service.fileupload;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.FileUploadException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.service.fileupload.FileUploadService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class FileUploadServiceTest extends SpringTest {

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private TutorialService tutorialService;

    private MockMultipartFile uploadedFile;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testUploadValidUnitCsv() throws IOException {

        File testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", stream);

        long countBefore = unitService.count();
        fileUploadService.parseUnitCsv(uploadedFile);
        long countAfter = unitService.count();

        assertEquals("4 New units should have been added.", 4, countAfter - countBefore);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadNullUnitCsv() throws IOException {

        fileUploadService.parseUnitCsv(null);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadEmptyUnitCsv() throws IOException {

        InputStream stream = new ByteArrayInputStream(new byte[0]);
        uploadedFile = new MockMultipartFile("file", stream);
        fileUploadService.parseUnitCsv(uploadedFile);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadCorruptUnitCsv() throws IOException {

        InputStream stream = new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 });
        uploadedFile = new MockMultipartFile("file", stream);
        fileUploadService.parseUnitCsv(uploadedFile);
    }

    @Test
    @Transactional
    public void testUploadValidTutorialCsv() throws IOException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("UnitA");
        unitService.create(unit);

        File testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", stream);

        long countBefore = tutorialService.count();
        fileUploadService.parseTutorialCsv(uploadedFile);
        long countAfter = tutorialService.count();

        assertEquals("4 New tutorials should have been added.", 4, countAfter - countBefore);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadNullTutorialCsv() throws IOException {

        fileUploadService.parseTutorialCsv(null);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadEmptyTutorialCsv() throws IOException {

        InputStream stream = new ByteArrayInputStream(new byte[0]);
        uploadedFile = new MockMultipartFile("file", stream);
        fileUploadService.parseTutorialCsv(uploadedFile);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testUploadCorruptTutorialCsv() throws IOException {

        InputStream stream = new ByteArrayInputStream(new byte[] { 1, 2, 3, 4 });
        uploadedFile = new MockMultipartFile("file", stream);
        fileUploadService.parseTutorialCsv(uploadedFile);
    }

    @Test
    @Transactional
    public void testDownloadUnitCsv() throws InvalidCSVException, CsvParsingException {

        long unitCount = unitService.count();
        long csvCount = 0;

        byte[] unitCsv = fileUploadService.downloadUnitCsv();

        ByteArrayInputStream bais = new ByteArrayInputStream(unitCsv);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The unitCount and csvCount should match.", unitCount, csvCount);
    }

    @Test
    @Transactional
    public void testDownloadTutorialCsv() throws InvalidCSVException, CsvParsingException {

        long tutorialCount = tutorialService.count();
        long csvCount = 0;

        byte[] tutorialCsv = fileUploadService.downloadTutorialCsv();

        ByteArrayInputStream bais = new ByteArrayInputStream(tutorialCsv);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The tutorialCount and csvCount should match.", tutorialCount, csvCount);
    }
}
