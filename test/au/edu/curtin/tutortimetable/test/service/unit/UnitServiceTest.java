package au.edu.curtin.tutortimetable.test.service.unit;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class UnitServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectServiceTester<Unit> tester;

    private Unit testUnit;

    @Before
    public void setUp() throws Exception {

        testUnit = DomainObjectFactory.createUnit();
        tester.setService(unitService);
        tester.setTestObject(testUnit);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUnit.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUnit = unitService.find(1);
        testUnit.setUnitName("TestUpdate");
        tester.setTestObject(testUnit);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testUnit.getUnitName());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUnit.setId(0); // Create object to test deletion.
        unitService.create(testUnit);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindByUnitCoordinator() {

        UnitCoordinator coord = DomainObjectFactory.createUnitCoordinator();
        coord.setId(0);
        userService.create(coord);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCoordinator(coord);
        unitService.create(unit);

        assertEquals("There should be one unit for the given coordinator.", 1,
                unitService.getUnitsForCoordinator(coord.getId()).size());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindByNonExistantCoordinator() {

        unitService.getUnitsForCoordinator(-1);
    }

    @Test
    @Transactional
    public void testLoadAll() {

        long countBefore = unitService.count();

        Unit unit1 = DomainObjectFactory.createUnit();
        unit1.setId(0);
        unitService.create(unit1);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        assertEquals("There should be 2 more units.", countBefore + 2, unitService.loadAllWithCoordinator().size());
    }

    @Test
    @Transactional
    public void testAssignCoordinator() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        unitService.assignCoordinator(unit.getId(), coordinator.getId());

        assertEquals("The unit should be coordinated by the new coordinator.", coordinator,
                unitService.find(unit.getId()).getUnitCoordinator());
    }

    @Test
    @Transactional
    public void testFindByUnitCode() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("NewUnitCode");
        unitService.create(unit);

        assertEquals(1, unitService.findUnitByCode(unit.getUnitCode()).size());
    }
}
