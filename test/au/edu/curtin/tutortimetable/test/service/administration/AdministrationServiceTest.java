package au.edu.curtin.tutortimetable.test.service.administration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.service.administration.AdministrationService;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class AdministrationServiceTest extends SpringTest {

    @Autowired
    private AdministrationService administrationService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testGetSystemState() {

        assertNotNull(administrationService.getSystemState());
    }

    @Test
    @Transactional
    public void testSetSemesterStartDate() {

        LocalDate date = LocalDate.now();
        administrationService.setSemesterStartDate(date);

        assertEquals("The dates should match.", date, administrationService.getSystemState().getSemesterStart());
    }

    @Test
    @Transactional
    public void testSetSemesterEndDate() {

        LocalDate date = LocalDate.now();
        administrationService.setSemesterEndDate(date);

        assertEquals("The dates should match.", date, administrationService.getSystemState().getSemesterEnd());
    }

    @Test
    @Transactional
    public void testSetApplicationStartDate() {

        LocalDate date = LocalDate.now();
        administrationService.setApplicationStartDate(date);

        assertEquals("The dates should match.", date, administrationService.getSystemState().getApplicationStart());
    }

    @Test
    @Transactional
    public void testSetApplicationEndDate() {

        LocalDate date = LocalDate.now();
        administrationService.setApplicationEndDate(date);

        assertEquals("The dates should match.", date, administrationService.getSystemState().getApplicationEnd());
    }

    @Test
    @Transactional
    public void testSetLockoutDate() {

        LocalDate date = LocalDate.now();
        administrationService.setLockoutDate(date);

        assertEquals("The dates should match.", date, administrationService.getSystemState().getLockout());
    }

    @Test
    @Transactional
    public void testDeleteAllUnits() {

        Unit unit1 = DomainObjectFactory.createUnit();
        unit1.setId(0);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);

        unitService.create(unit1);
        unitService.create(unit2);

        administrationService.deleteAllUnits();

        assertEquals("There should be 0 units.", 0, unitService.count());
    }

    @Test
    @Transactional
    public void testDeleteAllTutorials() {

        Tutorial tutorial1 = DomainObjectFactory.createTutorial();
        tutorial1.setId(0);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);

        tutorialService.create(tutorial1);
        tutorialService.create(tutorial2);

        administrationService.deleteAllTutorials();

        assertEquals("There should be 0 units.", 0, tutorialService.count());
    }

    @Test
    @Transactional
    public void testDeleteAllTutorPreferences() {

        TutorUnitPreference pref1 = DomainObjectFactory.createTutorUnitPreference();
        pref1.setId(0);

        TutorUnitPreference pref2 = DomainObjectFactory.createTutorUnitPreference();
        pref2.setId(0);

        tutorUnitPreferenceService.create(pref1);
        tutorUnitPreferenceService.create(pref2);

        administrationService.deleteAllTutorPreferences();

        assertEquals("There should be 0 units.", 0, tutorUnitPreferenceService.count());

    }

    @Test
    @Transactional
    public void testDeleteAllTutorAvailabilities() {

        TutorAvailability avail1 = DomainObjectFactory.createTutorAvailability();
        avail1.setId(0);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);

        tutorAvailabilityService.create(avail1);
        tutorAvailabilityService.create(avail2);

        administrationService.deleteAllTutorAvailabilities();

        assertEquals("There should be 0 units.", 0, tutorAvailabilityService.count());

    }

    @Test
    @Transactional
    public void testClearAllApplications() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setCourse("Test Course");
        userService.create(tutor);

        administrationService.clearAllApplications();

        assertEquals("There should be 0 availabilities.", 0, tutorAvailabilityService.count());
        assertEquals("There should be 0 preferences.", 0, tutorUnitPreferenceService.count());
        assertEquals("The tutor should have its course set to null.", null, tutor.getCourse());
    }

    @Test
    @Transactional
    public void testToggleTutorAccounts() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        boolean isLocked1 = administrationService.getSystemState().isTutorsLocked();

        administrationService.toggleTutorAccounts();
        assertEquals("The tutor should have its enabled field toggled.", isLocked1, tutor.isEnabled());

        boolean isLocked2 = administrationService.getSystemState().isTutorsLocked();
        assertEquals("is locked should be inverted", !isLocked1, isLocked2);

        administrationService.toggleTutorAccounts();
        assertEquals("The tutor should have its enabled field toggled.", isLocked2, tutor.isEnabled());
    }

    @Test
    @Transactional
    public void testToggleCoordinatorAccounts() {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        userService.create(coordinator);

        boolean isLocked1 = administrationService.getSystemState().isCoordinatorsLocked();

        administrationService.toggleCoordinatorAccounts();
        assertEquals("The coordinator should have its enabled field toggled.", isLocked1, coordinator.isEnabled());

        boolean isLocked2 = administrationService.getSystemState().isCoordinatorsLocked();
        assertEquals("is locked should be inverted", !isLocked1, isLocked2);

        administrationService.toggleCoordinatorAccounts();
        assertEquals("The coordinator should have its enabled field toggled.", isLocked2, coordinator.isEnabled());
    }

    @Test
    @Transactional
    public void testClearAllAssignments() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(group);
        tutorial.setTutor(tutor);
        tutorialService.create(tutorial);

        assertNotNull(tutorial.getTutor());

        administrationService.clearAllTutorAssignments();

        assertNull(tutorial.getTutor());
    }

    @Test
    @Transactional
    public void testSetTutorMaxHours() {

        administrationService.setTutorMaxHours(6);

        assertEquals("The hours should match.", 6, administrationService.getSystemState().getTutorMaxHours());
    }

    @Test
    @Transactional
    public void testAssignmentBasic() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitName("Test Unit");
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        unit.getTutorialGroups().add(group);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(null);
        tutorial.setTutorialGroup(group);
        tutorial.setStartTime(LocalDateTime.parse("2016-06-02T11:00:00"));
        tutorial.setEndTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        group.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutor(null);
        tutorial2.setTutorialGroup(group);
        tutorial2.setStartTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        tutorial2.setEndTime(LocalDateTime.parse("2016-06-02T15:00:00"));
        group.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        Tutor toBeAssigned = DomainObjectFactory.createTutor();
        toBeAssigned.setId(0);
        toBeAssigned.setName("Prior 1");
        userService.create(toBeAssigned);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setName("Prior 2");
        tutor2.setUsername("new user");
        userService.create(tutor2);

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        avail.setTutor(toBeAssigned);
        avail.setDay(DayOfWeek.THURSDAY);
        avail.setStartTime(LocalTime.parse("11:00:00"));
        avail.setEndTime(LocalTime.parse("15:00:00"));
        toBeAssigned.getAvailabilities().add(avail);
        tutorAvailabilityService.create(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        avail2.setTutor(tutor2);
        avail2.setDay(DayOfWeek.THURSDAY);
        avail2.setStartTime(LocalTime.parse("11:00:00"));
        avail2.setEndTime(LocalTime.parse("15:00:00"));
        tutor2.getAvailabilities().add(avail2);
        tutorAvailabilityService.create(avail2);

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(toBeAssigned);
        toBeAssigned.getPreferredUnits().add(preference);
        preference.setPriority(1);
        tutorUnitPreferenceService.create(preference);

        TutorUnitPreference preference2 = DomainObjectFactory.createTutorUnitPreference();
        preference2.setId(0);
        preference2.setUnit(unit);
        preference2.setTutor(tutor2);
        tutor2.getPreferredUnits().add(preference2);
        preference2.setPriority(2);
        tutorUnitPreferenceService.create(preference2);

        administrationService.assignAllTutorials();

        assertEquals("The tutor was not assigned.", tutorial.getTutor(), toBeAssigned);
        assertEquals("The tutor was not assigned.", tutorial2.getTutor(), toBeAssigned);
    }
}
