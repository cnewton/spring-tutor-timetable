package au.edu.curtin.tutortimetable.test.service.tutoravailability;

import static org.junit.Assert.assertEquals;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class TutorAvailabilityServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private TutorAvailabilityService tutorAvailabilityService;

    @Autowired
    private DomainObjectServiceTester<TutorAvailability> tester;

    private TutorAvailability testTutorAvailability;

    @Before
    public void setUp() throws Exception {

        testTutorAvailability = DomainObjectFactory.createTutorAvailability();
        tester.setService(tutorAvailabilityService);
        tester.setTestObject(testTutorAvailability);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorAvailability.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorAvailability = tutorAvailabilityService.find(1);
        testTutorAvailability.setDay(DayOfWeek.MONDAY);
        tester.setTestObject(testTutorAvailability);

        tester.testUpdate();

        assertEquals("The days do not match", DayOfWeek.MONDAY, testTutorAvailability.getDay());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorAvailability.setId(0); // Create object to test deletion.
        tutorAvailabilityService.create(testTutorAvailability);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testListAddAvailabilities() {

        List<TutorAvailability> list = new ArrayList<>();

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        list.add(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        list.add(avail2);

        long countBefore = tutorAvailabilityService.count();

        tutorAvailabilityService.listAddAvailabilities(2, list);

        assertEquals("Should have two more availabilties.", countBefore + 2, tutorAvailabilityService.count());
    }

    @Test
    @Transactional
    public void testListSubtractAvailabilities() {

        long[] ids = { 1, 2 };

        long countBefore = tutorAvailabilityService.count();

        tutorAvailabilityService.listSubtractAvailabilities(2, ids);

        assertEquals("Should have two less availabilties.", countBefore - 2, tutorAvailabilityService.count());
    }

    @Test
    @Transactional
    public void testListSubtractNonExistantAvailabilities() {

        long[] ids = { -1, 2 };

        long countBefore = tutorAvailabilityService.count();

        tutorAvailabilityService.listSubtractAvailabilities(2, ids);

        assertEquals("Should have one less availabilty.", countBefore - 1, tutorAvailabilityService.count());
    }
}
