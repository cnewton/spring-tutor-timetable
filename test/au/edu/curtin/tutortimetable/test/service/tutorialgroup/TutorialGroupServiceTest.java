package au.edu.curtin.tutortimetable.test.service.tutorialgroup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialType;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class TutorialGroupServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private DomainObjectServiceTester<TutorialGroup> tester;

    private TutorialGroup testTutorialGroup;

    @Before
    public void setUp() throws Exception {

        testTutorialGroup = DomainObjectFactory.createTutorialGroup();
        tester.setService(tutorialGroupService);
        tester.setTestObject(testTutorialGroup);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorialGroup.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorialGroup = tutorialGroupService.find(1);
        testTutorialGroup.setTutorialType(TutorialType.COMPUTER_LABORATORY);
        tester.setTestObject(testTutorialGroup);

        tester.testUpdate();

        assertEquals("The Rooms do not match", TutorialType.COMPUTER_LABORATORY, testTutorialGroup.getTutorialType());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorialGroup.setId(0); // Create object to test deletion.
        tutorialGroupService.create(testTutorialGroup);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindTutorialGroupGroupByName() {

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setGroupName("test1Name");
        group.setId(0);

        tutorialGroupService.create(group);

        assertNotNull(tutorialGroupService.findByName("test1Name"));
    }

    @Test
    @Transactional
    public void testAssignTutorialGroupToTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup tutorialGroup = DomainObjectFactory.createTutorialGroup();
        tutorialGroup.setId(0);
        tutorialGroupService.create(tutorialGroup);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        tutorialGroupService.update(tutorialGroup);

        tutorialGroupService.assignTutorialGroupToTutor(tutorialGroup.getId(), tutor.getId());

        assertEquals("Two tutorials should be found.", 2,
                tutorialService.loadTutorialsForTutor(tutor.getId()).size());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAssignNonExistantTutorialGroupToTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        tutorialGroupService.assignTutorialGroupToTutor(-1, tutor.getId());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAssignTutorialGroupToNonExistantTutor() {

        TutorialGroup tutorialGroup = DomainObjectFactory.createTutorialGroup();
        tutorialGroup.setId(0);
        tutorialGroupService.create(tutorialGroup);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutorialGroup(tutorialGroup);
        tutorialGroup.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        tutorialGroupService.update(tutorialGroup);

        tutorialGroupService.assignTutorialGroupToTutor(tutorialGroup.getId(), -1);
    }

    @Test
    @Transactional
    public void testLoadTutorialGroupsForUnit() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        tutorialGroupService.create(group);

        assertEquals("There should be 1 group.", 1,
                tutorialGroupService.loadTutorialGroupsForUnit(unit.getId()).size());

    }

    @Test
    @Transactional
    public void testClearAssignment() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorial.setTutorialGroup(group);
        tutorialService.create(tutorial);

        group.getTutorials().add(tutorial);
        tutorialGroupService.update(group);

        tutorialGroupService.clearAssignmentForTutorialGroup(group.getId());

        tutorial = tutorialService.find(tutorial.getId());

        assertEquals("The tutorial should not have a tutor", null, tutorial.getTutor());
    }
}
