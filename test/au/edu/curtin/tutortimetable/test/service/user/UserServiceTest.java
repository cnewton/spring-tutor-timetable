package au.edu.curtin.tutortimetable.test.service.user;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.user.User;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.domainobject.viewmodel.TutorDetailsDTO;
import au.edu.curtin.tutortimetable.exception.CustomSecurityException;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class UserServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectServiceTester<User> tester;

    @Autowired
    private BCryptPasswordEncoder encoder;

    private User testUser;

    @Before
    public void setUp() throws Exception {

        testUser = DomainObjectFactory.createUser();
        tester.setService(userService);
        tester.setTestObject(testUser);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUser.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUser = userService.find(1);
        testUser.setName("TestUpdate");
        tester.setTestObject(testUser);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testUser.getName());
    }

    @Test
    @Transactional
    public void testUpdateTutor() {

        // Note the tutor id here should match an existing tutor.
        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setStudentId("123444555");

        tutor = (Tutor) userService.updateUserDetails(tutor);

        assertEquals("The names do not match", "123444555", tutor.getStudentId());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUser.setId(0); // Create object to test deletion.
        userService.create(testUser);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateEmptyUsername() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("");
        tutor.setId(0);
        userService.create(tutor);
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNullUsername() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername(null);
        tutor.setId(0);
        userService.create(tutor);
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateUsernameTaken() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setUsername("test");
        tutor.setId(0);
        userService.create(tutor);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setUsername("test");
        tutor2.setId(0);
        userService.create(tutor2);
    }

    @Test
    @Transactional
    public void testUpdatePassword() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setPassword("update1");
        tutor.setId(0);
        userService.create(tutor);

        userService.updateUserPassword(tutor.getId(), "update1", "update2");

        assertEquals("The password was not updated.", true, encoder.matches("update2", tutor.getPassword()));
    }

    @Test(expected = CustomSecurityException.class)
    @Transactional
    public void testUpdateWrongPassword() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setPassword("update1");
        tutor.setId(0);
        userService.create(tutor);

        userService.updateUserPassword(tutor.getId(), "update5", "update2");
    }

    @Test
    @Transactional
    public void testFindTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setName("Test");
        tutor.setId(0);
        userService.create(tutor);

        Tutor tutor2 = userService.findTutor(tutor.getId());

        assertEquals("Tutor name did not match.", tutor.getName(), tutor2.getName());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistantTutor() {

        userService.findTutor(-1);
    }

    @Test
    @Transactional
    public void updateTutorDetails() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();
        dto.setCourse("TestCourse");

        userService.updateTutorDetails(tutor.getId(), dto);

        tutor = userService.findTutor(tutor.getId());

        assertEquals("The course was not correctly updated.", dto.getCourse(), tutor.getCourse());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void updateNonExistantTutorDetails() {

        TutorDetailsDTO dto = DomainObjectFactory.createTutorDetailsDTO();

        userService.updateTutorDetails(-1, dto);
    }

    @Test
    @Transactional
    public void testLoadTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        userService.loadTutor(tutor.getId());
    }

    @Test
    @Transactional
    public void testFindAllTutors() {

        long countBefore = userService.findAllTutors().size();

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        tutor.setUsername("New 1");
        userService.create(tutor);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setUsername("New 2");
        userService.create(tutor2);

        // Add a unit coordinator to ensure that tutors don't include coordinators (As UC's are an extension of Tutors)
        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setUsername("New 3");
        userService.create(coordinator);

        long countAfter = userService.findAllTutors().size();

        assertEquals("There should be 2 new tutors.", 2, countAfter - countBefore);
    }

    @Test
    @Transactional
    public void testLockTutors() {

        userService.setTutorAccountsEnabled(false);

        for (Tutor tutor : userService.findAllTutors()) {
            assertEquals("The tutor should be locked.", false, tutor.isEnabled());
        }

        // Ensure other accounts not locked.
        for (User user : userService.findAll()) {
            if (!(user instanceof Tutor))
                assertEquals("The user should be unlocked.", true, user.isEnabled());
        }
    }

    @Test
    @Transactional
    public void testUnlockTutors() {

        userService.setTutorAccountsEnabled(false);

        for (Tutor tutor : userService.findAllTutors()) {
            assertEquals("The tutor should be locked.", false, tutor.isEnabled());
        }

        userService.setTutorAccountsEnabled(true);

        for (Tutor tutor : userService.findAllTutors()) {
            assertEquals("The tutor should be unlocked.", true, tutor.isEnabled());
        }
    }

    @Test
    @Transactional
    public void testLockCoordinators() {

        userService.setCoordinatorAccountsEnabled(false);

        for (UnitCoordinator coordinator : userService.findAllCoordinators()) {
            assertEquals("The coordinator should be locked.", false, coordinator.isEnabled());
        }

        // Ensure other accounts not locked.
        for (User user : userService.findAll()) {
            if (!(user instanceof UnitCoordinator))
                assertEquals("The user should be unlocked.", true, user.isEnabled());
        }
    }

    @Test
    @Transactional
    public void testUnlockCoordinators() {

        userService.setCoordinatorAccountsEnabled(false);

        for (UnitCoordinator coordinator : userService.findAllCoordinators()) {
            assertEquals("The coordinator should be locked.", false, coordinator.isEnabled());
        }

        userService.setCoordinatorAccountsEnabled(true);

        for (UnitCoordinator coordinator : userService.findAllCoordinators()) {
            assertEquals("The coordinator should be unlocked.", true, coordinator.isEnabled());
        }
    }
}
