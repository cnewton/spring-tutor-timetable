package au.edu.curtin.tutortimetable.test.service.tutorunitpreference;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class TutorUnitPreferenceServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private DomainObjectServiceTester<TutorUnitPreference> tester;

    private TutorUnitPreference testTutorUnitPreference;

    @Autowired
    private UserService userService;

    @Autowired
    private UnitService unitService;

    @Before
    public void setUp() throws Exception {

        testTutorUnitPreference = DomainObjectFactory.createTutorUnitPreference();
        tester.setService(tutorUnitPreferenceService);
        tester.setTestObject(testTutorUnitPreference);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorUnitPreference.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorUnitPreference = tutorUnitPreferenceService.find(1);
        testTutorUnitPreference.setPriority(5);
        tester.setTestObject(testTutorUnitPreference);

        tester.testUpdate();

        assertEquals("The days do not match", 5, testTutorUnitPreference.getPriority());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorUnitPreference.setId(0); // Create object to test deletion.
        tutorUnitPreferenceService.create(testTutorUnitPreference);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testCreateTutorPreference() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference preference = tutorUnitPreferenceService.createTutorPreference(tutor.getId(), unit.getId());

        assertEquals("The preference did not have a priority of 0.", 0, preference.getPriority());
        assertEquals("The preferences tutor was not correct.", tutor, preference.getTutor());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testCreateTutorPreferenceNonExistantUnit() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        tutorUnitPreferenceService.createTutorPreference(tutor.getId(), -1);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testCreateTutorPreferenceNonExistantTutor() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        tutorUnitPreferenceService.createTutorPreference(-1, unit.getId());
    }

    @Test
    @Transactional
    public void testUpdateTutorPreferencePriority() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference preference = tutorUnitPreferenceService.createTutorPreference(tutor.getId(), unit.getId());

        tutorUnitPreferenceService.updateTutorPreferencePriority(preference.getId(), 5);

        assertEquals("The preference priority was not updated.", 5, preference.getPriority());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistantTutorPreferencePriority() {

        tutorUnitPreferenceService.updateTutorPreferencePriority(-1, 5);
    }

    @Test
    @Transactional
    public void testAddTutorPreferenceList() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);
        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unitService.create(unit2);

        long countBefore = tutorUnitPreferenceService.count();

        tutorUnitPreferenceService.listAddTutorPreferences(2, new long[] { unit.getId(), unit2.getId() });

        assertEquals("The tutor should now contain 2 unit preferences.", countBefore + 2,
                tutorUnitPreferenceService.count());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAddTutorPreferenceListNonExistantUnit() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        tutorUnitPreferenceService.listAddTutorPreferences(2, new long[] { -1 });

    }

    @Test
    @Transactional
    public void testSubtractTutorPreferenceList() {

        long countBefore = tutorUnitPreferenceService.count();

        tutorUnitPreferenceService.listSubtractTutorPreferences(2, new long[] { 1 });

        assertEquals("The tutor should contain 2 less unit preferences.", countBefore - 1,
                tutorUnitPreferenceService.count());
    }

    @Test
    @Transactional
    public void testSubtractTutorPreferenceListNonExistantUnit() {

        long countBefore = tutorUnitPreferenceService.count();

        tutorUnitPreferenceService.listSubtractTutorPreferences(2, new long[] { -1 });

        assertEquals("The unit preferences should not change.", countBefore, tutorUnitPreferenceService.count());
    }

    @Test
    @Transactional
    public void testLoadForUnit() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unitService.create(unit);

        TutorUnitPreference pref = DomainObjectFactory.createTutorUnitPreference();
        pref.setId(0);
        pref.setUnit(unit);
        tutorUnitPreferenceService.create(pref);

        assertEquals("There should be one preference for the given unit.", 1, tutorUnitPreferenceService
                .loadTutorPreferencesForUnit(unit.getId()).size());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testLoadForNonExistantUnit() {

        tutorUnitPreferenceService.loadTutorPreferencesForUnit(-1);
    }
}
