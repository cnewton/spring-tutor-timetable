package au.edu.curtin.tutortimetable.test.service.tutorial;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.exception.NotCreateOperationException;
import au.edu.curtin.tutortimetable.exception.NotUpdateOperationException;
import au.edu.curtin.tutortimetable.exception.ObjectNotFoundException;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTest;
import au.edu.curtin.tutortimetable.test.service.DomainObjectServiceTester;

public class TutorialServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectServiceTester<Tutorial> tester;

    private Tutorial testTutorial;

    @Before
    public void setUp() throws Exception {

        testTutorial = DomainObjectFactory.createTutorial();
        tester.setService(tutorialService);
        tester.setTestObject(testTutorial);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testTutorial.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testTutorial = tutorialService.find(1);
        testTutorial.setRoom("TestUpdate");
        tester.setTestObject(testTutorial);

        tester.testUpdate();

        assertEquals("The Rooms do not match", "TestUpdate", testTutorial.getRoom());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testTutorial.setId(0); // Create object to test deletion.
        tutorialService.create(testTutorial);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testAssignTutorialToTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        tutorialService.assignTutorialToTutor(tutorial.getId(), tutor.getId());

        assertEquals("One tutorial should be found.", 1, tutorialService.loadTutorialsForTutor(tutor.getId()).size());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAssignNonExistantTutorialToTutor() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        tutorialService.assignTutorialToTutor(-1, tutor.getId());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAssignTutorialToNonExistantTutor() {

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorialService.create(tutorial);

        tutorialService.assignTutorialToTutor(tutorial.getId(), -1);
    }

    @Test
    @Transactional
    public void testClearAssignment() {

        Tutor tutor = DomainObjectFactory.createTutor();
        tutor.setId(0);
        userService.create(tutor);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(tutor);
        tutorialService.create(tutorial);

        tutorialService.clearAssignmentForTutorial(tutorial.getId());

        tutorial = tutorialService.find(tutorial.getId());

        assertEquals("The tutorial should not have a tutor", null, tutorial.getTutor());
    }
}
