package au.edu.curtin.tutortimetable.test.service.csvparsing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.csv.parser.CsvImportParser;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.unitcoordinator.UnitCoordinator;
import au.edu.curtin.tutortimetable.exception.CsvParsingException;
import au.edu.curtin.tutortimetable.exception.InvalidCSVException;
import au.edu.curtin.tutortimetable.service.csvparsing.CsvParsingService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class CsvParsingServiceTest extends SpringTest {

    private File testFile;

    @Autowired
    private CsvParsingService csvParsingService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void test4StandardUnits() throws IOException, InvalidCSVException {

        long countBefore = unitService.count();
        testFile = new ClassPathResource("csv/test4StandardUnits.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
        long countAfter = unitService.count();

        assertEquals("There should be 4 new units.", 4, countAfter - countBefore);

    }

    @Test
    @Transactional
    public void test4StandardUnitsWithId() throws IOException, InvalidCSVException {

        long countBefore = unitService.count();
        testFile = new ClassPathResource("csv/test4StandardUnitsWithId.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
        long countAfter = unitService.count();

        assertEquals("There should be 4 new units.", 4, countAfter - countBefore);

    }

    @Test
    @Transactional
    public void test4StandardUnitsSameCoordinator() throws IOException, InvalidCSVException {

        UnitCoordinator coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setName("Unit Coordinator 1");
        userService.create(coordinator);

        testFile = new ClassPathResource("csv/test4StandardUnitsSameCoordinator.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);

        int countWithCoordinator = 0;

        for (Unit unit : unitService.findAll()) {
            if (unit.getUnitCoordinator().getName().equals(coordinator.getName())) {
                countWithCoordinator++;
            }
        }
        assertEquals("There should be 4 units with the same coordinator.", 4, countWithCoordinator);

    }

    @Test(expected = InvalidCSVException.class)
    @Transactional
    public void testEmptyHeader() throws IOException, InvalidCSVException {

        testFile = new ClassPathResource("csv/testEmptyHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
    }

    @Test(expected = InvalidCSVException.class)
    @Transactional
    public void testInvalidHeader() throws InvalidCSVException, IOException {

        testFile = new ClassPathResource("csv/testInvalidHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
    }

    @Test(expected = InvalidCSVException.class)
    @Transactional
    public void testMissingTutorialHeader() throws InvalidCSVException, IOException {

        testFile = new ClassPathResource("csv/testTutorialMissingHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseTutorialCsv(stream);
    }

    @Test(expected = InvalidCSVException.class)
    @Transactional
    public void testMissingUnitHeader() throws InvalidCSVException, IOException {

        testFile = new ClassPathResource("csv/testUnitMissingHeader.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
    }

    @Test
    @Transactional
    public void testMissingField() throws IOException, InvalidCSVException {

        long countBefore = unitService.count();
        testFile = new ClassPathResource("csv/testMissingFieldUnits.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
        long countAfter = unitService.count();

        assertEquals("There should be 3 new units.", 3, countAfter - countBefore);
    }

    @Test
    @Transactional
    public void test4StandardTutorials() throws IOException, InvalidCSVException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("UnitA");
        unitService.create(unit);

        long countBefore = tutorialService.count();
        testFile = new ClassPathResource("csv/test4StandardTutorials.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseTutorialCsv(stream);
        long countAfter = tutorialService.count();

        assertEquals("There should be 4 new tutorials.", 4, countAfter - countBefore);

    }

    @Test
    @Transactional
    public void testTutorialsMissingUnitAndGroup() throws IOException, InvalidCSVException {

        long countBefore = tutorialService.count();
        testFile = new ClassPathResource("csv/testTutorialsMissingUnitAndGroup.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseTutorialCsv(stream);
        long countAfter = tutorialService.count();

        assertEquals("There should be 0 new tutorials.", 0, countAfter - countBefore);

    }

    @Test
    @Transactional
    public void test4StandardTutorialsPreexistingGroup() throws IOException, InvalidCSVException {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitCode("UnitA");
        unitService.create(unit);

        Tutorial tute = DomainObjectFactory.createTutorial();
        tute.getTutorialGroup().setId(0);
        tute.getTutorialGroup().setGroupName("ExistingName");
        tute.getTutorialGroup().setUnit(unit);
        TutorialGroup grp = tutorialGroupService.create(tute.getTutorialGroup());

        unit.getTutorialGroups().add(tute.getTutorialGroup());

        long countBefore = tutorialService.count();
        testFile = new ClassPathResource("csv/test4StandardTutorialsPreexistingGroup.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseTutorialCsv(stream);
        long countAfter = tutorialService.count();

        assertEquals("There should be 4 new tutorials.", 4, countAfter - countBefore);

        assertEquals("The Group should now have 4 tutorials.", 4, grp.getTutorials().size());

    }

    @Test
    @Transactional
    public void testLargeUnitCsv() throws IOException, InvalidCSVException {

        insertCoordinator("Sie Teng  Soh", "1234");
        insertCoordinator("Mark Upston", "1235");
        insertCoordinator("Ling  Li", "1236");
        insertCoordinator("Hannes Herrmann", "1237");
        insertCoordinator("Wan-Quan  Liu", "1238");
        insertCoordinator("Sonny  Pham", "1239");
        insertCoordinator("Raymond  Sheh", "1240");
        insertCoordinator("Mihai  Lazarescu", "1241");
        insertCoordinator("David  Cooper", "1242");
        insertCoordinator("Aneesh  Krishna", "1243");

        long countBefore = unitService.count();
        testFile = new ClassPathResource("csv/testLargeUnitCsv.csv").getFile();
        InputStream stream = new FileInputStream(testFile);
        csvParsingService.parseUnitCsv(stream);
        long countAfter = unitService.count();

        assertEquals("There should be 34 new units.", 34, countAfter - countBefore);

        for (Unit unit : unitService.findAll()) {
            assertNotNull(unit.getUnitCoordinator());
        }

    }

    @Test
    @Transactional
    public void testLargeTutorialCsv() throws IOException, InvalidCSVException {

        insertCoordinator("Sie Teng  Soh", "1234");
        insertCoordinator("Mark Upston", "1235");
        insertCoordinator("Ling  Li", "1236");
        insertCoordinator("Hannes Herrmann", "1237");
        insertCoordinator("Wan-Quan  Liu", "1238");
        insertCoordinator("Sonny  Pham", "1239");
        insertCoordinator("Raymond  Sheh", "1240");
        insertCoordinator("Mihai  Lazarescu", "1241");
        insertCoordinator("David  Cooper", "1242");
        insertCoordinator("Aneesh  Krishna", "1243");

        testFile = new ClassPathResource("csv/testLargeUnitCsv.csv").getFile();
        InputStream stream = new FileInputStream(testFile);

        csvParsingService.parseUnitCsv(stream);

        testFile = new ClassPathResource("csv/testLargeTutorialCsv.csv").getFile();
        stream = new FileInputStream(testFile);

        long countBefore = tutorialService.count();
        csvParsingService.parseTutorialCsv(stream);
        long countAfter = tutorialService.count();

        assertEquals("There should be 70 new tutorials.", 70, countAfter - countBefore);

        for (Tutorial tutorial : tutorialService.findAll()) {
            assertNotNull(tutorial.getTutorialGroup());
        }
    }

    @Test
    @Transactional
    public void testExportUnitCsv() throws InvalidCSVException, CsvParsingException {

        long unitCount = unitService.count();
        long csvCount = 0;

        byte[] unitCsv = csvParsingService.exportUnitCsv();

        ByteArrayInputStream bais = new ByteArrayInputStream(unitCsv);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The unitCount and csvCount should match.", unitCount, csvCount);
    }
    
    @Test
    @Transactional
    public void testExportTutorialCsv() throws InvalidCSVException, CsvParsingException {

        long tutorialCount = tutorialService.count();
        long csvCount = 0;

        byte[] tutorialCSV = csvParsingService.exportTutorialCsv();

        ByteArrayInputStream bais = new ByteArrayInputStream(tutorialCSV);

        // parse the written file as an import csv to validate.
        try (CsvImportParser parser = new CsvImportParser(new InputStreamReader(bais))) {
            while (!parser.isEOF()) {
                if (parser.parseRow() != null) {
                    csvCount++;
                }
            }
        }
        assertEquals("The tutorialCount and csvCount should match.", tutorialCount, csvCount);
    }

    private void insertCoordinator(String name, String staffId) {

        UnitCoordinator coordinator;
        coordinator = DomainObjectFactory.createUnitCoordinator();
        coordinator.setId(0);
        coordinator.setName(name);
        coordinator.setStaffId(staffId);
        coordinator.setUsername(staffId + "username");
        userService.create(coordinator);
    }
}
