package au.edu.curtin.tutortimetable.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.IdBasedDomainObject;
import au.edu.curtin.tutortimetable.service.DomainObjectService;

@Component
public class DomainObjectServiceTester<T extends IdBasedDomainObject> {

    private DomainObjectService<T> service;

    private T obj;

    public void setService(DomainObjectService<T> service) {

        this.service = service;
    }

    public void setTestObject(T obj) {

        this.obj = obj;
    }

    @Transactional
    public void testFind() {

        T obj = service.find(1L);
        assertNotNull("object not found", obj);
        assertEquals("The object id was not correct", 1, obj.getId());
    }

    @Transactional
    public void testFindNonExistant() {

        service.find(-1);
        service.count(); // Cause transaction flush.
    }

    @Transactional
    public void testFindAll() {

        long count = service.count();
        List<T> list = service.findAll();
        assertEquals("Incorrect number of objects found", count, list.size());
    }

    @Transactional
    public void testCreate() {

        obj.setId(0);

        long countBefore = service.count();
        service.create(obj);
        long countAfter = service.count();

        assertNotNull(service.find(obj.getId()));
        assertEquals("Incorrect count after object was created", countBefore + 1, countAfter);
    }

    @Transactional
    public void testCreateNotCreateOperation() {

        obj.setId(5);
        service.create(obj);
    }

    @Transactional
    public void testUpdate() {

        long countBefore = service.count();
        obj = service.update(obj);
        long countAfter = service.count();

        assertNotNull(service.find(obj.getId()));
        assertEquals("Incorrect count after object was updated", countBefore, countAfter);
    }

    @Transactional
    public void testUpdateNonExistant() {

        obj.setId(1000);

        service.update(obj);

        service.count(); // Force transaction flush

    }

    @Transactional
    public void testUpdateNotUpdateOperation() {

        obj.setId(0);
        service.update(obj);
    }

    @Transactional
    public void testDeleteByObject() {

        long countBefore = service.count();
        service.delete(obj);
        long countAfter = service.count();

        assertEquals("object was not deleted using ID", countBefore - 1, countAfter);
    }

    @Transactional
    public void testDeleteNonExistantByObject() {

        obj.setId(-1);
        service.delete(obj);

        service.count(); // Force transaction flush

    }

    @Transactional
    public void testDeleteById() {

        long countBefore = service.count();
        service.delete(3);
        long countAfter = service.count();

        assertEquals("object was not deleted using ID", countBefore - 1, countAfter);

    }

    @Transactional
    public void testDeleteNonExistantById() {

        service.delete(-1);

        fail("Test did not throw exception when expected");
    }

    @Transactional
    public void testCount() {

        long countBefore = service.count();
        service.delete(3);
        long countAfter = service.count();

        assertEquals("Incorrect buyer count", countBefore - 1, countAfter);
    }

}
