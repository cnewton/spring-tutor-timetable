package au.edu.curtin.tutortimetable.test.service.assignment;

import static org.junit.Assert.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import au.edu.curtin.tutortimetable.domainobject.tutoravailability.TutorAvailability;
import au.edu.curtin.tutortimetable.domainobject.tutorial.Tutorial;
import au.edu.curtin.tutortimetable.domainobject.tutorialgroup.TutorialGroup;
import au.edu.curtin.tutortimetable.domainobject.tutorunitpreference.TutorUnitPreference;
import au.edu.curtin.tutortimetable.domainobject.unit.Unit;
import au.edu.curtin.tutortimetable.domainobject.user.tutor.Tutor;
import au.edu.curtin.tutortimetable.service.administration.AdministrationService;
import au.edu.curtin.tutortimetable.service.assignment.AssignmentService;
import au.edu.curtin.tutortimetable.service.tutoravailability.TutorAvailabilityService;
import au.edu.curtin.tutortimetable.service.tutorial.TutorialService;
import au.edu.curtin.tutortimetable.service.tutorialgroup.TutorialGroupService;
import au.edu.curtin.tutortimetable.service.tutorunitpreference.TutorUnitPreferenceService;
import au.edu.curtin.tutortimetable.service.unit.UnitService;
import au.edu.curtin.tutortimetable.service.user.UserService;
import au.edu.curtin.tutortimetable.test.common.DomainObjectFactory;
import au.edu.curtin.tutortimetable.test.common.SpringTest;

public class AssignmentServiceTest extends SpringTest {

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private UserService userService;

    @Autowired
    private TutorialService tutorialService;

    @Autowired
    private TutorialGroupService tutorialGroupService;

    @Autowired
    private TutorAvailabilityService availabilityService;

    @Autowired
    private TutorUnitPreferenceService tutorUnitPreferenceService;

    @Autowired
    private AdministrationService adminService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testAssignmentBasic() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitName("Test Unit");
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        unit.getTutorialGroups().add(group);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutor(null);
        tutorial.setTutorialGroup(group);
        tutorial.setStartTime(LocalDateTime.parse("2016-06-02T11:00:00"));
        tutorial.setEndTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        group.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutor(null);
        tutorial2.setTutorialGroup(group);
        tutorial2.setStartTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        tutorial2.setEndTime(LocalDateTime.parse("2016-06-02T15:00:00"));
        group.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        Tutor toBeAssigned = DomainObjectFactory.createTutor();
        toBeAssigned.setId(0);
        toBeAssigned.setName("Prior 1");
        userService.create(toBeAssigned);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setName("Prior 2");
        tutor2.setUsername("new user");
        userService.create(tutor2);

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        avail.setTutor(toBeAssigned);
        avail.setDay(DayOfWeek.THURSDAY);
        avail.setStartTime(LocalTime.parse("11:00:00"));
        avail.setEndTime(LocalTime.parse("15:00:00"));
        toBeAssigned.getAvailabilities().add(avail);
        availabilityService.create(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        avail2.setTutor(tutor2);
        avail2.setDay(DayOfWeek.THURSDAY);
        avail2.setStartTime(LocalTime.parse("11:00:00"));
        avail2.setEndTime(LocalTime.parse("15:00:00"));
        tutor2.getAvailabilities().add(avail2);
        availabilityService.create(avail2);

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(toBeAssigned);
        toBeAssigned.getPreferredUnits().add(preference);
        preference.setPriority(1);
        tutorUnitPreferenceService.create(preference);

        TutorUnitPreference preference2 = DomainObjectFactory.createTutorUnitPreference();
        preference2.setId(0);
        preference2.setUnit(unit);
        preference2.setTutor(tutor2);
        tutor2.getPreferredUnits().add(preference2);
        preference2.setPriority(2);
        tutorUnitPreferenceService.create(preference2);

        assignmentService.assignTutorialsGreedily();

        assertEquals("The tutor was not assigned.", tutorial.getTutor(), toBeAssigned);
        assertEquals("The tutor was not assigned.", tutorial2.getTutor(), toBeAssigned);
    }

    @Test
    @Transactional
    public void testAssignmentBasicAlreadyAssigned() {

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitName("Test Unit");
        unitService.create(unit);

        TutorialGroup group = DomainObjectFactory.createTutorialGroup();
        group.setId(0);
        group.setUnit(unit);
        unit.getTutorialGroups().add(group);
        tutorialGroupService.create(group);

        Tutorial tutorial = DomainObjectFactory.createTutorial();
        tutorial.setId(0);
        tutorial.setTutorialGroup(group);
        tutorial.setStartTime(LocalDateTime.parse("2016-06-02T11:00:00"));
        tutorial.setEndTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        group.getTutorials().add(tutorial);
        tutorialService.create(tutorial);

        Tutor originalAssignment1 = tutorial.getTutor();

        Tutorial tutorial2 = DomainObjectFactory.createTutorial();
        tutorial2.setId(0);
        tutorial2.setTutorialGroup(group);
        tutorial2.setStartTime(LocalDateTime.parse("2016-06-02T13:00:00"));
        tutorial2.setEndTime(LocalDateTime.parse("2016-06-02T15:00:00"));
        group.getTutorials().add(tutorial2);
        tutorialService.create(tutorial2);

        Tutor originalAssignment2 = tutorial2.getTutor();

        Tutor toBeAssigned = DomainObjectFactory.createTutor();
        toBeAssigned.setId(0);
        toBeAssigned.setName("Prior 1");
        userService.create(toBeAssigned);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setName("Prior 2");
        tutor2.setUsername("new user");
        userService.create(tutor2);

        TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
        avail.setId(0);
        avail.setTutor(toBeAssigned);
        avail.setDay(DayOfWeek.THURSDAY);
        avail.setStartTime(LocalTime.parse("11:00:00"));
        avail.setEndTime(LocalTime.parse("15:00:00"));
        toBeAssigned.getAvailabilities().add(avail);
        availabilityService.create(avail);

        TutorAvailability avail2 = DomainObjectFactory.createTutorAvailability();
        avail2.setId(0);
        avail2.setTutor(tutor2);
        avail2.setDay(DayOfWeek.THURSDAY);
        avail2.setStartTime(LocalTime.parse("11:00:00"));
        avail2.setEndTime(LocalTime.parse("15:00:00"));
        tutor2.getAvailabilities().add(avail2);
        availabilityService.create(avail2);

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(toBeAssigned);
        toBeAssigned.getPreferredUnits().add(preference);
        preference.setPriority(1);
        tutorUnitPreferenceService.create(preference);

        TutorUnitPreference preference2 = DomainObjectFactory.createTutorUnitPreference();
        preference2.setId(0);
        preference2.setUnit(unit);
        preference2.setTutor(tutor2);
        tutor2.getPreferredUnits().add(preference2);
        preference2.setPriority(2);
        tutorUnitPreferenceService.create(preference2);

        assignmentService.assignTutorialsGreedily();

        assertEquals("The tutor assignment should not change.", originalAssignment1, tutorial.getTutor());
        assertEquals("The tutor assignment should not change.", originalAssignment2, tutorial2.getTutor());
    }

    @Test
    @Transactional
    public void testAssignmentComplex() {

        int groupCount = 6;

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitName("Test Unit");
        unitService.create(unit);

        createFilledTutorialGroups(unit, groupCount);

        Tutor toBeAssigned = DomainObjectFactory.createTutor();
        toBeAssigned.setId(0);
        toBeAssigned.setUsername("Prior 1");
        userService.create(toBeAssigned);

        createBlanketAvailability(toBeAssigned);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setName("Prior 2");
        tutor2.setUsername("Prior 2");
        userService.create(tutor2);

        createBlanketAvailability(tutor2);

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(toBeAssigned);
        toBeAssigned.getPreferredUnits().add(preference);
        preference.setPriority(1);
        tutorUnitPreferenceService.create(preference);

        TutorUnitPreference preference2 = DomainObjectFactory.createTutorUnitPreference();
        preference2.setId(0);
        preference2.setUnit(unit);
        preference2.setTutor(tutor2);
        tutor2.getPreferredUnits().add(preference2);
        preference2.setPriority(2);
        tutorUnitPreferenceService.create(preference2);

        assignmentService.assignTutorialsGreedily();

        Tutor t1 = userService.findTutor(toBeAssigned.getId());

        assertEquals(
                "The first tutor should have 12(Classes) * tutorMaxhours(p/week) / 2(duration of a tutorial) tutorials assigned.",
                12 * adminService.getSystemState().getTutorMaxHours() / 2,
                t1.getAssignedTutorials().size());

        assertEquals("The first tutor should list tutorMaxHours assigned hours.",
                adminService.getSystemState().getTutorMaxHours(), t1.getAssignedHours());

        Tutor t2 = userService.findTutor(tutor2.getId());

        assertEquals("The second tutor should have 12(Classes) * groupCount - t1AssignedClasses assigned classes.",
                12 * groupCount - t1.getAssignedTutorials().size(), t2.getAssignedTutorials().size());
    }

    @Test
    @Transactional
    public void testAssignmentDuplicatedTutorials() {

        int groupCount = 1;

        Unit unit = DomainObjectFactory.createUnit();
        unit.setId(0);
        unit.setUnitName("Test Unit");
        unitService.create(unit);

        createFilledTutorialGroups(unit, groupCount);
        createConsecutiveTutorials(unit.getTutorialGroups().iterator().next(), 12, 0);

        Tutor tutor1 = DomainObjectFactory.createTutor();
        tutor1.setId(0);
        tutor1.setUsername("Prior 1");
        userService.create(tutor1);

        createBlanketAvailability(tutor1);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setName("Prior 2");
        tutor2.setUsername("Prior 2");
        userService.create(tutor2);

        createBlanketAvailability(tutor2);

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(tutor1);
        tutor1.getPreferredUnits().add(preference);
        preference.setPriority(1);
        tutorUnitPreferenceService.create(preference);

        TutorUnitPreference preference2 = DomainObjectFactory.createTutorUnitPreference();
        preference2.setId(0);
        preference2.setUnit(unit);
        preference2.setTutor(tutor2);
        tutor2.getPreferredUnits().add(preference2);
        preference2.setPriority(2);
        tutorUnitPreferenceService.create(preference2);

        assignmentService.assignTutorialsGreedily();

        Tutor t1 = userService.findTutor(tutor1.getId());
        Tutor t2 = userService.findTutor(tutor2.getId());

        assertEquals("The first tutor should have 12 Classes assigned.", 12, t1.getAssignedTutorials().size());

        assertEquals("The second tutor should have 12 Classes assigned.", 12, t2.getAssignedTutorials().size());
    }

    @Test
    @Transactional
    public void testAssignmentComplex2Units() {

        int groupCount = 6;

        Unit unit1 = DomainObjectFactory.createUnit();
        unit1.setId(0);
        unit1.setUnitName("Test Unit1");
        unitService.create(unit1);

        Unit unit2 = DomainObjectFactory.createUnit();
        unit2.setId(0);
        unit2.setUnitName("Test Unit2");
        unitService.create(unit2);

        createFilledTutorialGroups(unit1, groupCount);
        createFilledTutorialGroups(unit2, groupCount);

        Tutor tutor1 = DomainObjectFactory.createTutor();
        tutor1.setId(0);
        tutor1.setUsername("Prior 1");
        userService.create(tutor1);

        createBlanketAvailability(tutor1);
        preferenceUnit(tutor1, unit1, 1);
        preferenceUnit(tutor1, unit2, 1);

        Tutor tutor2 = DomainObjectFactory.createTutor();
        tutor2.setId(0);
        tutor2.setUsername("Prior 2");
        userService.create(tutor2);

        createBlanketAvailability(tutor2);
        preferenceUnit(tutor2, unit1, 2);
        preferenceUnit(tutor2, unit2, 2);

        Tutor tutor3 = DomainObjectFactory.createTutor();
        tutor3.setId(0);
        tutor3.setUsername("Prior 3");
        userService.create(tutor3);

        createBlanketAvailability(tutor3);
        preferenceUnit(tutor3, unit1, 3);

        assignmentService.assignTutorialsGreedily();

        Tutor t1 = userService.findTutor(tutor1.getId());

        assertEquals(60, t1.getAssignedTutorials().size());
        // The highest priority tutor should have been assigned tutorials from the lowest preference count unit (unit2)
        for (Tutorial tutorial : t1.getAssignedTutorials()) {
            assertEquals(unit2.getId(), tutorial.getTutorialGroup().getUnit().getId());
        }

        Tutor t2 = userService.findTutor(tutor2.getId());

        assertEquals(60, t2.getAssignedTutorials().size());
        // Map a count of how many tutorials are for each unit.
        Map<Long, Integer> t2Tutorials = new HashMap<>();
        t2Tutorials.put(unit1.getId(), 0);
        t2Tutorials.put(unit2.getId(), 0);

        for (Tutorial tutorial : t2.getAssignedTutorials()) {
            long unitId = tutorial.getTutorialGroup().getUnit().getId();
            int count = t2Tutorials.get(unitId);
            t2Tutorials.put(unitId, count + 1);
        }
        // Expect that unit2 is assigned first, unit1 fills the remainder.
        assertEquals(12, t2Tutorials.get(unit2.getId()).intValue());
        assertEquals(48, t2Tutorials.get(unit1.getId()).intValue());

        Tutor t3 = userService.findTutor(tutor3.getId());
        // Lowest priority tutor is given the remainder.
        assertEquals(24, t3.getAssignedTutorials().size());
    }

    private void createFilledTutorialGroups(Unit unit, int count) {

        for (int ii = 0; ii < count; ii++) {
            TutorialGroup group = DomainObjectFactory.createTutorialGroup();
            group.setId(0);
            group.setUnit(unit);
            unit.getTutorialGroups().add(group);
            tutorialGroupService.create(group);

            createConsecutiveTutorials(group, 12, ii);
        }
    }

    private void createConsecutiveTutorials(TutorialGroup container, int count, int timeOffset) {

        LocalDateTime week1 = LocalDateTime.parse("2016-02-29T08:00:00");
        week1 = week1.plusHours(timeOffset * 2);

        for (int ii = 0; ii < count; ii++) {
            Tutorial tutorial = DomainObjectFactory.createTutorial();
            tutorial.setTutor(null);
            tutorial.setId(0);
            tutorial.setTutorialGroup(container);
            tutorial.setStartTime(week1.plusDays(ii * 7));
            tutorial.setEndTime(tutorial.getStartTime().plusHours(2));
            container.getTutorials().add(tutorial);
            tutorialService.create(tutorial);
        }
    }

    private void createBlanketAvailability(Tutor tutor) {

        for (DayOfWeek day : DayOfWeek.values()) {

            TutorAvailability avail = DomainObjectFactory.createTutorAvailability();
            avail.setId(0);
            avail.setTutor(tutor);
            avail.setDay(day);
            avail.setStartTime(LocalTime.parse("07:00:00"));
            avail.setEndTime(LocalTime.parse("21:00:00"));
            tutor.getAvailabilities().add(avail);
            availabilityService.create(avail);
        }
    }

    private void preferenceUnit(Tutor tutor, Unit unit, int priority) {

        TutorUnitPreference preference = DomainObjectFactory.createTutorUnitPreference();
        preference.setId(0);
        preference.setUnit(unit);
        preference.setTutor(tutor);
        tutor.getPreferredUnits().add(preference);
        preference.setPriority(priority);
        tutorUnitPreferenceService.create(preference);
    }
}
